package com.instreamatic.adman.view;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public class ViewEvent extends BaseEvent<ViewEvent.Type, ViewEvent.Listener> {

    final public static EventType<Type, ViewEvent, Listener> TYPE = new EventType<Type, ViewEvent, Listener>("view") {
        @Override
        public void callListener(ViewEvent event, Listener listener) {
            listener.onViewEvent(event);
        }
    };

    public enum Type {
        SHOW,
        CLOSE,
    }

    public ViewEvent(Type type) {
        super(type);
    }

    @Override
    public EventType<Type, ViewEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onViewEvent(ViewEvent event);
    }
}