package com.instreamatic.adman.view.core;

import android.app.Activity;

import com.instreamatic.adman.view.AdmanLayoutType;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.IAdmanViewBundleFactory;

abstract public class AdmanViewBindFactory implements IAdmanViewBundleFactory {

    abstract protected IAdmanViewBundle buildPortrait(Activity activity);
    abstract protected IAdmanViewBundle buildLandscape(Activity activity);
    abstract protected IAdmanViewBundle buildVoice(Activity activity);

    @Override
    public IAdmanViewBundle build(AdmanLayoutType type, Activity activity) {
        switch (type) {
            case PORTRAIT:
                return buildPortrait(activity);
            case LANDSCAPE:
                return buildLandscape(activity);
            case VOICE:
                return buildVoice(activity);
            default:
                return null;
        }
    }
}
