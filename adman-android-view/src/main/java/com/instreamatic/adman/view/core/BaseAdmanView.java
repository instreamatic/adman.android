package com.instreamatic.adman.view.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.ControlEvent;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.adman.view.*;
import com.instreamatic.adman.voice.VoiceEvent;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.VASTExtension;
import com.instreamatic.vast.VASTPlayer;
import com.instreamatic.vast.model.VASTInline;

import java.lang.ref.WeakReference;

import static com.instreamatic.adman.event.PlayerEvent.Type.CLOSEABLE;
import static com.instreamatic.adman.view.AdmanViewType.*;


abstract public class BaseAdmanView extends BaseAdmanModule implements IAdmanView, PlayerEvent.Listener, VoiceEvent.Listener, View.OnClickListener {

    final private static String TAG = BaseAdmanView.class.getSimpleName();

    protected WeakReference<Activity> contextRef;
    protected ViewGroup target;

    private IAdmanViewBundle bundle;

    protected View overlay;

    private boolean closeable;
    private boolean displaying;
    private boolean voiceRunning;
    private boolean needControlButtons = false;

    public BaseAdmanView(Activity context) {
        setActivity(context);
    }

    public void setActivity(Activity activity){
        WeakReference<Activity> weakRef = this.contextRef;
        this.contextRef = new WeakReference<>(activity);
        if (weakRef != null) {
            weakRef.clear();
        }
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {PlayerEvent.TYPE, VoiceEvent.TYPE};
    }

    @Override
    public String getId() {
        return IAdmanView.ID;
    }

    @Override
    public View getView() {
        return this.bundle != null ? this.bundle.get(CONTAINER) : null;
    }

    @Override
    public void setTarget(ViewGroup target) {
        this.target = target;
    }

    @Override
    public ViewGroup getTarget() {
        if (this.target != null) {
            return this.target;
        } else {
            Activity context = this.contextRef.get();
            if(context == null) {
                Log.w(TAG, "Activity is null");
                return null;
            }
            return (ViewGroup) context.getWindow().getDecorView().findViewById(android.R.id.content);
        }
    }

    @Override
    public void show() {
        Activity context = this.contextRef.get();
        if (context == null) return;
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!displaying) {
                    displaying = true;
                    prepareView();
                    setVisibleControlButtons(PlayerEvent.Type.PLAYING);

                    displaying = displayContent();
                    displayBanner();

                    IAdman adman = getAdman();
                    if (adman != null && adman.isPlaying()) {
                        AudioPlayer player = adman.getPlayer();
                        if (player != null) {
                            processProgressChange(player.getPosition(), player.getDuration());
                        }
                        if (adman.isVoiceRecording()) {
                            processVoiceEvent(VoiceEvent.Type.START);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void close() {
        Activity context = contextRef.get();
        if(context == null) return;

        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideOverlay();
                View viewContainer = getView();
                if (viewContainer != null) {
                    getTarget().removeView(viewContainer);
                    //viewContainer.destroyDrawingCache();
                }
                displaying = false;
            }
        });

        IAdman adman = getAdman();
        if (adman != null) {
            adman.getDispatcher().dispatch(new ViewEvent(ViewEvent.Type.CLOSE));
        }
    }

    public void reset() {
        close();
    }

    public void rebuild() {
        close();
        IAdman adman = getAdman();
        if (adman != null && adman.isPlaying()) {
            show();
        }
    }

    @Override
    public void onClick(View v) {
        AdmanViewBind<?> bind = bundle != null ? bundle.getById(v.getId()) : null;
        if (bind != null) {
            AdmanViewType<?> type = bind.type;
            if (type == RESTART) {
                AudioPlayer player = getAdman().getPlayer();
                if (player != null) {
                    player.rewind();
                }
            } else if (type == PLAY) {
                getAdman().getDispatcher().dispatch(new ControlEvent(ControlEvent.Type.RESUME));
            } else if (type == PAUSE) {
                getAdman().getDispatcher().dispatch(new ControlEvent(ControlEvent.Type.PAUSE));
            } else if (type == ABOUT) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://instreamatic.com"));
                Context context = contextRef.get();
                if(context != null) context.startActivity(intent);
            } else if (type == SKIP) {
                getAdman().getPlayer().stop();
            } else if (type == TEXT) {
                getAdman().open();
            } else if (type == CLOSE) {
                if (closeable) {
                    close();
                    AudioPlayer player = getAdman().getPlayer();
                    if (player != null && player.getState() == VASTPlayer.State.PAUSED) {
                        player.resume();
                    }
                } else {
                    getAdman().getDispatcher().dispatch(new ControlEvent(ControlEvent.Type.SKIP));
                }
                showOverlay();
            }
            //voice
            else if (type == VOICE_POSITIVE) {
                getAdman().getDispatcher().dispatch(new ControlEvent(ControlEvent.Type.CLICK_POSITIVE));
            } else if (type == VOICE_NEGATIVE) {
                getAdman().getDispatcher().dispatch(new ControlEvent(ControlEvent.Type.CLICK_NEGATIVE));
            }
        }
    }

    @Override
    public void onPlayerEvent(PlayerEvent event) {
        processPlayerEvent(event.getType());
    }

    @Override
    public void onVoiceEvent(VoiceEvent event) {
        processVoiceEvent(event.getType());
    }

    protected String formatTime(int time) {
        int mm = time / 60;
        int ss = time % 60;
        return ((mm < 10 ? "0" : "") + mm) + ":" + ((ss < 10 ? "0" : "") + ss);
    }

    private void processPlayerEvent(PlayerEvent.Type type) {
        Activity context;
        switch (type) {
            case PLAYING:
                show();
                break;
            case PLAY:
                context = contextRef.get();
                if (context == null) break;
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setVisibleControlButtons(PlayerEvent.Type.PLAY);
                    }
                });
                break;
            case PAUSE:
                context = contextRef.get();
                if (context == null) break;
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setVisibleControlButtons(PlayerEvent.Type.PAUSE);
                    }
                });
                break;
            case COMPLETE:
            case FAILED:
                close();
                break;
            case PROGRESS:
                AudioPlayer player = getAdman().getPlayer();
                if (player != null) {
                    processProgressChange(player.getPosition(), player.getDuration());
                }
                break;
            case CLOSEABLE:
            case SKIPPABLE:
                closeable = (type == CLOSEABLE);
                if (!closeable && bundle != null && bundle.contains(CLOSE)) {
                    View close = bundle.get(CLOSE);
                    close.setVisibility(View.VISIBLE);
                    close.bringToFront();
                }
                break;
        }
    }

    private void processVoiceEvent(VoiceEvent.Type type) {
        switch (type) {
            case START:
                setVisible(View.VISIBLE, VOICE_CONTAINER, VOICE_MIC);
                voiceRunning = true;
                break;
            case STOP:
                if (voiceRunning) {
                    setVisible(View.VISIBLE, VOICE_PROGRESS);
                    voiceRunning = false;
                }
                setVisible(View.INVISIBLE, VOICE_CONTAINER, VOICE_MIC);
                break;
            case RESPONSE:
            case FAIL:
                setVisible(View.INVISIBLE, VOICE_CONTAINER, VOICE_PROGRESS, VOICE_MIC);
                voiceRunning = false;
                break;
        }
    }

    private void setVisible(int visibility, AdmanViewType<?>... types){
        if (bundle == null) return;
        for (AdmanViewType<?> type : types) {
            if (bundle.contains(type)) {
                bundle.get(type).setVisibility(visibility);
            }
        }
    }

    private void processProgressChange(final int position, final int total) {
        Activity context = contextRef.get();
        if (context == null) return;
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int l = (total - position) / 1000;
                if (bundle == null) return;
                if (bundle.contains(LEFT)) {
                    bundle.get(LEFT).setText(formatTime(l));
                }
                if (bundle.contains(PROGRESS)) {
                    SeekBar progress = bundle.get(PROGRESS);
                    progress.setMax(total);
                    progress.setProgress(position);
                }
                if (bundle.contains(TIME)) {
                    bundle.get(TIME).setText(formatTime(position / 1000));
                }
                if (bundle.contains(DURATION)) {
                    bundle.get(DURATION).setText(formatTime(total / 1000));
                }
            }
        });
    }


    private IAdmanViewBundle buildViewBundle(VASTInline ad) {
        Activity context = contextRef.get();
        if (context == null) {
            Log.i(TAG, "Activity is null");
            return null;
        }

        if (ad != null && ad.extensions != null) {
            VASTExtension extensionControls = ad.extensions.containsKey("controls") ? ad.extensions.get("controls") : null;
            if (extensionControls != null) {
                needControlButtons = (1 == Integer.parseInt(extensionControls.value));
            }
        }

        if (ad != null && ad.isVoice()) {
            return factory().build(AdmanLayoutType.VOICE, context);
        }
        int orientation = context.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return factory().build(AdmanLayoutType.PORTRAIT, context);
        } else {
            return factory().build(AdmanLayoutType.LANDSCAPE, context);
        }
    }

    private void prepareView() {
        VASTInline ad = getAdman().getCurrentAd();
        bundle = buildViewBundle(ad);
        if (bundle == null){
            Log.e(TAG, "Layout has not built");
            return;
        }
        if (bundle.contains(BANNER)) {
            bundle.get(BANNER).setVisibility(View.INVISIBLE);
        }
        if (bundle.contains(CLOSE)) {
            bundle.get(CLOSE).setVisibility(closeable ? View.INVISIBLE : View.GONE);
        }
        if (bundle.contains(SKIP)) {
            bundle.get(SKIP).setVisibility(View.GONE);
        }
        if (bundle.contains(PLAY)) {
            bundle.get(PLAY).setVisibility(View.GONE);
        }
        hideVisibleControlButtons();
        for (AdmanViewType<?> type : new AdmanViewType[]{
                RESTART,
                PLAY,
                PAUSE,
                ABOUT,
                TEXT,
                VOICE_POSITIVE,
                VOICE_NEGATIVE,
                SKIP,
                CLOSE,
        }) {
            if (bundle.contains(type)) {
                bundle.get(type).setOnClickListener(this);
            }
        }
    }


    //voice
    private View getViewOverlay(){
        // ToDo: may be do R.layout.adman_overlay
        /*
        LayoutInflater inflater = LayoutInflater.from(context);
        return inflater.inflate(R.layout.adman_overlay, null, false);
        /**/
        Activity context = contextRef.get();
        if(context == null) return null;
        LinearLayout layout = new LinearLayout(context);
        layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        layout.setBackgroundColor(Color.TRANSPARENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        layout.setFocusable(true);
        layout.setClickable(true);
        return layout;
    }

    private void showOverlay() {
        if (overlay == null) {
            ViewGroup target = getTarget();
            if (target != null) {
                overlay = getViewOverlay();
                target.addView(overlay);
            }
        }
    }

    private void hideOverlay() {
        if (overlay != null) {
            ViewGroup target = getTarget();
            if (target != null) {
                target.removeView(overlay);
            }
            overlay = null;
        }
    }

    private void showBanner(ViewGroup container, ImageView banner) {
        if (banner != null) {
            ViewGroup parent = (ViewGroup) banner.getParent();
            if (parent != null) {
                parent.removeView(banner);
            }
            container.addView(banner);
        }
    }

    private void displayBanner(){
        if (bundle != null && bundle.contains(BANNER)) {
            ViewGroup container = bundle.get(BANNER);
            ImageView banner = getAdman().getBanner();
            if (banner != null){
                container.setVisibility(View.VISIBLE);
                showBanner(container, banner);
            } else {
                container.setVisibility(View.INVISIBLE);
            }
        }
    }

    private boolean displayContent(){
        boolean isAdd = false;
        //prepareView();
        VASTInline ad = getAdman().getCurrentAd();
        VASTExtension extensionLink = (ad != null && ad.extensions.containsKey("linkTxt")) ? ad.extensions.get("linkTxt") : null;
        if (bundle != null && bundle.contains(TEXT) &&  extensionLink != null) {
            TextView text = bundle.get(TEXT);
            text.setText(extensionLink.value);
            text.setSelected(true);
            bundle.get(TEXT).setVisibility(View.VISIBLE);
        }
        ViewGroup target = getTarget();
        if (target != null) {
            View viewContainer = getView();
            if (viewContainer != null) {
                target.addView(viewContainer, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                isAdd = true;
                getAdman().getDispatcher().dispatch(new ViewEvent(ViewEvent.Type.SHOW));
            }
            else{
                Log.e(TAG, "View container not found");
            }
        }
        return isAdd;
    }

    private void hideVisibleControlButtons() {
        if (needControlButtons) return;

        if (bundle != null) {
            if (bundle.contains(PLAY)) {
                bundle.get(PLAY).setVisibility(View.GONE);
            }
            if (bundle.contains(PAUSE)) {
                bundle.get(PAUSE).setVisibility(View.GONE);
            }
            if (bundle.contains(RESTART)) {
                bundle.get(RESTART).setVisibility(View.GONE);
            }


            if (bundle.contains(VOICE_POSITIVE)) {
                bundle.get(VOICE_POSITIVE).setVisibility(View.GONE);
            }
            if (bundle.contains(VOICE_NEGATIVE)) {
                bundle.get(VOICE_NEGATIVE).setVisibility(View.GONE);
            }
        }
    }

    private void setVisibleControlButtons(PlayerEvent.Type type) {
        // !!! need call from runOnUiThread
        if (!needControlButtons) return;
        if (bundle == null) {
            Log.w(TAG, "Bundle not found");
            return;
        }

        if (type == PlayerEvent.Type.PLAYING) {
            if (bundle.contains(PLAY)) {
                bundle.get(PLAY).setVisibility(View.GONE);
            }
            if (bundle.contains(PAUSE)) {
                bundle.get(PAUSE).setVisibility(View.VISIBLE);
            }
        } else if (type == PlayerEvent.Type.PLAY) {
            if (bundle.contains(PLAY)) {
                bundle.get(PLAY).setVisibility(View.GONE);
            }
            if (bundle.contains(PAUSE)) {
                bundle.get(PAUSE).setVisibility(View.VISIBLE);
            }
        } else if (type == PlayerEvent.Type.PAUSE) {
            if (bundle.contains(PLAY)) {
                bundle.get(PLAY).setVisibility(View.VISIBLE);
            }
            if (bundle.contains(PAUSE)) {
                bundle.get(PAUSE).setVisibility(View.GONE);
            }
        }
    }

}
