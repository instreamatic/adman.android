package com.instreamatic.adman.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

final public class AdmanViewType<T extends View> {

    final public static AdmanViewType<ViewGroup> CONTAINER = new AdmanViewType<>(ViewGroup.class);
    final public static AdmanViewType<ViewGroup> BANNER = new AdmanViewType<>(ViewGroup.class);
    final public static AdmanViewType<View> CLOSE = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> RESTART = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> PLAY = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> PAUSE = new AdmanViewType<>(View.class);
    final public static AdmanViewType<TextView> LEFT = new AdmanViewType<>(TextView.class);
    final public static AdmanViewType<TextView> ABOUT = new AdmanViewType<>(TextView.class);
    final public static AdmanViewType<TextView> TEXT = new AdmanViewType<>(TextView.class);
    final public static AdmanViewType<SeekBar> PROGRESS = new AdmanViewType<>(SeekBar.class);
    final public static AdmanViewType<TextView> TIME = new AdmanViewType<>(TextView.class);
    final public static AdmanViewType<TextView> DURATION = new AdmanViewType<>(TextView.class);
    final public static AdmanViewType<View> SKIP = new AdmanViewType<>(View.class);

    final public static AdmanViewType<ViewGroup> VOICE_CONTAINER = new AdmanViewType<>(ViewGroup.class);
    final public static AdmanViewType<View> VOICE_POSITIVE = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> VOICE_NEGATIVE = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> VOICE_MIC = new AdmanViewType<>(View.class);
    final public static AdmanViewType<View> VOICE_PROGRESS = new AdmanViewType<>(View.class);

    final public Class<T> type;

    private AdmanViewType(Class<T> type) {
        this.type = type;
    }
}
