package com.instreamatic.adman.view.generic;

import android.app.Activity;

import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanViewBundle;
import com.instreamatic.adman.view.R;
import com.instreamatic.adman.view.core.AdmanViewBindFactory;
import com.instreamatic.adman.view.core.AdmanViewBundle;

import java.util.HashMap;
import java.util.Map;

import static com.instreamatic.adman.view.AdmanViewType.BANNER;
import static com.instreamatic.adman.view.AdmanViewType.CLOSE;
import static com.instreamatic.adman.view.AdmanViewType.LEFT;
import static com.instreamatic.adman.view.AdmanViewType.PAUSE;
import static com.instreamatic.adman.view.AdmanViewType.PLAY;
import static com.instreamatic.adman.view.AdmanViewType.RESTART;
import static com.instreamatic.adman.view.AdmanViewType.VOICE_CONTAINER;
import static com.instreamatic.adman.view.AdmanViewType.VOICE_MIC;
import static com.instreamatic.adman.view.AdmanViewType.VOICE_NEGATIVE;
import static com.instreamatic.adman.view.AdmanViewType.VOICE_POSITIVE;
import static com.instreamatic.adman.view.AdmanViewType.VOICE_PROGRESS;

public class DefaultAdmanViewBindFactory extends AdmanViewBindFactory {

    final Map<AdmanViewType, Integer> bindings = new HashMap<AdmanViewType, Integer>() {{
            put(BANNER, R.id.adman_banner);
            put(CLOSE, R.id.adman_close);
            put(PLAY, R.id.adman_play);
            put(PAUSE, R.id.adman_pause);
            put(RESTART, R.id.adman_restart);
            put(LEFT, R.id.adman_left);

            put(VOICE_POSITIVE, R.id.adman_response_positive);
            put(VOICE_NEGATIVE, R.id.adman_response_negative);
            put(VOICE_CONTAINER, R.id.adman_response_container);
            put(VOICE_MIC, R.id.adman_mic_active);
            put(VOICE_PROGRESS, R.id.adman_voice_progress);
        }};

    @Override
    protected IAdmanViewBundle buildPortrait(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.adman_portrait, bindings);
    }

    @Override
    protected IAdmanViewBundle buildLandscape(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.adman_landscape, bindings);
    }

    @Override
    protected IAdmanViewBundle buildVoice(Activity activity) {
        return AdmanViewBundle.fromLayout(activity, R.layout.adman_voice_portrait, bindings);
    }

}
