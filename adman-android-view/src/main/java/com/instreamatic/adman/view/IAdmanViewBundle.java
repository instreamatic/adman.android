package com.instreamatic.adman.view;

import android.view.View;

public interface IAdmanViewBundle {
    public <T extends View> boolean contains(AdmanViewType<T> type);
    public <T extends View> T get(AdmanViewType<T> type);
    public AdmanViewBind getById(int id);
}
