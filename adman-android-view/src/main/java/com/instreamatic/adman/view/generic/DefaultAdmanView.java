package com.instreamatic.adman.view.generic;

import android.app.Activity;
import android.content.Context;
import com.instreamatic.adman.view.IAdmanViewBundleFactory;
import com.instreamatic.adman.view.core.BaseAdmanView;

public class DefaultAdmanView extends BaseAdmanView {

    private IAdmanViewBundleFactory factory;

    public DefaultAdmanView(Activity activity) {
        super(activity);
        factory = new DefaultAdmanViewBindFactory();
    }

    @Override
    public IAdmanViewBundleFactory factory() {
        return factory;
    }
}
