package com.instreamatic.adman.view;

import android.view.View;
import android.view.ViewGroup;
import com.instreamatic.adman.module.IAdmanModule;

public interface IAdmanView extends IAdmanModule {
    final public static String ID = "view";

    public View getView();

    public void setTarget(ViewGroup target);

    public ViewGroup getTarget();

    public void show();

    public void close();

    public void rebuild();

    public IAdmanViewBundleFactory factory();
}
