package com.instreamatic.adman.view.core;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.instreamatic.adman.view.AdmanViewBind;
import com.instreamatic.adman.view.AdmanViewType;
import com.instreamatic.adman.view.IAdmanViewBundle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.instreamatic.adman.view.AdmanViewType.CONTAINER;

public class AdmanViewBundle implements IAdmanViewBundle {

    final private Map<Integer, AdmanViewBind> byId;
    final private Map<AdmanViewType, AdmanViewBind> byType;

    public AdmanViewBundle(List<AdmanViewBind> bindings) {
        byId = new HashMap<>();
        byType = new HashMap<>();
        for (AdmanViewBind bind : bindings) {
            if (bind.view != null) {
                byId.put(bind.view.getId(), bind);
                byType.put(bind.type, bind);
            }
        }
    }

    public <T extends View> boolean contains(AdmanViewType<T> type) {
        return byType.containsKey(type) && byType.get(type).view != null;
    }

    @SuppressWarnings("unchecked")
    public <T extends View> T get(AdmanViewType<T> type) {
        return byType.containsKey(type) ? ((T) byType.get(type).view) : null;
    }

    @Override
    public AdmanViewBind getById(int id) {
        return byId.get(id);
    }

    @SuppressWarnings("unchecked")
    public static AdmanViewBundle fromLayout(Activity activity, int layoutId, Map<AdmanViewType, Integer> bindings) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        ViewGroup container = (ViewGroup) inflater.inflate(layoutId, null, false);
        List<AdmanViewBind> result = new ArrayList<>();
        result.add(new AdmanViewBind<>(CONTAINER, container));
        for (Map.Entry<AdmanViewType, Integer> entry : bindings.entrySet()) {
            result.add(new AdmanViewBind<View>(entry.getKey(), container.findViewById(entry.getValue())));
        }
        return new AdmanViewBundle(result);
    }
}
