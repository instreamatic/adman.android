package com.instreamatic.adman.view;

import android.view.View;

public class AdmanViewBind<T extends View> {

    final public AdmanViewType type;
    final public T view;

    public AdmanViewBind(AdmanViewType<T> type, T view) {
        this.type = type;
        this.view = view;
    }
}
