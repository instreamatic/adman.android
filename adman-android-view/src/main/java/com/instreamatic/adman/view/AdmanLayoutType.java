package com.instreamatic.adman.view;

public enum AdmanLayoutType {
    LANDSCAPE,
    PORTRAIT,
    VOICE;
}
