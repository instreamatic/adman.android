package com.instreamatic.adman.view;

import android.app.Activity;

public interface IAdmanViewBundleFactory {
    IAdmanViewBundle build(AdmanLayoutType type, Activity context);
}
