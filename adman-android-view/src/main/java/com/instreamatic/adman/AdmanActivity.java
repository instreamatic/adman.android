package com.instreamatic.adman;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.view.IAdmanView;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.core.android.UiThread;


public class AdmanActivity extends Activity implements AdmanEvent.Listener {

    final private static String TAG = "AdMan";

    private IAdman adman;
    private ProgressBar loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        RelativeLayout content = new RelativeLayout(this);
        content.setBackgroundColor(Color.GRAY);
        content.setGravity(Gravity.CENTER);
        loading = new ProgressBar(this);
        loading.setIndeterminate(true);
        content.addView(loading);
        setContentView(content, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("request")) {
            AdmanRequest request = intent.getParcelableExtra("request");
            adman = new Adman(this, request);
            if (request.type.voice) {
                adman.bindModule(new AdmanVoice(this));
            }
            adman.bindModule(new DefaultAdmanView(this));
            adman.addListener(this);
            adman.start();
        } else {
            adman.getDispatcher().dispatch(new AdmanEvent(AdmanEvent.Type.FAILED));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        ((IAdmanView) adman.getModule(IAdmanView.ID)).rebuild();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adman.play();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void setLoading(final boolean value) {
        UiThread.run(new Runnable() {
            @Override
            public void run() {
                if (value) {
                    loading.setVisibility(View.VISIBLE);
                } else {
                    loading.setVisibility(View.GONE);
                }
            }
        });
    }

    private void finishWhitResult(int result) {
        setResult(result, new Intent());
        finish();
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        switch (event.getType()) {
            case PREPARE:
                setLoading(true);
                break;
            case FAILED:
            case NONE:
                setLoading(false);
                finishWhitResult(RESULT_CANCELED);
                break;
            case STARTED:
                setLoading(false);
                break;
            case COMPLETED:
                finishWhitResult(RESULT_OK);
                break;
        }
    }
}

