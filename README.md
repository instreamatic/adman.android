# Instreamatic android sdk #

## Modules ##

### adman-android ###

Base module.

```
AdmanRequest request = new AdmanRequest.Builder()
        .setSiteId(777)
        .setRegion(Region.EUROPE)
        .setPreview(11)
        .build();

IAdman adman = new Adman(context, request);
adman.start();
```

### adman-android-view ###

Default view implementation module.

```
adman.bindModule(new DefaultAdmanView(context));

...
@Override
public void onConfigurationChanged(Configuration newConfig) {
  super.onConfigurationChanged(newConfig);
  ((IAdmanView) adman.getModule(IAdmanView.ID)).rebuild();
}
```

### adman-android-voice ###

Voice reaction module.

```
adman.bindModule(new AdmanVoice(context));
```

## Develop ##

### Build ###

```
gradle build
```

### Deploy ###

```
gradle uploadArchives
```
