package com.instreamatic.adman.dtmf;

import org.json.JSONException;
import org.json.JSONObject;


public class DTMFMessage {
    final public String label;
    final public long timestamp;

    public DTMFMessage(String label, long timestamp) {
        this.label = label;
        this.timestamp = timestamp;
    }

    public static DTMFMessage fromString(String string) throws JSONException {
        JSONObject json = new JSONObject(string);
        return fromJSON(json);
    }

    public static DTMFMessage fromJSON(JSONObject json) throws JSONException {
        return new DTMFMessage(json.has("label") ? json.getString("label") : null, json.getLong("ts"));
    }
}