package com.instreamatic.adman.dtmf;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;


public class DTMFEvent extends BaseEvent<DTMFEvent.Type, DTMFEvent.Listener> {

    final public static EventType<Type, DTMFEvent, Listener> TYPE = new EventType<Type, DTMFEvent, Listener>("dtmf") {
        @Override
        public void callListener(DTMFEvent event, Listener listener) {
            listener.onDTMFEvent(event);
        }
    };

    public enum Type {
        CONNECTED,
        DTMF_IN,
        DTMF_OUT,
        ERROR,
    }

    final public long timestamp;
    final public Throwable error;

    public DTMFEvent(Type type) {
        this(type, 0);
    }

    public DTMFEvent(Type type, long timestamp) {
        super(type);
        this.timestamp = timestamp;
        this.error = null;
    }
    public DTMFEvent(Type type, Throwable error) {
        super(type);
        this.timestamp = 0;
        this.error = error;
    }

    @Override
    public EventType<Type, DTMFEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onDTMFEvent(DTMFEvent event);
    }
}
