package com.instreamatic.adman.dtmf;

import android.util.Log;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.TextLoader;
import org.json.JSONException;

import java.util.Timer;
import java.util.TimerTask;


public class AdmanDTMF extends BaseAdmanModule implements DTMFConnection.Listener, DTMFEvent.Listener, AdmanEvent.Listener {

    final public static String ID = "dtmf";

    final private static String TAG = AdmanDTMF.class.getSimpleName();

    private DTMFConnection connection;
    private String name;

    private TimeHelp timeHelp;
    private TimerTask startTask;

    public AdmanDTMF(String name) {
        this.name = name;
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {AdmanEvent.TYPE, DTMFEvent.TYPE};
    }

    private void resolveTimestamp(final ICallback<Long> callback) {
        String url = "https://dtmf.instreamatic.com:8081/dtmf/ts";
        new TextLoader().GET(url, new ICallback<String>() {
            @Override
            public void onSuccess(String data) {
                try {
                    DTMFMessage message = DTMFMessage.fromString(data);
                    callback.onSuccess(message.timestamp);
                } catch (JSONException e) {
                    callback.onFail(e);
                }
            }
            @Override
            public void onFail(Throwable t) {
                callback.onFail(t);
            }
        });
    }

    public void start() {
        final String url = "wss://dtmf.instreamatic.com:8081/dtmfws/sub/" + name;
        Log.d(TAG, "start on: " + url);
        resolveTimestamp(new ICallback<Long>() {
            @Override
            public void onSuccess(Long data) {
                getAdman().getDispatcher().dispatch(new DTMFEvent(DTMFEvent.Type.CONNECTED, data));
                timeHelp = new TimeHelp(data);
                connection = new DTMFConnection(url);
                connection.setListener(AdmanDTMF.this);
                connection.start();
            }

            @Override
            public void onFail(Throwable t) {
                getAdman().getDispatcher().dispatch(new DTMFEvent(DTMFEvent.Type.ERROR, t));
            }
        });
    }

    public void stop() {
        cancel();
        if (connection != null && connection.isRunning()) {
            Log.d(TAG, "stop");
            connection.stop();
        }
        connection = null;
    }

    public void cancel() {
        if (startTask != null) {
            startTask.cancel();
            startTask = null;
        }
    }

    @Override
    public void onConnectionError(String message, Throwable e) {
        getAdman().getDispatcher().dispatch(new DTMFEvent(DTMFEvent.Type.ERROR, e));
    }

    @Override
    public void onMessage(DTMFMessage message) {
        if (message.label.equals("IN")) {
            getAdman().getDispatcher().dispatch(new DTMFEvent(DTMFEvent.Type.DTMF_IN, message.timestamp));
        } else if (message.label.equals("OUT")) {
            getAdman().getDispatcher().dispatch(new DTMFEvent(DTMFEvent.Type.DTMF_OUT, message.timestamp));
        }
    }

    @Override
    public void onDTMFEvent(DTMFEvent event) {
        Log.d(TAG, "onDTMFEvent: " + event);
        switch (event.getType()) {
            case DTMF_IN:
                cancel();
                if (getAdman().isPlaying()) {
                    event.stop();
                    return;
                }
                long delay = timeHelp.delay(event.timestamp);
                Log.i(TAG, "onDTMFIN: " + delay);
                startTask = new TimerTask() {
                    @Override
                    public void run() {
                        getAdman().start();
                    }
                };
                new Timer().schedule(startTask, delay * 1000);
                break;
            case ERROR:
                Log.e(TAG, "onDTMFError: " + event.error);
                break;
        }
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        switch (event.getType()) {
            case PREPARE:
            case STARTED:
                cancel();
                break;
        }
    }

    private class TimeHelp {
        private long timestamp;
        private long localTimestamp;

        public TimeHelp(long timestamp) {
            this.timestamp = timestamp;
            this.localTimestamp = System.currentTimeMillis();
        }

        public long delay(long timestamp) {
            return timestamp - (this.timestamp + (System.currentTimeMillis() - localTimestamp) / 1000);
        }
    }
}
