package com.instreamatic.adman.dtmf;

import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import okhttp3.*;
import okio.ByteString;
import org.json.JSONException;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;


public class DTMFConnection {

    private final String TAG = DTMFConnection.class.getSimpleName();

    private static final long ABSOLUTE_TIMEOUT = 30; // For the entire connection

    private final URI uri;

    private volatile boolean running = false;

    private final Object threadLock = new Object();
    private ConnectThread connectThread;

    private final HandlerThread handlerThread;

    private Listener listener;


    public DTMFConnection(String endpoint) {
        try {
            uri = new URI(endpoint);
        } catch (URISyntaxException ex) {
            throw new IllegalArgumentException(ex);
        }
        this.handlerThread = new HandlerThread("Websocket Timeout");
        this.handlerThread.start();
    }

    public void setListener(final Listener listener) {
        this.listener = listener;
    }

    public void start() {
        synchronized (threadLock) {
            Log.d(TAG, "start()");
            running = true;
            connectThread = new ConnectThread();
            connectThread.start();
        }
    }

    public boolean isRunning() {
        return running;
    }

    public void stop() {
        Log.d(TAG, "stop()");
        running = false;
        if (handlerThread.getLooper() != null) {
            handlerThread.getLooper().quit();
        }

        synchronized (threadLock) {
            if (connectThread != null) {
                connectThread.interrupt();
                connectThread = null;
            }
        }
    }

    private class ConnectThread extends Thread {

        private final WebsocketCallbackListener websocketListener = new WebsocketCallbackListener();

        @Override
        public void run() {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(ABSOLUTE_TIMEOUT, TimeUnit.SECONDS)
                    .build();

            Request request = new Request.Builder()
                    .url(uri.toString())
                    .build();

            client.newWebSocket(request, websocketListener);

            // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
            client.dispatcher().executorService().shutdown();
        }

        @Override
        public void interrupt() {
            websocketListener.closeConnection();
            super.interrupt();
        }

        private class WebsocketCallbackListener extends WebSocketListener {

            private WebSocket webSocket = null;

            /**
             * Called to set the web socket being used so it can be closed when the response is received.
             *
             * @param webSocket
             */
            public void setWebSocket(WebSocket webSocket) {
                this.webSocket = webSocket;
            }

            private void closeConnection() {
                // Close the connection once we get a result
                if (webSocket != null) {
                    webSocket.close(1000, "");
                    webSocket = null;
                }
            }

            @Override
            public void onOpen(final WebSocket webSocket, Response response) {
                this.webSocket = webSocket;
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                try {
                    Log.d(TAG, "onMessage() received: " + text);
                    DTMFMessage message = DTMFMessage.fromString(text);
                    if (listener != null) listener.onMessage(message);
                } catch (JSONException e) {
                    System.err.println("Unable to receive message: " + e.getMessage());
                }
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {
                Log.d(TAG, "onMessage() received: " + bytes.string(Charset.forName("utf-8")));
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {

            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                Log.e(TAG, "onClose() received");
                closeConnection();
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                Log.e(TAG, "onFailure", t);
                callErrorListener("WebSocket Error", t);
            }
        }
    }

    private void callErrorListener(final String message, final Throwable ex) {
        if (running) {
            stop();
            if (listener != null) listener.onConnectionError(message, ex);
        }
    }

    public interface Listener {
        void onConnectionError(final String message, final Throwable e);
        void onMessage(DTMFMessage message);
    }
}
