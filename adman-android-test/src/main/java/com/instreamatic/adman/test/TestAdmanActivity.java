package com.instreamatic.adman.test;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.media.AudioManager;

import com.instreamatic.adman.Adman;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.format.MediaFormat;
import com.instreamatic.vast.VASTSelector;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class TestAdmanActivity extends Activity implements AdmanEvent.Listener, View.OnClickListener {

  final private static String TAG = "AdManDemo";

  final private static String DEFAULT_TEXT = "DEFAULT";

  private Adman adman;

  private AudioPlayer radioPlayer;

  DecimalFormat volumeFormat = new DecimalFormat("0.00");

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_test);
    Log.d(TAG, "onCreate");

    Config config = Config.getInstance(this);
    adman = new Adman(this, config.buildRequest());
    adman.bindModule(new AdmanVoice(this));
    adman.addListener(this);
    ((TextView) findViewById(R.id.volume)).setText(volumeFormat.format(adman.getVolume()));

    findViewById(R.id.prepare).setOnClickListener(this);
    findViewById(R.id.play).setOnClickListener(this);
    findViewById(R.id.pause).setOnClickListener(this);
    findViewById(R.id.reset).setOnClickListener(this);
    findViewById(R.id.volumeDown).setOnClickListener(this);
    findViewById(R.id.volumeUp).setOnClickListener(this);
    findViewById(R.id.canShow).setOnClickListener(this);
    findViewById(R.id.playOther).setOnClickListener(this);
    findViewById(R.id.metaInfo).setOnClickListener(this);

    initMediaFormatControls();
  }

  private void initMediaFormatControls() {
    //strict
    CheckBox strictMediaFormat = (CheckBox) findViewById(R.id.strictMediaFormat);
    strictMediaFormat.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        changeMediaFormat();
      }
    });

    //audioTypeMF
    ArrayList<String> listAudioType = new ArrayList<>();
    listAudioType.add(DEFAULT_TEXT);
    for (MediaFormat.MIMETypes item : MediaFormat.MIMETypes.values()) {
      listAudioType.add(item.code);
    }
    ArrayAdapter<String> adapterAudioType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listAudioType);
    adapterAudioType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    Spinner audioTypeSpinner = (Spinner) findViewById(R.id.audioTypeMF);
    audioTypeSpinner.setAdapter(adapterAudioType);
    audioTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        changeMediaFormat();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });

    //Bitrates
    ArrayList<String> listBitrate = new ArrayList<>();
    listBitrate.add(DEFAULT_TEXT);
    for (MediaFormat.Bitrates item : MediaFormat.Bitrates.values()) {
      listBitrate.add(String.valueOf(item.code));
    }
    ArrayAdapter<String> adapterBitrate = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listBitrate);
    adapterBitrate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    Spinner bitratesMFSpinner = (Spinner) findViewById(R.id.bitratesMF);
    bitratesMFSpinner.setAdapter(adapterBitrate);
    bitratesMFSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        changeMediaFormat();
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
      }
    });

  }

  private void changeMediaFormat(){
    if (adman == null) return;

    CheckBox strictMediaFormat = (CheckBox) findViewById(R.id.strictMediaFormat);
    boolean isStrict = strictMediaFormat.isChecked();

    Spinner audioTypeSpinner = (Spinner) findViewById(R.id.audioTypeMF);
    String typeAudioType = (String) audioTypeSpinner.getSelectedItem();

    Spinner bitratesMFSpinner = (Spinner) findViewById(R.id.bitratesMF);
    String itemBitrate = (String) bitratesMFSpinner.getSelectedItem();

    int valueBitrate = itemBitrate.equals(DEFAULT_TEXT) ? MediaFormat.DEFAULT_BITRATE : Integer.valueOf(itemBitrate, 10);

    MediaFormat mediaFormat = (typeAudioType.equals(DEFAULT_TEXT))
            ? new MediaFormat(VASTSelector.DEFAULT_MEDIA.mimeTypes, valueBitrate, isStrict)
            : new MediaFormat(typeAudioType, valueBitrate, isStrict);

    adman.getSelector().setMediaFormat(mediaFormat);
  }

  @Override
  protected void onPause() {
    super.onPause();
    adman.pause();
    setRadioPlaying(false);
  }

  @Override
  protected void onResume() {
    super.onResume();
    adman.play();
  }

  private void toast(final String text) {
    final Context context = this;
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.prepare:
        adman.preload();
        break;
      case R.id.play:
        adman.play();
        break;
      case R.id.pause:
        adman.pause();
        break;
      case R.id.reset:
        ((ViewGroup) findViewById(R.id.container)).removeAllViews();
        adman.reset();
        ((TextView) findViewById(R.id.state)).setText("");
        setRadioPlaying(false);
        break;
      case R.id.volumeDown:
        adman.setVolume(adman.getVolume() - 0.1f);
        ((TextView) findViewById(R.id.volume)).setText(volumeFormat.format(adman.getVolume()));
        break;
      case R.id.volumeUp:
        adman.setVolume(adman.getVolume() + 0.1f);
        ((TextView) findViewById(R.id.volume)).setText(volumeFormat.format(adman.getVolume()));
        break;
      case R.id.canShow:
        adman.sendCanShow();
        break;
      case R.id.playOther:
        setRadioPlaying(radioPlayer == null);
        break;
      case R.id.metaInfo:
        Bundle metaData = adman.getMetadata();
        Toast toast = Toast.makeText(getApplicationContext(),
                String.format("Meta data: %s", metaData.toString()), Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        break;
    }
  }

  private void setRadioPlaying(boolean value) {
    if (value) {
      if (radioPlayer == null) {
        radioPlayer = new AudioPlayer(
                this, "https://shmyga.ru/stream/ices", true);
      }
    } else {
      if (radioPlayer != null) {
        radioPlayer.stop();
        radioPlayer = null;
      }
    }
  }

  @Override
  public void onAdmanEvent(final AdmanEvent event) {
    runOnUiThread(new Runnable() {
      @Override
      public void run() {
        ((TextView) findViewById(R.id.state)).setText(event.getType().name());
      }
    });
    switch (event.getType()) {
      case STARTED:
        if (adman.canShowBanner()) {
          adman.showBanner((ViewGroup) findViewById(R.id.container));
        }
        break;
    }
  }
}

