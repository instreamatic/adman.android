package com.instreamatic.adman.test;

import android.content.Context;
import android.content.SharedPreferences;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.Age;
import com.instreamatic.adman.Gender;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Slot;
import com.instreamatic.adman.Type;


public class Config {

  private static Config instance;

  public static Config getInstance(Context context) {
    if (instance == null) {
      instance = new Config(context);
    }
    return instance;
  }

  private Context context;

  private Region region;
  private Slot slot;
  private Type type;
  private int preview;
  private int siteId;
  private int capmaignId;
  private Gender gender;
  private Age age;

  private Config(Context context) {
    this.context = context;
    restore();
  }

  public AdmanRequest buildRequest() {
    return new AdmanRequest.Builder()
        .setSiteId(siteId)
        .setRegion(region)
        .setSlot(slot)
        .setType(type)
        .setPreview(preview)
        .setCampaignId(capmaignId)
        .setGender(gender)
        .setAge(age)
        .build();
  }

  private void restore() {
    SharedPreferences sp = this.context.getSharedPreferences("adman", Context.MODE_PRIVATE);
    region = Region.valueOf(sp.getString("region", Region.DEFAULT.name()));
    slot = Slot.valueOf(sp.getString("slot", Slot.ANY.name()));
    type = Type.valueOf(sp.getString("type", Type.ANY.name()));
    preview = sp.getInt("preview", 0);
    siteId = sp.getInt("siteId", 777);
    capmaignId = sp.getInt("capmaignId", 0);
    gender = Gender.valueOf(sp.getString("gender", Gender.NONE.name()));
    age = Age.valueOf(sp.getString("age", "0"));
  }

  private void save() {
    this.context.getSharedPreferences("adman", Context.MODE_PRIVATE)
        .edit()
        .putString("region", region.name())
        .putString("slot", slot.name())
        .putString("type", type.name())
        .putInt("preview", preview)
        .putInt("siteId", siteId)
        .putInt("capmaignId", capmaignId)
        .putString("gender", gender.name())
        .putString("age", age.value())
        .apply();
  }

  public Region getRegion() {
    return region;
  }

  public void setRegion(Region region) {
    this.region = region;
    save();
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
    save();
  }

  public Slot getSlot() {
    return slot;
  }

  public void setSlot(Slot slot) {
    this.slot = slot;
    save();
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
    save();
  }

  public int getPreview() {
    return preview;
  }

  public void setPreview(int preview) {
    this.preview = preview;
    save();
  }

  public int getSiteId() {
    return siteId;
  }

  public void setSiteId(int siteId) {
    this.siteId = siteId;
    save();
  }

  public int getCapmaignId() {
    return capmaignId;
  }

  public void setCapmaignId(int capmaignId) {
    this.capmaignId = capmaignId;
    save();
  }

  public Age getAge() {
    return age;
  }

  public void setAge(Age age) {
    this.age = age;
    save();
  }

}
