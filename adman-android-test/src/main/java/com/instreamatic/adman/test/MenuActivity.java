package com.instreamatic.adman.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;
import com.instreamatic.adman.AdmanActivity;
import com.instreamatic.adman.Age;
import com.instreamatic.adman.Gender;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Slot;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.voice.RecordAudioPermission;

public class MenuActivity extends Activity implements AdapterView.OnItemSelectedListener {

    final private static int AD = 1;

    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        config = Config.getInstance(this);

        Spinner regionView = (Spinner) findViewById(R.id.region);
        regionView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Region.values()));
        regionView.setSelection(config.getRegion().ordinal());
        regionView.setOnItemSelectedListener(this);

        Spinner slotView = (Spinner) findViewById(R.id.slot);
        slotView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Slot.values()));
        slotView.setSelection(config.getSlot().ordinal());
        slotView.setOnItemSelectedListener(this);

        Spinner typeView = (Spinner) findViewById(R.id.type);
        typeView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Type.values()));
        typeView.setSelection(config.getType().ordinal());
        typeView.setOnItemSelectedListener(this);

        TextView siteView = (TextView) findViewById(R.id.site);
        siteView.setText(String.valueOf(config.getSiteId()));
        siteView.addTextChangedListener(new TextChangeListener(R.id.site));

        TextView camapignView = (TextView) findViewById(R.id.campaign);
        camapignView.setText(String.valueOf(config.getCapmaignId()));
        camapignView.addTextChangedListener(new TextChangeListener(R.id.campaign));

        TextView previewView = (TextView) findViewById(R.id.preview);
        previewView.setText(String.valueOf(config.getPreview()));
        previewView.addTextChangedListener(new TextChangeListener(R.id.preview));

        Spinner genderView = (Spinner) findViewById(R.id.gender);
        genderView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, Gender.values()));
        genderView.setSelection(config.getGender().ordinal());
        genderView.setOnItemSelectedListener(this);

        Age age = config.getAge();
        TextView age_a = (TextView) findViewById(R.id.age_a);
        age_a.setText(String.valueOf(age.a));
        age_a.addTextChangedListener(new TextChangeListener(R.id.age_a));
        TextView age_b = (TextView) findViewById(R.id.age_b);
        age_b.setText(String.valueOf(age.b));
        age_b.addTextChangedListener(new TextChangeListener(R.id.age_b));


        if (!RecordAudioPermission.isSupported() || RecordAudioPermission.isGranted(this)) {
            findViewById(R.id.audioRecordButton).setVisibility(View.GONE);
        }
    }

    public void test(View view) {
        startActivity(new Intent(this, TestAdmanActivity.class));
    }

    public void testView(View view) {
        startActivity(new Intent(this, TestAdmanViewActivity.class));
    }

    public void testDTMF(View view) {
        startActivity(new Intent(this, TestAdmanDTMFActivity.class));
    }

    public void testActivity(View view) {
        Intent intent = new Intent(this, AdmanActivity.class);
        intent.putExtra("request", config.buildRequest());
        startActivityForResult(intent, AD);
    }

    public void testCrash(View view) {
        throw new RuntimeException("TEST");
    }

    public void audioRecord(View view) {
        RecordAudioPermission.request(this, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (RecordAudioPermission.isSupported() && RecordAudioPermission.checkResult(requestCode, grantResults)) {
            findViewById(R.id.audioRecordButton).setVisibility(View.GONE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AD) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "RESULT_OK", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "RESULT_CANCELED", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "RESULT_#" + resultCode, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (RecordAudioPermission.isSupported()) {
            if (RecordAudioPermission.isGranted(this)){
                findViewById(R.id.audioRecordButton).setVisibility(View.GONE);
            } else{
                findViewById(R.id.audioRecordButton).setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Object item = adapterView.getItemAtPosition(i);
        if (item instanceof Region) {
            config.setRegion((Region) item);
        } else if (item instanceof Slot) {
            config.setSlot((Slot) item);
        } else if (item instanceof Type) {
            config.setType((Type) item);
        } else if (item instanceof Gender) {
            config.setGender((Gender) item);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class TextChangeListener implements TextWatcher {

        private int viewId;

        private TextChangeListener(int viewId) {
            this.viewId = viewId;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            try {
                String str = String.valueOf(editable);
                int value = 0;
                if (str.length() > 0) {
                    value = Integer.parseInt(str);
                }
                if (viewId == R.id.site) {
                    if (value == 0) {
                        throw new NumberFormatException();
                    }
                    config.setSiteId(value);
                } else if (viewId == R.id.preview) {
                    config.setPreview(value);
                } else if (viewId == R.id.campaign) {
                    config.setCapmaignId(value);
                } else if (viewId == R.id.age_a || viewId == R.id.age_b) {
                    if (viewId == R.id.age_b) {
                        TextView age_a = (TextView) findViewById(R.id.age_a);
                        int a = Integer.parseInt(age_a.getText().toString());
                        config.setAge(new Age(a, value));
                    } else {
                        TextView age_b = (TextView) findViewById(R.id.age_b);
                        int b = Integer.parseInt(age_b.getText().toString());
                        config.setAge(new Age(value, b));
                    }
                }
                ((TextView) findViewById(viewId)).setError(null);
            } catch (NumberFormatException ignored) {
                ((TextView) findViewById(viewId)).setError("Invalid number");
            }
        }
    }
}
