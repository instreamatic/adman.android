package com.instreamatic.adman.test;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.dtmf.AdmanDTMF;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.variable.AdmanVariable;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.view.IAdmanView;

import java.util.HashMap;
import java.util.Map;


public class TestAdmanDTMFActivity extends Activity implements AdmanEvent.Listener {

    final private static String TAG = "AdManDemo";

    private IAdman adman;
    private View loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_view_test);
        loading = findViewById(R.id.loading);

        Config config = Config.getInstance(this);
        adman = new Adman(this, config.buildRequest());
        adman.bindModule(new DefaultAdmanView(this));
        adman.bindModule(new AdmanDTMF("test"));
        Map<String, String> variables = new HashMap<>();
        variables.put("variable1", "value1");
        variables.put("variable2", "value2");
        adman.bindModule(new AdmanVariable(variables));
        adman.addListener(this);
    }

    public void test(View view) {
        adman.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        adman.getModuleAs(IAdmanView.ID, IAdmanView.class).rebuild();
    }

    @Override
    protected void onPause() {
        super.onPause();
        adman.getModuleAs(AdmanDTMF.ID, AdmanDTMF.class).stop();
        adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        adman.getModuleAs(AdmanDTMF.ID, AdmanDTMF.class).start();
        adman.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adman.removeListener(this);
    }

    private void setLoading(final boolean value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(value ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        switch (event.getType()) {
            case PREPARE:
                setLoading(true);
                break;
            case STARTED:
            case FAILED:
            case NONE:
                setLoading(false);
                break;
        }
    }
}
