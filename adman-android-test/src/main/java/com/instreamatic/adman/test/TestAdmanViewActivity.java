package com.instreamatic.adman.test;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.view.generic.DefaultAdmanView;
import com.instreamatic.adman.view.IAdmanView;
import com.instreamatic.adman.voice.AdmanVoice;


public class TestAdmanViewActivity extends Activity implements AdmanEvent.Listener {

    final private static String TAG = "AdManDemo";

    private IAdman adman;
    private View loading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_view_test);
        loading = findViewById(R.id.loading);
    }

    public void test(View view) {
        Config config = Config.getInstance(this);
        if (adman == null) {
            adman = new Adman(this, config.buildRequest());
        }
        adman.bindModule(new AdmanVoice(this));
        adman.bindModule(new DefaultAdmanView(this));
        adman.addListener(this);
        adman.start();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (adman != null) ((IAdmanView) adman.getModule(IAdmanView.ID)).rebuild();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (adman != null) adman.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adman != null) adman.play();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (adman != null) adman.removeListener(this);
    }

    private void setLoading(final boolean value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loading.setVisibility(value ? View.VISIBLE : View.GONE);
            }
        });
    }

    @Override
    public void onAdmanEvent(AdmanEvent event) {
        switch (event.getType()) {
            case PREPARE:
                setLoading(true);
                break;
            case STARTED:
            case FAILED:
            case NONE:
                setLoading(false);
                break;
        }
    }
}

