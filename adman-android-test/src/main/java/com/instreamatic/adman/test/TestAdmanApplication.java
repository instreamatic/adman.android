package com.instreamatic.adman.test;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;
import org.acra.ACRA;
import org.acra.config.*;
import org.acra.data.StringFormat;

public class TestAdmanApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        String appName = getResources().getString(R.string.app_name);

        CoreConfigurationBuilder builder = new CoreConfigurationBuilder(this)
                .setBuildConfigClass(BuildConfig.class)
                .setReportFormat(StringFormat.KEY_VALUE_LIST);
        builder.getPluginConfigurationBuilder(MailSenderConfigurationBuilder.class)
                .setMailTo("report@unisound.net")
                .setSubject(appName + " crash log")
                .setReportFileName("crash.stacktrace")
                .setEnabled(true);
        builder.getPluginConfigurationBuilder(ToastConfigurationBuilder.class)
                .setText(appName + " has been crashed")
                .setLength(Toast.LENGTH_LONG)
                .setEnabled(true);
        ACRA.init(this, builder);
    }
}
