package com.instreamatic.adman.voice;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.instreamatic.adman.ActionUtil;
import com.instreamatic.core.android.ActionIntentStorage;
import com.instreamatic.core.android.PhoneUnlockedReceiver;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.VASTDispatcher;
import com.instreamatic.vast.VASTSelector;
import com.instreamatic.vast.model.VASTCalendar;
import com.instreamatic.vast.model.VASTMedia;
import com.instreamatic.vast.model.VASTTrackingEvent;
import com.instreamatic.voice.android.sdk.util.CalendarUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ResponseRunner {

    final private static String TAG = "Adman."+ResponseRunner.class.getSimpleName();

    private AudioPlayer player;
    private VASTSelector selector;
    private ActionIntentStorage storageAction;

    private void asyncCallback(Runnable callback) {
        new Thread(callback).start();
    }

    public ResponseRunner(VASTSelector selector, ActionIntentStorage storageAction) {
        this.selector = selector;
        this.storageAction = storageAction;
    }

    public void run(final Context context, final VoiceEvent event, final Runnable callback) {
        final VoiceResponse response = event.getResponse();
        final String sender = event.getSender();
        this.run(context, response, sender, callback);
    }

    public void run(final Context context, final VoiceResponse response, final String sender, final Runnable callback) {
        if (response.values.length > 0) {
            final List<VoiceResponse.Value> values = new ArrayList<>(Arrays.asList(response.values));
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    if (values.size() > 0) {
                        ResponseRunner.this.run(context, values.remove(0), response.action, sender, this);
                    } else {
                        callback.run();
                    }
                }
            };
            task.run();
        } else {
            callback.run();
        }

    }

    private void play(Context context, String url, final Runnable callback) {
        player = new AudioPlayer(context, url, true);
        player.setName("response");
        player.setStateListener(new AudioPlayer.StateListener() {
            @Override
            public void onStateChange(AudioPlayer.State state) {
                Log.d(TAG, "onStateChange: " + state.name());
                switch (state) {
                    case ERROR:
                    case STOPPED:
                        player = null;
                        callback.run();
                        break;
                }
            }
        });
    }

    public void run(Context context, VoiceResponse.Value value, String action, String sender, final Runnable callback) {
        Log.d(TAG, "Run intent: action " + action + "; sender " + sender + "; value " + value);
        try {
            switch (value.type) {
                case VoiceResponse.LINK:
                    String url = ((VoiceResponse.TextValue) value).value;
                    if (this.storageAction.needStorage()) {
                        this.storageAction.add(ActionUtil.getIntentOpenUrl(url));
                    } else {
                        Context currContext = this.storageAction.getCurrentActivity();
                        ActionUtil.openUrl(currContext == null ? context : currContext, url);
                    }
                    asyncCallback(callback);
                    break;
                case VoiceResponse.PHONE:
                    String numberPhone = ((VoiceResponse.TextValue) value).value;
                    if (this.storageAction.needStorage()) {
                        this.storageAction.add(ActionUtil.getIntentByPhone(numberPhone));
                    } else {
                        Context currContext = this.storageAction.getCurrentActivity();
                        ActionUtil.callByPhone(currContext == null ? context : currContext, numberPhone);
                    }
                    asyncCallback(callback);
                    break;
                case VoiceResponse.SMS:
                    String[] data = ((VoiceResponse.TextValue) value).value.split("\\|");
                    if (this.storageAction.needStorage()) {
                        this.storageAction.add(ActionUtil.getIntentBySMS(data));
                    } else {
                        Context currContext = this.storageAction.getCurrentActivity();
                        ActionUtil.sendSMS(currContext == null ? context : currContext, data);
                    }
                    asyncCallback(callback);
                    break;
                case VoiceResponse.AUDIO:
                    play(context, ((VoiceResponse.TextValue) value).value, callback);
                    break;
                case VoiceResponse.MEDIA:
                    VASTMedia media = selector.selectMedia(((VoiceResponse.MediaValue) value).value);
                    if (media != null) {
                        play(context, media.url, callback);
                    }
                    else {
                        asyncCallback(callback);
                    }
                    break;
                case VoiceResponse.TRACKING:
                    List<VASTTrackingEvent> events = ((VoiceResponse.TrackingValue) value).value;
                    String event_name = sender != null ? "response_" + action + "_" + sender : "response_" + action;
                    for (VASTTrackingEvent event : events) {
                        if (event.event.equals(event_name)) {
                            VASTDispatcher.send(event.url);
                        }
                    }
                    asyncCallback(callback);
                    break;
                case VoiceResponse.CALENDAR:
                    VASTCalendar calendar = ((VoiceResponse.CalendarValue) value).value;
                    CalendarUtils.addEvent(context, calendar);
                    asyncCallback(callback);
                    break;
                case VoiceResponse.REPEAT:
                    asyncCallback(callback);
                    break;
                default:
                    asyncCallback(callback);
                    break;
            }
        } catch (ActivityNotFoundException e) {
            Log.d(TAG, "Run intent, failed", e);
            asyncCallback(callback);
        }
    }

    public boolean pause() {
        if (player != null && player.getState() == AudioPlayer.State.PLAYING) {
            player.pause();
            return true;
        }
        return false;
    }

    public boolean resume() {
        if (player != null && player.getState() == AudioPlayer.State.PAUSED) {
            player.resume();
            return true;
        }
        return false;
    }

    public boolean skip() {
        if (player != null && player.getState() == AudioPlayer.State.PLAYING) {
            player.stop();
            return true;
        }
        return false;
    }
}
