package com.instreamatic.adman.voice;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.UserIdResolver;
import com.instreamatic.adman.event.ControlEvent;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.ModuleEvent;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.core.net.Loader;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.model.VASTInline;
import com.instreamatic.voice.android.fd.SkippedInputStream;
import com.instreamatic.voice.android.sdk.VoiceSearch;
import com.instreamatic.voice.android.sdk.VoiceSearchInfo;
import com.instreamatic.voice.android.sdk.VoiceSearchListener;
import com.instreamatic.voice.android.sdk.audio.SimpleAudioByteStreamSource;
import com.instreamatic.voice.core.model.RequestModel;
import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;

import javax.xml.xpath.XPathExpressionException;
import java.io.InputStream;
import java.util.*;


public class AdmanVoice extends BaseAdmanModule implements PlayerEvent.Listener, VoiceEvent.Listener, ControlEvent.Listener, RequestEvent.Listener, ModuleEvent.Listener {

    final public static String ID = "voice";

    final private static String TAG = AdmanVoice.class.getSimpleName();

    public enum Language {
        ENGLISH_US("en-US"),
        ENGLISH_GB("en-GB"),
        RUSSIAN("ru-RU"),
        CHINESE("zh-CN"),
        FRENCH("fr-FR"),
        GERMAN("de-DE"),
        ITALIAN("it-IT"),
        SPANISH("es-ES"),
        UKRAINIAN("uk-UA"),
        TURKISH("tr-TR"),
        ;

        final public String code;

        Language(String code) {
            this.code = code;
        }

        public static Language fromCode(String code) {
            for (Language language : values()) {
                if (language.code.equals(code)) {
                    return language;
                }
            }
            return null;
        }
    }

    public enum Source {
        AUTO("adman/v2", Language.values()),
        HOUND("adman/hound/v2", new Language[] {
                Language.ENGLISH_US
        }),
        MICROSOFT("adman/microsoft/v2", new Language[]{
                Language.ENGLISH_US,
                Language.ENGLISH_GB,
                Language.RUSSIAN,
                Language.CHINESE,
                Language.FRENCH,
                Language.GERMAN,
                Language.ITALIAN,
                Language.SPANISH,
        }),
        YANDEX("adman/yandex/v2", new Language[] {
                Language.RUSSIAN,
                Language.UKRAINIAN,
                Language.ENGLISH_US,
                Language.TURKISH,
        }),
        NUANCE("adman/nuance/v2", new Language[] {
                Language.RUSSIAN,
                Language.ENGLISH_US,
        }),
        ;

        final public String endpoint;
        final public Language[] languages;

        Source(String endpoint, Language[] languages) {
            this.endpoint = endpoint;
            this.languages = languages;
        }
    }

    private enum State {
        NONE,
        READY,
        PROCESS,
        RESPONSE,
        FAILED,
        SKIP
    }

    final private static int DEFAULT_RESPONSE_TIME = 5;

    private Source source;
    private String languageCode;
    private boolean vad;

    private Context context;
    private VoiceSearch voiceSearch;
    private String lastTranscription;

    private ResponseRunner runner;
    private List<VoiceResponse> responses;

    private String introAudio;
    private Integer responseDelay;
    private Integer responseTime;
    private State state;
    private AudioPlayer introPlayer;

    private TimerTask responseTask;
    private String currentTranscript;

    private AudioPlayer responseMicOnSound;
    private AudioPlayer responseMicOffSound;

    @Override
    public int eventPriority() {
        return super.eventPriority() + 100;
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {PlayerEvent.TYPE, VoiceEvent.TYPE, ControlEvent.TYPE, RequestEvent.TYPE, ModuleEvent.TYPE};
    }

    public AdmanVoice(Context context) {
        this(context, false);
    }

    public AdmanVoice(Context context, boolean debug) {
        this.context = context;
        state = State.NONE;
        source = Source.AUTO;
        languageCode = Language.ENGLISH_US.code;
        vad = false;
        // Turn on debug output. This should be disable for your production code.
        VoiceSearch.setDebug(debug);

    }


    @Override
    public String getId() {
        return ID;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
        if (!Arrays.asList(source.languages).contains(Language.fromCode(languageCode))) {
            setLanguage(source.languages[0]);
        }
    }

    public Language getLanguage() {
        return Language.fromCode(languageCode);
    }

    public void setLanguage(Language language) {
        if (!Arrays.asList(source.languages).contains(language)) {
            throw new IllegalArgumentException(source.name() + " not supported " + language.name());
        }
        this.languageCode = language.code;
    }

    public boolean isVad() {
        return vad;
    }

    public void setVad(boolean vad) {
        this.vad = vad;
    }

    public List<VoiceResponse> getResponses() {
        return responses;
    }

    @Override
    public void onPlayerEvent(PlayerEvent event) {
        final VASTInline ad = getAdman().getCurrentAd();
        if (ad == null) {
            // ToDo: player events after {@link Adman#reset()}
            return;
        }
        switch (event.getType()) {
            case PLAYING:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        onStartAd(ad);
                    }
                }).start();
                if (introAudio != null) {
                    Log.d(TAG, "Intro Audio: " + introAudio);
                    getAdman().getPlayer().setDispatchEnabled(false);
                    getAdman().getPlayer().pause();
                    introPlayer = new AudioPlayer(context, introAudio, true);
                    introPlayer.setName("introPlayer");
                    introPlayer.setStateListener(new AudioPlayer.StateListener() {
                        @Override
                        public void onStateChange(AudioPlayer.State state) {
                            switch (state) {
                                case ERROR:
                                    Log.d(TAG, "Intro Audio failed");
                                case STOPPED:
                                    AudioPlayer player = getAdman().getPlayer();
                                    if (player != null) {
                                        getAdman().getPlayer().resume();
                                        getAdman().getPlayer().setDispatchEnabled(true);
                                    }
                                    introPlayer.dispose();
                                    introPlayer = null;
                            }
                        }
                    });
                }
                break;
            case PROGRESS:
                if (state == State.READY && responseDelay != null && responseDelay < 0) {
                    AudioPlayer player = getAdman().getPlayer();
                    int estimateTime = player.getDuration() - player.getPosition();
                    if (estimateTime <= -responseDelay * 1000) {
                        startResponse();
                    }
                }
                break;
            case COMPLETE:
                if (state == State.READY || state == State.PROCESS) {
                    event.stop();
                    if (state == State.READY) {
                        startResponse();
                    }
                } else {
                    onCompleteAd();
                }
                break;
            case FAILED:
                onCompleteAd();
                break;
        }
    }

    public void pause() {
        if (voiceListener.isRecording) {
            voiceListener.stopSending();
            stopSearch(false);
        } else {
            if (runner != null && runner.pause()) {
                getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.PAUSE));
            }
        }
    }

    public void resume() {
        if (!voiceListener.statusSending()) {
            voiceListener.sendLast();
        } else {
            if (runner != null && runner.resume()) {
                getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.PLAY));
            }
        }
    }

    @Override
    public void onControlEvent(ControlEvent event) {
        switch (event.getType()) {
            case PAUSE:
                pause();
                break;
            case RESUME:
                resume();
                break;
            case SKIP:
                setState(State.SKIP);
                if (runner != null) {
                    boolean isSkip = runner.skip();
                    if (!isSkip && state == State.PROCESS) {
                        onCompleteAd(true);
                        getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
                    }
                }
                break;
            case CLICK:
                if (state != State.NONE) {
                    event.stop();
                    if (state == State.PROCESS) {
                        runAction(VoiceResponse.ACTION_POSITIVE, VoiceResponse.SENDER_BANNER);
                    }
                }
                break;
            case CLICK_POSITIVE:
                if (state == State.PROCESS) {
                    runAction(VoiceResponse.ACTION_POSITIVE, VoiceResponse.SENDER_BUTTON);
                }
                break;
            case CLICK_NEGATIVE:
                if (state == State.PROCESS) {
                    runAction(VoiceResponse.ACTION_NEGATIVE, VoiceResponse.SENDER_BUTTON);
                }
                break;
        }
    }

    @Override
    public void onRequestEvent(RequestEvent event) {
        if (event.getType() == RequestEvent.Type.LOAD) {
            if (context.checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                event.params.put("microphone", "1");
            }
            if (context.checkCallingOrSelfPermission(Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                event.params.put("calendar", "1");
            }
        }
    }

    private void startResponse() {
        responseTask = new TimerTask() {
            @Override
            public void run() {
                stopSearch(false);
            }
        };
        new Timer().schedule(responseTask, (responseTime == null ? DEFAULT_RESPONSE_TIME : responseTime) * 1000);
        startSearch();
    }

    private VoiceResponse getResponse(String action) {
        if (action != null && responses != null) {
            for (VoiceResponse response : responses) {
                if (response.type.equals(action)) {
                    return response;
                }
            }
        }
        return null;
    }

    private void runAction(String action, String sender) {
        stopSearch(true);
        VoiceResponse response = getResponse(action);
        if (response != null) {
            getAdman().getDispatcher().dispatch(new VoiceEvent(VoiceEvent.Type.RESPONSE, currentTranscript, response, sender));
        }
    }

    @Override
    public void onVoiceEvent(VoiceEvent event) {
        switch (event.getType()) {
            case UPDATE:
                break;
            case RESPONSE:
                stopSearch(true);
                if (responseTask != null) responseTask.cancel();
                final VoiceResponse response = event.getResponse();
                responses.remove(response);
                setState(State.RESPONSE);
                Log.d(TAG, String.format("onVoiceEvent, action: %s; currentTranscript: %s", response.action, currentTranscript));
                getAdman().getVASTDispatcher().dispatch("response_" + response.action);
                runner.run(context, event, new Runnable() {
                    @Override
                    public void run() {
                        if (response.isRepeat()) {
                            currentTranscript = null;
                            Log.d(TAG, "onVoiceEvent, startResponse");
                            /*if (state == State.RESPONSE) */startResponse();
                        } else {
                            Log.d(TAG, "onVoiceEvent, runner onCompleteAd");
                            onCompleteAd();
                            getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
                        }
                    }
                });
                break;
            case START:
                if (responseMicOnSound != null) {
                    responseMicOnSound.play();
                }
                break;
            case STOP:
                if (responseMicOffSound != null) {
                    responseMicOffSound.play();
                }
                break;
            case FAIL:
                setState(State.FAILED);

                Log.d(TAG, "onVoiceEvent, action: response_error");
                getAdman().getVASTDispatcher().dispatch("response_error");
                final VoiceResponse response_error = getResponse("error");
                if (response_error != null) {
                    runner.run(context, response_error, null, new Runnable() {
                        @Override
                        public void run() {
                        }
                    });
                }

                getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
                if (responseMicOffSound != null) {
                    responseMicOffSound.play();
                }
                break;
        }
    }

    @Override
    public void onModuleEvent(ModuleEvent event) {
        Log.d(TAG, "ModuleEvent: " +  event);
        switch (event.getType()) {
            case ADMAN_PAUSE:
                pause();
                break;
            case ADMAN_RESUME:
                resume();
                break;
        }
    }

    protected void onStartAd(VASTInline ad) {
        state = State.NONE;
        responses = new ArrayList<>();
        if (ad.extensions.containsKey("response")) {
            try {
                responses = VoiceResponse.fromExtension(ad.extensions.get("response"));
            } catch (XPathExpressionException e) {
                Log.e(TAG, "Failed to parse response", e);
            }
            setState(State.READY);
        }
        Collections.sort(responses, new Comparator<VoiceResponse>() {
            @Override
            public int compare(VoiceResponse r1, VoiceResponse r2) {
                return r2.priority - r1.priority;
            }
        });
        if (ad.extensions.containsKey("IntroAudio")) {
            introAudio = ad.extensions.get("IntroAudio").value;
        }
        if (ad.extensions.containsKey("ResponseTime")) {
            responseTime = Integer.parseInt(ad.extensions.get("ResponseTime").value);
        }
        if (ad.extensions.containsKey("ResponseDelay")) {
            responseDelay = Integer.parseInt(ad.extensions.get("ResponseDelay").value);
        }
        if (ad.extensions.containsKey("ResponseLanguage")) {
            languageCode = ad.extensions.get("ResponseLanguage").value;
        }
        if (ad.extensions.containsKey("ResponseMicOnSound")) {
            responseMicOnSound = new AudioPlayer(
                    getAdman().getContext(),
                    ad.extensions.get("ResponseMicOnSound").value, false
            );
            responseMicOnSound.setNeedToStart(true);
            responseMicOnSound.setName("MicOnSound");

        }
        if (ad.extensions.containsKey("ResponseMicOffSound")) {
            responseMicOffSound = new AudioPlayer(
                    getAdman().getContext(),
                    ad.extensions.get("ResponseMicOffSound").value, false
            );
            responseMicOffSound.setNeedToStart(true);
            responseMicOffSound.setName("MicOffSound");
        }
        runner = new ResponseRunner(getAdman().getSelector(), getAdman().getStorageAction());
    }

    protected void setState(State value) {
        if(value == state) return;
        if(value != State.NONE) {
            state = value;
        }
        else{
            if(state != State.SKIP){
                state = value;
            }
        }
    }

    protected void onCompleteAd() {
        onCompleteAd(false);
    }

    protected void onCompleteAd(boolean abort) {
        state = State.NONE;
        currentTranscript = null;
        if (responseTask != null) {
            responseTask.cancel();
            responseTask = null;
        }
        stopSearch(abort);
        responses = null;
        introAudio = null;
        responseDelay = null;
        responseTime = null;
        responseMicOnSound = null;
        responseMicOffSound = null;
        runner = null;
    }

    public InputStream getAudioInputStream() {
        final InputStream is = new SimpleAudioByteStreamSource();
        final int RECORD_DELAY = 300;
        final int SAMPLE_RATE = 16000;

        final int skipLength = RECORD_DELAY * SAMPLE_RATE / 1000 * 2 /* 16 bits per sample */;

        return new SkippedInputStream(is, skipLength);
    }

    private void startSearch() {
        Log.d(TAG, "startSearch");
        stopSearch(true);
        setState(State.PROCESS);
        getAdman().getDispatcher().dispatch(new ModuleEvent(ModuleEvent.Type.RECORD_START, TAG));
        if (voiceSearch == null) {
            IAdman adman = getAdman();
            final VoiceSearch.Builder builder = new VoiceSearch.Builder();
            VASTInline ad = getAdman().getCurrentAd();
            String endpoint = (ad != null && ad.extensions.containsKey("ResponseUrl"))
                    ? ad.extensions.get("ResponseUrl").value
                    : getAdman().getRequest().region.voiceServer + "/" + source.endpoint + "?language=" + languageCode;
            String ad_id = (ad != null)
                        ? ad.extensions.containsKey("AdId")
                                ? ad.extensions.get("AdId").value
                                : ad.id
                        : null;
            builder.setEndpoint(endpoint);
            builder.setRequestInfo(new RequestModel(
                    1,
                    adman.getRequest().siteId,
                    ad_id,
                    responseDelay.doubleValue(),
                    Loader.getUserAgent(),
                    adman.getUser().advertisingId,
                    vad
            ));
            builder.setAudioSource(getAudioInputStream());
            builder.setServerVadWindow(0);
            builder.setCompressAudio(false);
            builder.setListener(voiceListener);

            voiceSearch = builder.build();
        }
        voiceListener.activeSending();
        voiceSearch.start();
    }

    private void stopSearch(boolean abort) {
        setState(State.NONE);
        if (voiceSearch != null) {
            if (abort) {
                voiceSearch.abort();
            } else {
                voiceSearch.stopRecording();
            }
            voiceSearch = null;
        }
        IAdman adman = getAdman();
        if (adman != null) {
            adman.getDispatcher().dispatch(new ModuleEvent(ModuleEvent.Type.RECORD_STOP, TAG));
        }
        if (lastTranscription != null) {
            //if (lastTranscription.length() > 0) new VoiceStatisticLoader().send(getAdman(), lastTranscription);
            lastTranscription = null;
        }
    }


    private boolean checkSkip() {
        if (state == State.SKIP) {
            Log.d(TAG, "state == SKIP -> COMPLETE");
            onCompleteAd();
            getAdman().getDispatcher().dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
            return true;
        }
        return false;
    }

    @Override
    public void unbind() {
        super.unbind();
        stopSearch(true);
    }

/*
    public void runAction() {
        receiver.adman = getAdman();
        receiver.run(context);
    }
/**/

    private VoiceSearchListenerImpl voiceListener = new VoiceSearchListenerImpl();
    class VoiceSearchListenerImpl implements VoiceSearchListener {
        final private String TAG_EVENT = TAG + ".event";
        private boolean isRecording;

        private boolean stateSending = true;
        private VoiceEvent.Type lastEventType;
        private String lastText;
        private String lastAction;

        @Override
        public void onTranscriptionUpdate(TranscriptModel transcriptMessage) {
            String text = transcriptMessage.getTranscript();
            if (text != null && state == State.PROCESS) {
                text = text.toLowerCase();
                if (!text.equals(lastTranscription)) {
                    lastTranscription = text;
                    Log.d(TAG_EVENT, "onTranscriptionUpdate: " + text);
                    doEvent(VoiceEvent.Type.UPDATE, text);
                }
            }
        }

        @Override
        public void onResponse(ResponseModel responseMessage, VoiceSearchInfo info) {
            Log.d(TAG_EVENT, "onResponse");
            if(checkSkip()){
                return;
            }
            if (isRecording) {
                doEvent(VoiceEvent.Type.STOP, null);
            }
            String action = responseMessage.getAction();
            currentTranscript = responseMessage.getTranscript();
            Log.d(TAG_EVENT, String.format("onResponse, action: %s; currentTranscript: %s", action, currentTranscript));
            doAction(action);
        }

        @Override
        public void onError(Throwable e, VoiceSearchInfo info) {
            Log.e(TAG_EVENT, "onError: " + e.toString());
            doEvent(VoiceEvent.Type.FAIL, null);
        }

        @Override
        public void onAbort(VoiceSearchInfo info) {
            Log.e(TAG_EVENT, "onAbort");
            doEvent(VoiceEvent.Type.STOP, null);
        }

        @Override
        public void onRecordingStarted() {
            Log.d(TAG_EVENT, "onRecordingStarted");
            if(checkSkip()){
                return;
            }
            doEvent(VoiceEvent.Type.START, null);
        }

        @Override
        public void onRecordingStopped() {
            Log.d(TAG_EVENT, "onRecordingStopped");
            doEvent(VoiceEvent.Type.STOP, null);
        }

        /**
         * util methods
         */
        public boolean isRecording() {
            return isRecording;
        }

        public boolean statusSending() {
            return stateSending;
        }

        public void activeSending() {
            stateSending = true;
        }

        public void stopSending() {
            stateSending = false;
        }

        public void sendLast() {
            stateSending = true;
            if (lastAction != null){
                runAction(lastAction, null);
                lastAction = null;
            }
            else if (lastEventType != null) {
                getAdman().getDispatcher().dispatch(new VoiceEvent(lastEventType, lastText, null));
                lastEventType = null;
            }
        }

        private void doEvent(VoiceEvent.Type type, String transcript) {
            lastText = transcript;
            lastEventType = type;
            if (type == VoiceEvent.Type.STOP ||
                    type == VoiceEvent.Type.FAIL) {
                isRecording = false;
            } else if (type == VoiceEvent.Type.START) {
                isRecording = true;
            }
            if (stateSending) {
                getAdman().getDispatcher().dispatch(new VoiceEvent(type, transcript, null));
            }
        }
        private void doAction(String action) {
            if (stateSending) {
                runAction(action, null);
            } else  {
                lastAction = action;
            }
        }
    }
}
