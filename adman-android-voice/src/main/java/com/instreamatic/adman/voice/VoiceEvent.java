package com.instreamatic.adman.voice;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public class VoiceEvent extends BaseEvent<VoiceEvent.Type, VoiceEvent.Listener> {

    final public static EventType<Type, VoiceEvent, Listener> TYPE = new EventType<Type, VoiceEvent, Listener>("voice") {
        @Override
        public void callListener(VoiceEvent event, Listener listener) {
            listener.onVoiceEvent(event);
        }
    };

    public enum Type {
        START,
        UPDATE,
        RESPONSE,
        FAIL,
        STOP
    }

    final private String transcript;
    final private VoiceResponse response;
    final private String sender;

    public VoiceEvent(Type type) {
        this(type, null, null, null);
    }

    public VoiceEvent(Type type, String transcript, VoiceResponse response) {
        this(type, transcript, response, null);
    }

    public VoiceEvent(Type type, String transcript, VoiceResponse response, String sender) {
        super(type);
        this.transcript = transcript;
        this.response = response;
        this.sender = sender;
    }

    public String getTranscript() {
        return transcript;
    }

    public VoiceResponse getResponse() {
        return response;
    }

    public String getSender() { return sender; }

    @Override
    public EventType<Type, VoiceEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onVoiceEvent(VoiceEvent event);
    }
}
