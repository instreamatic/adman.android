package com.instreamatic.adman.voice;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.M)
public class RecordAudioPermission {

    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    static public boolean isSupported() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    static public boolean isGranted(Context context) {
        return context.checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    static public void request(Activity activity) {
        request(activity, false);
    }

    static public void request(Activity activity, boolean force) {
        if (!isGranted(activity)) {

            // Should we show an explanation?
            if (activity.shouldShowRequestPermissionRationale(Manifest.permission.RECORD_AUDIO) && !force) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                activity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public static boolean checkResult(int requestCode, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    return true;
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    return false;
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
        return false;
    }
}
