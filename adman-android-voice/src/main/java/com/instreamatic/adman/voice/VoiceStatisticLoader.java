package com.instreamatic.adman.voice;

import com.instreamatic.adman.*;
import com.instreamatic.adman.statistic.LiveStatisticLoader;

import java.util.Map;

public class VoiceStatisticLoader extends LiveStatisticLoader {

    public void send(IAdman adman, String text) {
        Map<String, String> data = buildData(adman, adman.getRequest());
        data.put("text", text);
        Region region = adman.getRequest().region;
        POST(region.statServer + "/voice", data, defaultCallback);
    }
}
