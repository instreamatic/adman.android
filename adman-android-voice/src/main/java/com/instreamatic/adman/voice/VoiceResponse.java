package com.instreamatic.adman.voice;

import com.instreamatic.vast.VASTExtension;
import com.instreamatic.vast.VASTParser;
import com.instreamatic.vast.model.VASTCalendar;
import com.instreamatic.vast.model.VASTMedia;
import com.instreamatic.vast.model.VASTTrackingEvent;
import com.instreamatic.vast.utils.NodeListWrapper;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;


public class VoiceResponse {

    public static class Value<T> {

        final public String type;
        final public T value;

        public Value(String type, T value) {
            this.type = type;
            this.value = value;
        }

        public String toString() {
            return type;
        }

    }

    public static class TextValue extends Value<String> {
        public TextValue(String type, String value) {
            super(type, value);
        }
    }

    public static class MediaValue extends Value<List<VASTMedia>> {
        public MediaValue(List<VASTMedia> value) {
            super(MEDIA, value);
        }
    }

    public static class TrackingValue extends Value<List<VASTTrackingEvent>> {
        public TrackingValue(List<VASTTrackingEvent> value) {
            super(TRACKING, value);
        }
    }

    public static class CalendarValue extends Value<VASTCalendar> {
        public CalendarValue(VASTCalendar value) {
            super(CALENDAR, value);
        }
    }

    final public static String ACTION_POSITIVE = "positive";
    final public static String ACTION_NEGATIVE = "negative";

    final public static String LINK = "link";
    final public static String PHONE = "phone";
    final public static String SMS = "phone+text";
    final public static String AUDIO = "audio";
    final public static String REPEAT = "repeat";
    final public static String MEDIA = "media";
    final public static String TRACKING = "tracking_events";
    final public static String CALENDAR = "calendar";

    final public static String SENDER_BUTTON = "button";
    final public static String SENDER_BANNER = "banner";

    final public String action;
    final public String type;
    final public Value[] values;
    final public int priority;

    public VoiceResponse(String action ,String type, Value[] values, int priority) {
        this.action = action;
        this.type = type;
        this.values = values;
        this.priority = priority;
    }

    public boolean isIntent() {
        for (Value value : values) {
            switch (value.type) {
                case LINK:
                case PHONE:
                case SMS:
                    return true;
            }
        }
        return false;
    }

    public boolean isRepeat() {
        for (Value value : values) {
            switch (value.type) {
                case REPEAT:
                    return true;
            }
        }
        return false;
    }

    public String toString() {
        String str_values = "";
        for (Value value : values) {
            str_values += value.toString() + " ";
        }
        return String.format("%s; %s; %s", action, type, str_values);
    }

    public static Value parseValue(Node valueNode) throws XPathExpressionException {
        String type = valueNode.getAttributes().getNamedItem("type").getNodeValue();
        switch (type) {
            case MEDIA:
                return new MediaValue(VASTParser.extractMedias(XPathFactory.newInstance().newXPath(), valueNode));
            case TRACKING:
                return new TrackingValue(VASTParser.extractEvents(XPathFactory.newInstance().newXPath(), valueNode));
            case CALENDAR:
                return new CalendarValue(VASTParser.extractCalendar(XPathFactory.newInstance().newXPath(), valueNode));
            default:
                return new TextValue(type, valueNode.getTextContent());
        }
    }

    public static List<VoiceResponse> fromExtension(VASTExtension extension) throws XPathExpressionException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        NodeList responsesNodes = (NodeList) xpath.evaluate("Responses/Response", extension.node, XPathConstants.NODESET);
        List<VoiceResponse> responses = new ArrayList<>();
        for (Node responseNode : new NodeListWrapper(responsesNodes)) {
            String action = responseNode.getAttributes().getNamedItem("action").getNodeValue();
            String type = responseNode.getAttributes().getNamedItem("type").getNodeValue();
            String priorityStr = responseNode.getAttributes().getNamedItem("priority").getNodeValue();
            int priority = priorityStr != null ? Integer.parseInt(priorityStr) : 0;
            NodeList valuesNodes = (NodeList) xpath.evaluate("Values/Value", responseNode, XPathConstants.NODESET);
            List<Value> values = new ArrayList<>();
            for (Node valueNode : new NodeListWrapper(valuesNodes)) {
                values.add(parseValue(valueNode));
            }
            Node valueNode = (Node) xpath.evaluate("Value", responseNode, XPathConstants.NODE);
            if (valueNode != null) {
                values.add(parseValue(valueNode));
            }
            responses.add(new VoiceResponse(action, type, values.toArray(new Value[values.size()]), priority));
        }
        return responses;
    }
}
