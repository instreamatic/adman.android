package com.instreamatic.voice.message;

import com.instreamatic.voice.core.model.ErrorModel;
import com.instreamatic.voice.core.model.Model;
import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class JsonMessage extends StringMessage {

    final public static String ERROR = "error";
    final public static String AUDIO_STOP = "audio.stop";
    final public static String VOICE_TRANSCRIPT = "voice.transcript";
    final public static String VOICE_RESULT = "voice.result";

    protected Object model;

    public JsonMessage(Map<String, String> headers, String data) throws JSONException {
        super(headers, data);
        model = parseModel();
    }

    private Object parseModel() throws JSONException {
        JSONObject json = new JSONObject(data);
        Model model = null;
        switch (getPath()) {
            case ERROR:
                model = new ErrorModel();
                break;
            case VOICE_TRANSCRIPT:
                model = new TranscriptModel();
                break;
            case VOICE_RESULT:
                model = new ResponseModel();
                break;
        }
        if (model != null) {
            model.fromJSON(json);
        }
        return model;
    }

    public String getPath() {
        return headers.get("Path");
    }

    public <T> T getModel(Class<T> modelClass) {
        if (modelClass.isInstance(model)) {
            return modelClass.cast(model);
        }
        return null;
    }

    public static JsonMessage parse(String data) throws JSONException {
        int split = data.indexOf(CRLF + CRLF);
        String headersData = data.substring(0, split);
        Map<String, String> headers = parseHeaders(headersData);
        return new JsonMessage(headers, data.substring(split + 4));
    }
}
