package com.instreamatic.voice.message;

import com.instreamatic.voice.core.model.Model;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class MessageBuilder {

    public enum AudioType {
        SPEEX("audio/speex"),
        WAV("audio/wav");

        final public String header;

        AudioType(String header) {
            this.header = header;
        }

        public static AudioType resolve(boolean compressed) {
            if (compressed) {
                return AudioType.SPEEX;
            } else {
                return AudioType.WAV;
            }
        }
    }

    public static StringMessage json(String path, Model model) throws JSONException {
        String data =  model.toJSON().toString(4);
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Path", path);
        return new StringMessage(headers, data);
    }

    public static BytesMessage audio(byte[] audio, AudioType type) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", type.header);
        headers.put("Path", "audio.data");
        return new BytesMessage(headers, audio);
    }

    public static BytesMessage audioEnd(AudioType type) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", type.header);
        headers.put("Path", "audio.end");
        return new BytesMessage(headers, new byte[0]);
    }
}
