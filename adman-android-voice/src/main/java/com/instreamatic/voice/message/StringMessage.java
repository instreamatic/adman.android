package com.instreamatic.voice.message;

import org.json.JSONException;

import java.io.IOException;
import java.util.Map;

public class StringMessage extends Message<String> {

    public StringMessage(Map<String, String> headers, String data) {
        super(headers, data);
    }

    @Override
    public String pack() {
        String headersData = packHeaders(headers);
        return headersData + data;
    }

    public static StringMessage parse(String data) throws JSONException {
        int split = data.indexOf(CRLF + CRLF);
        String headersData = data.substring(0, split);
        Map<String, String> headers = parseHeaders(headersData);
        return new StringMessage(headers, data.substring(split + 4));
    }
}
