package com.instreamatic.voice.message;

import java.util.Map;
import java.util.TreeMap;


abstract public class Message<D> implements IMessage<D> {
    final static String CRLF = "\r\n";

    protected Map<String, String> headers;
    protected D data;

    public Message(Map<String, String> headers, D data) {
        this.headers = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        this.headers.putAll(headers);
        this.data = data;
    }

    static Map<String, String> parseHeaders(String headersData) {
        Map<String, String> result = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        for (String h : headersData.split(CRLF)) {
            int p = h.indexOf(":");
            result.put(h.substring(0, p).trim(), h.substring(p + 1).trim());
        }
        return result;
    }

    static String packHeaders(Map<String, String> headers) {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            result.append(entry.getKey());
            result.append(":");
            result.append(entry.getValue());
            result.append(CRLF);
        }
        result.append(CRLF);
        return result.toString();
    }
}
