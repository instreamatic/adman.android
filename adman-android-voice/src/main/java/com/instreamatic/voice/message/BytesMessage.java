package com.instreamatic.voice.message;

import com.instreamatic.voice.core.util.Utils;

import java.util.Arrays;
import java.util.Map;

public class BytesMessage extends Message<byte[]> {

    public BytesMessage(Map<String, String> headers, byte[] data) {
        super(headers, data);
    }

    public static BytesMessage parse(byte[] data) {
        int headerSize = (data[0] << 8) + data[1];
        String headersData = new String(Arrays.copyOfRange(data, 2, headerSize + 2));
        Map<String, String> headers = parseHeaders(headersData);
        return new BytesMessage(headers, Arrays.copyOfRange(data, headerSize + 4, data.length));
    }

    @Override
    public byte[] pack() {
        byte[] headersData = packHeaders(headers).getBytes();
        byte[] headerSize = new byte[] {(byte)(headersData.length >>> 8), (byte) headersData.length};
        return Utils.concatAll(headerSize, headersData, data);
    }
}
