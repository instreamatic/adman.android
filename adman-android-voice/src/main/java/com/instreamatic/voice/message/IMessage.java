package com.instreamatic.voice.message;

public interface IMessage<D> {
    public D pack();
}
