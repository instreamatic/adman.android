package com.instreamatic.voice.android.sdk.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.CalendarContract;

import com.instreamatic.vast.model.VASTCalendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarUtils {
    public static boolean addEvent(Context context, VASTCalendar calendar) {
        boolean res = false;
        try {
            Date beginTime = stringToDate(calendar.begin);
            Date endTime = stringToDate(calendar.end);

            ContentValues values = new ContentValues();
            values.put(CalendarContract.Events.TITLE, calendar.name);
            values.put(CalendarContract.Events.DESCRIPTION, calendar.description);
            values.put(CalendarContract.Events.CALENDAR_ID, Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ? 3 : 1);
            values.put(CalendarContract.Events.DTSTART, beginTime.getTime());
            values.put(CalendarContract.Events.DTEND, endTime.getTime());
            if (calendar.timezone.isEmpty()) {
                TimeZone tz = TimeZone.getDefault();
                values.put(CalendarContract.Events.EVENT_TIMEZONE, tz.getID());
            } else {
                values.put(CalendarContract.Events.EVENT_TIMEZONE, calendar.timezone);
            }

            ContentResolver cr = context.getContentResolver();
            cr.insert(CalendarContract.Events.CONTENT_URI, values);
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean addEvent2(Context context, VASTCalendar calendar) {
        boolean res = false;
        try {
            Date beginTime = stringToDate(calendar.begin);
            Date endTime = stringToDate(calendar.end);

            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTime())
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTime())
                    .putExtra(CalendarContract.Events.EVENT_TIMEZONE, calendar.timezone)
                    .putExtra(CalendarContract.Events.TITLE, calendar.name)
                    .putExtra(CalendarContract.Events.DESCRIPTION, calendar.description)
                    .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
            context.startActivity(intent);
            res = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static int findEvent(Context context, VASTCalendar calendar) {
        Cursor cur = null;
        int count = 0;
        try {
            Date beginTime = stringToDate(calendar.begin);
            Date endTime = stringToDate(calendar.end);
            ContentResolver cr = context.getContentResolver();

            String[] mProjection =
                    {
                            "_id",
                            CalendarContract.Events.DTSTART,
                            CalendarContract.Events.DTEND,
                            CalendarContract.Events.TITLE,
                    };

            String selection = "((" + CalendarContract.Events.DTSTART + " >= ?) AND ("
                    + CalendarContract.Events.DTSTART + " <= ?) AND ("
                    + CalendarContract.Events.TITLE + " = ?))";

            String[] selectionArgs = new String[]
                    {
                            Long.toString(beginTime.getTime()),
                            Long.toString(endTime.getTime()),
                            calendar.name
                    };

            cur = cr.query(CalendarContract.Events.CONTENT_URI, mProjection, selection, selectionArgs, null);
            count = cur == null ? 0 : cur.getCount();
        } catch (Exception e) {
            count = -1;
            e.printStackTrace();
        } finally {
            if (cur != null) cur.close();
        }
        return count;
    }

    public static Date stringToDate(String value) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(value);
    }

}