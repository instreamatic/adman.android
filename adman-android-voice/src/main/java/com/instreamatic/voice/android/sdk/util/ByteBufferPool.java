package com.instreamatic.voice.android.sdk.util;


import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.LinkedList;


/**
 * Maintains a set of byte buffers in groups of multiples of two used for
 * audio buffers (or whatever you want).  When you request a buffer from
 * getBuffer, you will get an AudioBuffer with a buf at least as big as
 * minSize, up to the next power of two.  When you release a buffer, it will
 * go back into the pool for the next call to getBuffer().
 *
 * TODO: Use weak references to avoid excessive memory use.
 */

public class ByteBufferPool {

    private static ByteBufferPool instance;

    private static final int MIN_BUFF_CAPACITY = 256;
    private static final int MAX_BUFF_PER_SIZE = 5;

    // maximum size of the whole BufferPool (1mb)
    private static final int MAX_SIZE = 1000 * 1024;

    private final int minBufferSize;
    private final int maxNumPerSize;
    private int totalSize = 0;
    private final LinkedHashMap<Integer, LinkedList<ByteBuffer>> pool = new LinkedHashMap<>();

    public static ByteBufferPool getInstance() {
        if (instance == null) {
            instance = new ByteBufferPool(MIN_BUFF_CAPACITY, MAX_BUFF_PER_SIZE);
        }
        return instance;
    }

    /**
     * new BufferPool with buffers of capacity at least minBufferSize and at most maxPerSize
     * @param minBufferSize  minimum Buffer Size in this BufferPool
     * @param maxPerSize    maximum number of buffers per size
     */
    private ByteBufferPool(final int minBufferSize, final int maxPerSize) {
        this.minBufferSize = minBufferSize;
        this.maxNumPerSize = maxPerSize;
    }

    /**
     * Get buffer of at least minSize
     * @param minSize
     * @return a ByteBuffer with capacity in powers of 2 greater than minSize
     */
    public synchronized ByteBuffer getBuffer(final int minSize) {
        int mult = nextPow2(minSize);
        if (mult < minBufferSize) {
            mult = minBufferSize;
        }

        LinkedList<ByteBuffer> ar = pool.get(mult);
        if (ar == null) {
            ar = new LinkedList<>();
            pool.put(mult, ar);
        }

        ByteBuffer abuf;
        if (ar.size() > 0) {
            abuf = ar.removeFirst();
            totalSize -= mult;
        }
        else {
            abuf = ByteBuffer.allocate(mult);
        }
        return abuf;
    }


    /**
     * Release the ByteBuffer and put it back into the pool, evict any older buffer if the size exceeds MAX_SIZE
     * @param buffer
     */
    public synchronized void releaseBuffer(final ByteBuffer buffer) {
        final int size = buffer.capacity();
        // Only release if buf length is a power of 2
        if ((size & size - 1) == 0) {
            buffer.clear();

            LinkedList<ByteBuffer> ar = pool.get(size);
            if (ar == null) {
                ar = new LinkedList<>();
                pool.put(size, ar);
            }

            // add to the list if the max number of available buffer is not reached.
            if (ar.size() < maxNumPerSize) {
                ar.add(buffer);
                totalSize += size;

                // if we hold too many byte arrays, release a few LRU until the size is smaller
                if (totalSize > MAX_SIZE) {
                    evictEldest();
                }
            }
        }
    }

    /**
     * Remove unused buffer from the list until total size of the buffer is less than the MAX_SIZE.
     * The removal starts with the earliest inserted buffer size.
     */
    private void evictEldest() {
        for (final Integer bufferSize : pool.keySet()) {
            final LinkedList<ByteBuffer> bufferList = pool.get(bufferSize);
            if (bufferList != null) {
                while (totalSize > MAX_SIZE && !bufferList.isEmpty()) {
                    bufferList.removeFirst();
                    totalSize -= bufferSize;
                }
            }

            if (totalSize <= MAX_SIZE) {
                break;
            }
        }
    }

    /**
     * Next power of 2 integer
     * @param min   minimum integer to return
     * @return
     */
    private int nextPow2(int min) {
        min--;
        for (int i = 1; i < 32; i <<= 1) {
            min = min | min >> i;
        }
        return min + 1;
    }

}
