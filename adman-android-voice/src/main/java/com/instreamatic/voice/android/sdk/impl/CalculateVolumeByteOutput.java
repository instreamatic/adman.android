package com.instreamatic.voice.android.sdk.impl;

import com.instreamatic.voice.android.sdk.bytesplitter.ByteOutput;
import com.instreamatic.voice.android.sdk.util.ByteBufferPool;
import com.instreamatic.voice.android.sdk.util.Pcm;

import java.nio.ByteBuffer;

public class CalculateVolumeByteOutput implements ByteOutput {

    private volatile int volume;

    @Override
    public void onStart() {

    }

    @Override
    public void onBytes(final ByteBuffer byteBuffer) {
        volume = Pcm.computeVolume(byteBuffer.array(), byteBuffer.position(), byteBuffer.limit());
        ByteBufferPool.getInstance().releaseBuffer(byteBuffer);
    }

    @Override
    public void onStop() {

    }

    public int getVolume() {
        return volume;
    }
}
