package com.instreamatic.voice.android.sdk.impl.connection;

import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import com.instreamatic.voice.android.sdk.bytesplitter.ByteOutput;
import com.instreamatic.voice.android.sdk.util.ByteBufferPool;
import com.instreamatic.voice.core.model.ErrorModel;
import com.instreamatic.voice.core.model.RequestModel;
import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;
import com.instreamatic.voice.message.BytesMessage;
import com.instreamatic.voice.message.JsonMessage;
import com.instreamatic.voice.message.MessageBuilder;
import com.instreamatic.voice.message.StringMessage;
import okhttp3.*;
import okio.ByteString;
import org.json.JSONException;

import java.net.ConnectException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.*;



public class WebsocketVoiceConnection implements VoiceConnection {

    private final String LOG_TAG = "websocket";


    private static final long ABSOLUTE_TIMEOUT = 30; // For the entire connection

    private final Uri uri;
    private final RequestModel request;
    private final MessageBuilder.AudioType audioType;

    private BlockingQueue<ByteBuffer> inputQueue = new LinkedBlockingQueue<>();

    private volatile boolean running = false;

    private Listener listener;

    private final Object threadLock = new Object();
    private ConnectThread connectThread;

    private final HandlerThread handlerThread;
    private final Handler handler;

    private volatile boolean audioStopped;


    public WebsocketVoiceConnection(final VoiceConnectionConfig config) {
        this.uri = config.getEndpoint();
        this.request = config.getRequestInfo();
        this.audioType = MessageBuilder.AudioType.resolve(config.isCompressAudio());
        this.handlerThread = new HandlerThread("Websocket Timeout");
        this.handlerThread.start();
        this.handler = new Handler(handlerThread.getLooper());
    }

    @Override
    public BlockingQueue<ByteBuffer> getAudioDataInputQueue() {
        return inputQueue;
    }

    @Override
    public void setListener(final Listener listener) {
        this.listener = listener;
    }

    @Override
    public void start() {
        synchronized (threadLock) {
            Log.d(LOG_TAG, "start()");
            running = true;
            connectThread = new ConnectThread();
            connectThread.start();
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public void stop() {
        Log.d(LOG_TAG, "stop()");
        running = false;
        if ( handlerThread.getLooper() != null ) {
            handlerThread.getLooper().quit();
        }

        synchronized (threadLock) {
            if (connectThread != null) {
                connectThread.interrupt();
                connectThread = null;
            }
        }
    }

    private class ConnectThread extends Thread {

        private final WebsocketCallbackListener websocketListener = new WebsocketCallbackListener();

        private final Executor writeExecutor = Executors.newSingleThreadExecutor();

        @Override
        public void run() {
            // The absolute timeout
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    callTimeoutListener();
                }
            }, ABSOLUTE_TIMEOUT * 1000);

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(ABSOLUTE_TIMEOUT, TimeUnit.SECONDS)
                    .build();

            //final AsyncHttpPost httpPost = new AsyncHttpPost(uri);
            //WebSocket webSocket = null;
            Request request = new Request.Builder()
                    .url( uri.toString() )
                    .build();

            client.newWebSocket(request, websocketListener);

            // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
            client.dispatcher().executorService().shutdown();
        }

        private void sendRequestInfo(final WebSocket webSocket) throws JSONException {
            StringMessage message = MessageBuilder.json("request", request);
            webSocket.send(message.pack());
        }

        private void sendAudioData(final WebSocket webSocket) throws InterruptedException {
            Log.d( LOG_TAG, "Entering sendAudioData()" );
            while (!isInterrupted() && running && !audioStopped) {

                final ByteBuffer buffer = inputQueue.take();
                if (buffer == ByteOutput.STOP) {
                    Log.d( LOG_TAG, "sendAudioData() got ByteOutput.STOP" );
                    break;
                }
                else {
                    buffer.rewind();
                    byte[] data = Arrays.copyOfRange(buffer.array(), 0, buffer.limit());
                    BytesMessage message = MessageBuilder.audio(data, audioType);
                    webSocket.send(ByteString.of(message.pack()));
                    ByteBufferPool.getInstance().releaseBuffer(buffer);
                }
            }

            Log.d( LOG_TAG, "Sending end of data" );

            //webSocket.sendMessage(RequestBody.create(BINARY, MessageBuilder.audioEnd().pack()));
            webSocket.send(ByteString.of(MessageBuilder.audioEnd(audioType).pack()));
        }


        private class WebsocketCallbackListener extends WebSocketListener {

            private WebSocket webSocket = null;

            /**
             * Called to set the web socket being used so it can be closed when the response is received.
             *
             * @param webSocket
             */
            public void setWebSocket(WebSocket webSocket) {
                this.webSocket = webSocket;
            }

            private void closeConnection() {
                // Close the connection once we get a result
                if( webSocket != null ) {
                    webSocket.close(1000, "");
                    webSocket = null;
                }
            }

            @Override
            public void onOpen(final WebSocket webSocket, Response response) {
                this.webSocket = webSocket;
                writeExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            sendRequestInfo( webSocket );
                            sendAudioData( webSocket );
                        }
                        catch (final InterruptedException ex) {
                            // nothing just move along
                            if ( webSocket != null ) {
                                closeConnection();
                            }
                        }
                        catch (Exception e) {
                            System.err.println("Unable to send messages: " + e.getMessage());
                        }

                    }
                });
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                try {
                    JsonMessage message = JsonMessage.parse(text);
                    Log.d( LOG_TAG, "onMessage() received: " + message.pack());
                    switch (message.getPath()) {
                        case JsonMessage.AUDIO_STOP:
                            listener.onAudioStop();
                            audioStopped = true;
                            break;
                        case JsonMessage.ERROR:
                            String error = message.getModel(ErrorModel.class).getError();
                            listener.onConnectionError(error, new ConnectException(error));
                            break;
                        case JsonMessage.VOICE_TRANSCRIPT:
                            listener.onPartialTranscript(message.getModel(TranscriptModel.class));
                            break;
                        case JsonMessage.VOICE_RESULT:
                            listener.onResponse(message.getModel(ResponseModel.class), "");
                            closeConnection();
                            break;
                    }
                } catch (JSONException e) {
                    System.err.println("Unable to receive message: " + e.getMessage());
                }
            }

            @Override
            public void onMessage(WebSocket webSocket, ByteString bytes) {
                BytesMessage message = BytesMessage.parse(bytes.toByteArray());
                Log.d( LOG_TAG, "onMessage() received: " + message);
            }

            @Override
            public void onClosing(WebSocket webSocket, int code, String reason) {

            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                Log.e( LOG_TAG, "onClose() received");
                closeConnection();
            }

            @Override
            public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                Log.e(LOG_TAG, "onFailure", t);
                callErrorListener("WebSocket Error", t);
            }
        }
    }

    private void callTimeoutListener() {
        if (listener != null && running) {
            stop();
            listener.onConnectionTimeout();
        }
    }

    private void callErrorListener(final String message, final Throwable ex) {
        if (listener != null && running) {
            stop();
            listener.onConnectionError(message, ex);
        }
    }
}
