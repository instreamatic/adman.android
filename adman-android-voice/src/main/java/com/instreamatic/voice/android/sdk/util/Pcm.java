package com.instreamatic.voice.android.sdk.util;

public class Pcm {

    /**
     * Computes the average volume of a buffer of PCM data, returns a number
     * from 0 to 100.  The PCM must be 16 bit little-endian.  This will take
     * up to 128 sample distributed over the buffer and compute based on the
     * RMS power of those samples.
     *
     * @param pcm
     *     the input pcm
     * @param offset
     *     the offset of the first pcm sample in bytes from the beginning of the array
     * @param length
     *     the number of bytes to process in the array
     * @return
     *     the average volume of the buffer expressed as a number between 0 and 100
     */

    public static int computeVolume(final byte[] pcm, final int offset, final int length) {
        if (length <= 1) {
            return 0;
        }

        long sqsum = 0;
        int samples = 0;

        // Compute sample increment over the buffer
        int inc = pcm.length / 128;
        if (inc % 2 != 0) {
            inc--;  // Align on 16 bit sample boundary
        }
        if (inc == 0) {
            inc = 2;  // Don't loop infinitely on small buffers
        }
        for (int i = offset; i < length - 1; i += inc) {
            sqsum += Math.pow(Math.abs(pcm[i] | pcm[i+1] << 8), 2);
            samples++;
        }
        final double rms = Math.sqrt(sqsum / samples);
        final int percent_est = (int) Math.pow(rms, 0.5);
        return Math.min(percent_est, 100);
    }

}
