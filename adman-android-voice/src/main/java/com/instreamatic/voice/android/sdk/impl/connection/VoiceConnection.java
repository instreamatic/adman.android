package com.instreamatic.voice.android.sdk.impl.connection;

import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;

import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;


public interface VoiceConnection {

    interface Listener {
        void onAudioStop();

        void onConnectionError(final String message, final Throwable e);

        void onConnectionTimeout();

        void onPartialTranscript(final TranscriptModel partialTranscription);

        void onResponse(final ResponseModel response, final String rawResponse);
    }

    void setListener(final Listener listener);

    void start();

    BlockingQueue<ByteBuffer> getAudioDataInputQueue();

    boolean isRunning();

    void stop();

}
