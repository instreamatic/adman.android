package com.instreamatic.voice.android.sdk.bytesplitter;

import android.os.Process;
import com.instreamatic.voice.android.sdk.util.ByteBufferPool;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;

public class ByteSplitter {

    public interface ErrorListener {
        void onInputError(final Exception exception);
    }

    private final InputStream input;
    private final Set<ByteOutputThread> outputs = new HashSet<>();
    private final ErrorListener errorListener;

    private byte[] readBuffer = new byte[1024];
    private volatile boolean running = false;

    private ByteSplitter(final Builder builder) {
        errorListener = builder.errorListener;
        input = builder.input;
        for (final ByteOutput byteOutput : builder.byteOutputs) {
            outputs.add(new ByteOutputThread(byteOutput));
        }
    }

    public void start() {
        running = true;

        for (final ByteOutputThread output : outputs) {
            output.start();
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);

                try {
                    pump();
                }
                catch (final IOException e) {
                    if (errorListener != null) {
                        errorListener.onInputError(e);
                    }
                }
            }
        }).start();
    }

    public boolean isReading() {
        return running;
    }

    public void stopReading() {
        running = false;
    }

    public void stopAll() {
        running = false;
        stopOutputs();
    }

    private void pump() throws IOException {
        while (running) {
            final int bytesRead = input.read(readBuffer);
            if (bytesRead < 0) {
                break;
            }
            else if (bytesRead > 0) {
                distributeBytes(readBuffer, bytesRead);
            }
        }

        input.close();
        for (final ByteOutputThread output : outputs) {
            output.getInputQueue().offer(ByteOutputThread.STOP);
        }
    }

    private void distributeBytes(final byte[] bytes, final int count) {
        for (final ByteOutputThread output : outputs) {
            final ByteBuffer bufferCopy = ByteBufferPool.getInstance().getBuffer(count);
            bufferCopy.put(bytes, 0, count);
            bufferCopy.limit(bufferCopy.position());
            bufferCopy.rewind();
            output.getInputQueue().offer(bufferCopy);
        }
    }

    private void stopOutputs() {
        for (final ByteOutputThread output : outputs) {
            output.interrupt();
        }
    }

    public static class Builder {
        private ErrorListener errorListener;
        private final InputStream input;
        private final Set<ByteOutput> byteOutputs = new HashSet<>();

        public Builder(final InputStream input) {
            this.input = input;
        }

        public Builder output(final ByteOutput byteOutput) {
            byteOutputs.add(byteOutput);
            return this;
        }

        public Builder errorListener(final ByteSplitter.ErrorListener errorListener) {
            this.errorListener = errorListener;
            return this;
        }

        public ByteSplitter build() {
            return new ByteSplitter(this);
        }
    }
}