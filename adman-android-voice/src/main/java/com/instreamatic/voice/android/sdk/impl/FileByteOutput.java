package com.instreamatic.voice.android.sdk.impl;

import com.instreamatic.voice.android.sdk.bytesplitter.ByteOutput;
import com.instreamatic.voice.android.sdk.util.ByteBufferPool;
import com.instreamatic.voice.java.audio.WavHeaderFactory;
import com.instreamatic.voice.java.audio.WavHeaderUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class FileByteOutput implements ByteOutput {

    public interface ErrorListener {
        void onException(final Exception e);
    }

    private final File outputFile;
    private final int sampleRate;
    private final ErrorListener errorListener;

    private FileOutputStream fos;

    public FileByteOutput(final File outputFile, final int sampleRate, final ErrorListener errorListener) {
        this.outputFile = outputFile;
        this.sampleRate = sampleRate;
        this.errorListener = errorListener;
    }

    @Override
    public void onStart() {
        try {
            fos = new FileOutputStream(outputFile);
            fos.write(WavHeaderUtil.write(WavHeaderFactory.createPCM16MonoHeader(sampleRate)));
        }
        catch (final WavHeaderUtil.WavHeaderFactoryException | IOException e) {
            errorListener.onException(e);
        }
    }

    @Override
    public void onBytes(final ByteBuffer byteBuffer) {
        try {
            fos.write(byteBuffer.array(), 0, byteBuffer.limit());
        }
        catch (final IOException e) {
            errorListener.onException(e);
        }
        finally {
            ByteBufferPool.getInstance().releaseBuffer(byteBuffer);
        }
    }

    @Override
    public void onStop() {
        try {
            fos.flush();
            fos.close();
        }
        catch (final IOException e) {
            errorListener.onException(e);
        }
    }
}
