package com.instreamatic.voice.android.sdk;

import android.os.SystemClock;
import com.instreamatic.voice.android.sdk.util.Beta;

/**
 * Model that contains various information about the voice search.
 *
 * All timestamps are derived from {@link SystemClock#elapsedRealtime()}.
 *
 * This object is immutable.
 */
@Beta
public class VoiceSearchInfo {

    private final long startTime;
    private final long recordingEndTime;
    private final long endTime;
    private final long parseDuration;

    private final VadSource vadSource;

    private final String contentBody;

    public enum VadSource {
        MANUAL, LOCAL, SERVER, TIMEOUT;
    }

    public enum ErrorType {
        NETWORK, PROTOCOL, TIMEOUT, AUDIO, AUTHENTICATION, UNKNOWN;
    }

    private final ErrorType errorType;
    private final Throwable errorException;

    private VoiceSearchInfo(Builder builder) {
        startTime = builder.startTime;
        recordingEndTime = builder.recordingEndTime;
        endTime = builder.endTime;
        parseDuration = builder.parseDuration;

        vadSource = builder.vadSource;

        contentBody = builder.contentBody;

        errorType = builder.errorType;
        errorException = builder.errorException;
    }

    // Helpers

    public long getTotalDuration() {
        return endTime - startTime;
    }

    public long getRecordingDuration() {
        if (recordingEndTime >= 0) {
            return recordingEndTime - startTime;
        }
        else {
            return -1;
        }
    }

    public long getReceivingDuration() {
        if (recordingEndTime >= 0) {
            return endTime - recordingEndTime;
        }
        else {
            return -1;
        }
    }

    // Getters and Setters

    /**
     * The value here is from {@link SystemClock#elapsedRealtime()}.
     * @return
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * The value here is from {@link SystemClock#elapsedRealtime()}.
     * @return
     */
    public long getRecordingEndTime() {
        return recordingEndTime;
    }

    /**
     * The value here is from {@link SystemClock#elapsedRealtime()}.
     * @return
     */
    public long getEndTime() {
        return endTime;
    }

    public long getParseDuration() {
        return parseDuration;
    }

    public VadSource getVadSource() {
        return vadSource;
    }

    public String getContentBody() {
        return contentBody;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public Throwable getErrorException() {
        return errorException;
    }

    /**
     * All timestamp use {@link SystemClock#elapsedRealtime()}.
     */
    public static class Builder {
        private volatile long startTime = -1;
        private volatile long recordingEndTime = -1;
        private volatile long endTime = -1;
        private volatile long parseDuration = -1;

        private volatile VadSource vadSource;
        private volatile String contentBody;
        private volatile ErrorType errorType;
        private volatile Throwable errorException;

        public Builder() {
            // Empty
        }

        public Builder withStartTime(final long startTime) {
            this.startTime = startTime;
            return this;
        }

        public Builder withRecordingEndTime(final long recordingEndTime) {
            this.recordingEndTime = recordingEndTime;
            return this;
        }

        public Builder withEndTime(final long endTime) {
            this.endTime = endTime;
            return this;
        }

        public Builder setVadSource(final VadSource vadSource) {
            this.vadSource = vadSource;
            return this;
        }

        public Builder withContentBody(final String contentBody) {
            this.contentBody = contentBody;
            return this;
        }

        public Builder withError(final ErrorType errorType, final Throwable errorException) {
            this.errorType = errorType;
            this.errorException = errorException;
            return this;
        }

        public Builder withParseDuration(final long parseDuration) {
            this.parseDuration = parseDuration;
            return this;
        }

        public VoiceSearchInfo build() {
            return new VoiceSearchInfo(this);
        }
    }
}
