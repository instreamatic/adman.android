package com.instreamatic.voice.android.sdk.impl.connection;

public class VoiceConnectionFactory {

    public static VoiceConnection createConnectionForConfig(final VoiceConnectionConfig config) {
        if (config.getEndpoint().getScheme().equals("wss") || config.getEndpoint().getScheme().equals("ws")) {
            return new WebsocketVoiceConnection(config);
        }
        else {
            throw new UnsupportedOperationException("Unsupported endpoint scheme " + config.getEndpoint().getScheme());
        }
    }
}
