package com.instreamatic.voice.android.sdk;

import com.instreamatic.voice.android.sdk.util.Exposed;
import com.instreamatic.voice.core.model.ResponseModel;

/**
 * All the states for a {@link VoiceSearch}.
 */
@Exposed
public enum VoiceSearchState {
    /**
     * The state when VoiceSearch object is constructed and is ready for a search.
     */
    STATE_INIT,

    /**
     * The state after {@link VoiceSearch#start()} is called.  We are now sending audio and awaiting a response.
     */
    STATE_STARTED,

    /**
     * When all audio recording has stopped but we are still awaiting a response.
     */
    STATE_SEARCHING,

    /**
     * The state when {@link VoiceSearchListener#onResponse(ResponseModel, VoiceSearchInfo)} is called.
     *
     * You may not start another voice search after being finished.
     */
    STATE_FINISHED,

    /**
     * The state when {@link VoiceSearchListener#onError(Exception, VoiceSearchInfo)} is called.
     *
     * You may not start another voice search after being finished.
     */
    STATE_ERROR,

    /**
     * The state when {@link VoiceSearchListener#onAbort(VoiceSearchInfo)} is called.
     *
     * You may not start another voice search after being finished.
     */
    STATE_ABORTED
}
