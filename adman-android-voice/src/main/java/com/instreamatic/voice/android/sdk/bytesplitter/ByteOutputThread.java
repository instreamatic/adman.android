package com.instreamatic.voice.android.sdk.bytesplitter;

import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;

public class ByteOutputThread extends Thread {

    public static final ByteBuffer STOP = ByteBuffer.allocate(0);

    private final ByteOutput byteOutput;
    private LinkedBlockingQueue<ByteBuffer> inputQueue = new LinkedBlockingQueue<>();

    public ByteOutputThread(final ByteOutput byteOutput) {
        this.byteOutput = byteOutput;
    }

    public LinkedBlockingQueue<ByteBuffer> getInputQueue() {
        return inputQueue;
    }

    @Override
    public void run() {
        byteOutput.onStart();
        try {
            while (!Thread.interrupted()) {
                final ByteBuffer next = inputQueue.take();
                if (next == STOP) {
                    break;
                }
                else {
                    byteOutput.onBytes(next);
                }
            }
        } catch (final InterruptedException e) {
        }

        byteOutput.onStop();
    }
}
