package com.instreamatic.voice.android.sdk.impl;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import com.instreamatic.voice.android.sdk.VoiceSearch;
import com.instreamatic.voice.android.sdk.VoiceSearchInfo;
import com.instreamatic.voice.android.sdk.VoiceSearchListener;
import com.instreamatic.voice.android.sdk.VoiceSearchState;
import com.instreamatic.voice.android.sdk.bytesplitter.ByteOutput;
import com.instreamatic.voice.android.sdk.bytesplitter.ByteSplitter;
import com.instreamatic.voice.android.sdk.impl.connection.AuthenticationException;
import com.instreamatic.voice.android.sdk.impl.connection.VoiceConnection;
import com.instreamatic.voice.android.sdk.impl.connection.VoiceConnectionConfig;
import com.instreamatic.voice.android.sdk.impl.connection.VoiceConnectionFactory;
import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;
import com.instreamatic.voice.core.util.Utils;

import java.io.InputStream;
import java.util.concurrent.TimeoutException;

/**
 * Contains the actual logic to talking to the backend.
 */
public class VoiceSearchImpl extends VoiceSearch {

    //private static final String LOG_TAG = VoiceSearchImpl.class.getSimpleName();
    public static final String LOG_TAG = "VOICE_DEBUG";

    private final Handler searchEventHandler = new Handler(Looper.getMainLooper());

    private volatile VoiceSearchState state = VoiceSearchState.STATE_INIT;
    private VoiceSearchListener listener; // We null this out at the end to prevent any possible memory leaks

    private final long serverVadWindow;

    private final InputStream audioInputStream;
    private final boolean compressAudio;

    private final VoiceConnectionConfig connectionConfig;
    private VoiceConnection voiceConnection;
    private ByteSplitter byteSplitter;

    private CalculateVolumeByteOutput calculateVolumeOutput = new CalculateVolumeByteOutput();

    private VoiceSearchInfo.Builder searchInfoBuilder;
    private long startTime;

    private String inputLanguageEnglishName;
    private String inputLanguageIetfTag;
    private static boolean sendRequestInfoInHttpHeader = false;


    private volatile VoiceSearchInfo.VadSource vadSource;
    private volatile boolean isFinished;

    // Metrics for local server VAD timing
    private volatile long lastPartialDuration = 0;

    public VoiceSearchImpl(final Builder builder) {
        setState( VoiceSearchState.STATE_INIT );
        this.audioInputStream = builder.audioInputStream;
        this.compressAudio = builder.compressAudio;
        this.connectionConfig = VoiceConnectionConfig.fromVoiceSearchBuilder(builder);

        this.serverVadWindow = builder.serverVadWindow;

        this.listener = builder.listener;

        this.inputLanguageEnglishName = builder.inputLanguageEnglishName;
        this.inputLanguageIetfTag = builder.inputLanguageIetfTag;

        this.sendRequestInfoInHttpHeader = builder.sendRequestInfoInHttpHeader;

        searchInfoBuilder = new VoiceSearchInfo.Builder();
    }

    private void postSearchEvent(final Runnable runnable, final long delay) {
        if (!isFinished) {
            searchEventHandler.postDelayed(runnable, delay);
        }
    }

    private void postSearchEvent(final Runnable runnable) {
        postSearchEvent(runnable, 0);
    }

    /**
     * Passing null will remove all callbacks.
     * @param runnable
     */
    private void removeSearchEvent(final Runnable runnable) {
        searchEventHandler.removeCallbacks(runnable);
    }

    /**
     * Starts the recording and the sending of the data.  Correct implementations must make this method thread safe.
     */
    @Override
    public void start() {
        if ( debug ) {
            Log.d( LOG_TAG, "start() called " );
        }
        postSearchEvent(new StartSearchEvent());
    }

    /**
     * Stops the recording.
     */
    @Override
    public void stopRecording() {
        if ( debug ) {
            Log.d(LOG_TAG, "stopRecording() called " + Utils.getStackTrace());
        }
        markVadTimestamp(VoiceSearchInfo.VadSource.MANUAL);
        postSearchEvent(new StopRecordingEvent());
    }

    /**
     * Completely stop the entire process.
     */
    @Override
    public void abort() {
        if ( debug ) {
            Log.d( LOG_TAG, "abort() called " + Utils.getStackTrace() );
        }
        removeSearchEvent(null);
        postSearchEvent(new AbortSearchEvent());
        isFinished = true;
    }

    private final VoiceConnection.Listener voiceConnectionListener = new VoiceConnection.Listener() {
        @Override
        public void onPartialTranscript(final TranscriptModel monitoredPartialTranscript) {
            postSearchEvent(new PartialTranscriptReceivedEvent(monitoredPartialTranscript));
        }

        @Override
        public void onResponse(final ResponseModel response, final String rawResponse) {
            if ( debug ) {
                Log.d( LOG_TAG, "onResponse() called " + Utils.getStackTrace() );
            }

            postSearchEvent(new SearchResponseEvent(response, rawResponse));
            isFinished = true;
        }

        @Override
        public void onAudioStop() {
            postSearchEvent(new AudioStopEvent());
        }

        @Override
        public void onConnectionError(final String message, final Throwable ex) {
            if ( debug ) {
                Log.d( LOG_TAG, "onConnectionError() called " + Utils.getStackTrace() );
            }
            postSearchEvent(new SearchErrorEvent(ex, ex instanceof AuthenticationException
                    ? VoiceSearchInfo.ErrorType.AUTHENTICATION
                    : VoiceSearchInfo.ErrorType.NETWORK));
            isFinished = true;
        }

        @Override
        public void onConnectionTimeout() {
            if ( debug ) {
                Log.d( LOG_TAG, "onConnectionTimeout() called " + Utils.getStackTrace() );
            }

            postSearchEvent(new SearchErrorEvent(new TimeoutException(), VoiceSearchInfo.ErrorType.TIMEOUT));
            isFinished = true;
        }
    };

    private final ByteSplitter.ErrorListener byteSplitterErrorListener = new ByteSplitter.ErrorListener() {
        @Override
        public void onInputError(final Exception exception) {
            postSearchEvent(new SearchErrorEvent(exception, VoiceSearchInfo.ErrorType.AUDIO));
            isFinished = true;
        }
    };

    private void startConnection() {
        voiceConnection = VoiceConnectionFactory.createConnectionForConfig(connectionConfig);
        voiceConnection.setListener(voiceConnectionListener);
        voiceConnection.start();
    }

    private void setupBytePump() {
        /*final ByteOutput audioProcessor = compressAudio ? new SpeexEncoderRunner(voiceConnection.getAudioDataInputQueue())
                : new WavEncoderRunner(voiceConnection.getAudioDataInputQueue());*/
        final ByteOutput audioProcessor = new WavEncoderRunner(voiceConnection.getAudioDataInputQueue());

        // mandatory input/outputs
        final ByteSplitter.Builder byteSplitterBuilder = new ByteSplitter.Builder(audioInputStream)
                .errorListener(byteSplitterErrorListener)
                .output(calculateVolumeOutput)
                .output(audioProcessor);

        byteSplitter = byteSplitterBuilder.build();
    }

    private void markVadTimestamp(final VoiceSearchInfo.VadSource vadSource) {
        if (vadSource != null) {
            this.vadSource = vadSource;
            if ( debug ) {
                Log.d( LOG_TAG, "markVadTimestamp() called with " + vadSource.toString() + Utils.getStackTrace() );
            }
        }
    }

    /**
     * Stops the whole audio pump including the search on the server. This will
     * pretty much stop everything.
     */
    private void completeStop() {
        // Null check here as we may try to stop even if we haven't started (Abort before Start).  This method
        // of addressing it is not the cleanest, but the most straightforward.
        if (byteSplitter != null) {
            byteSplitter.stopAll();
        }
        if (voiceConnection != null && voiceConnection.isRunning()) {
            voiceConnection.stop();
        }

        if (debug) {
            final String stoppedBy = vadSource != null ? vadSource.name() : "NONE";
            Log.d(LOG_TAG, "Stopped by: " + stoppedBy);
        }
    }

    private void cleanUp() {
        removeSearchEvent(null);
        listener = null; // We are paranoid here on holding references to listeners that contain references to long lived things i.e. a context
    }

    /**
     * The current state of the VoiceSearch.  Correct implementations must make this method thread safe.
     * @return the current state of the search.
     */
    @Override
    public VoiceSearchState getState() {
        return state;
    }

    /**
     * An estimation of the current volume level for the search.
     * @return a number between 0 and 100.
     */
    @Override
    public int getCurrentVolume() {
        return calculateVolumeOutput.getVolume();
    }

    private long getTimestamp() {
        return SystemClock.elapsedRealtime();
    }

    private Runnable vadCheckRunnable = new Runnable() {
        @Override
        public void run() {
            final long searchSoFar = getTimestamp() - startTime;

            if ( debug ) {
                Log.d( LOG_TAG, "vadCheckRunnable.run() called with lastPartialDuration: " + lastPartialDuration + " search duration: " +  searchSoFar + " vadSource: " + vadSource );
            }

            if (state !=  VoiceSearchState.STATE_STARTED) {
                return;
            }



            // If we are outside our window, respect local VAD
            if ( serverVadWindow == 0 || Math.abs(searchSoFar - lastPartialDuration) > serverVadWindow) {
                // Did local VAD fire?  Then stop now
                if (vadSource == VoiceSearchInfo.VadSource.LOCAL) {
                    if ( debug ) {
                        Log.d(LOG_TAG, "*** vadCheckRunnable.run() local vad called, stopping recording " + Utils.getStackTrace());
                    }
                    postSearchEvent(new StopRecordingEvent());
                }
                else if ( serverVadWindow != 0 ) {
                    if ( debug ) {
                        Log.d( LOG_TAG, "vadCheckRunnable.run() exiting with vadSource: " + vadSource );
                    }
                }
            }
        }
    };

    private class StartSearchEvent implements Runnable {
        @Override
        public void run() {
            if ( debug ) {
                Log.d( LOG_TAG, "Starting VoiceSearch with config: \n" + getConfigAsString() );
            }

            setState( VoiceSearchState.STATE_STARTED );

            startConnection();
            setupBytePump();
            byteSplitter.start();

            startTime = getTimestamp();
            searchInfoBuilder.withStartTime(startTime);
            if (listener != null) listener.onRecordingStarted();
        }
    }

    private class StopRecordingEvent implements Runnable {
        @Override
        public void run() {
            if ( debug ) {
                Log.d( LOG_TAG, "StopRecordingEvent.run() called");
            }

            if (state !=  VoiceSearchState.STATE_STARTED) {
                return;
            }

            removeSearchEvent(vadCheckRunnable);

            setState( VoiceSearchState.STATE_SEARCHING );

            byteSplitter.stopReading();

            searchInfoBuilder.withRecordingEndTime(getTimestamp());
            if (listener != null) listener.onRecordingStopped();
        }
    }

    private class AbortSearchEvent implements Runnable {
        @Override
        public void run() {
            setState( VoiceSearchState.STATE_ABORTED );

            completeStop();

            searchInfoBuilder.withEndTime(getTimestamp());

            if (listener != null) listener.onAbort(searchInfoBuilder.build());
            cleanUp();
        }
    }

    /**
     * Sets the new search state.
     *
     * @param newState
     */
    private void setState( final VoiceSearchState newState ) {
        state = newState;
        if ( debug ) {
            Log.d( LOG_TAG, "State is: " + newState.toString() );
        }
    }

    private class SearchErrorEvent implements Runnable {
        private final Throwable ex;
        private final VoiceSearchInfo.ErrorType errorType;

        private SearchErrorEvent(final Throwable ex, final VoiceSearchInfo.ErrorType errorType) {
            this.ex = ex;
            this.errorType = errorType;
        }

        @Override
        public void run() {
            setState( VoiceSearchState.STATE_ERROR );

            completeStop();

            searchInfoBuilder.setVadSource(vadSource);
            searchInfoBuilder.withError(errorType, ex);
            searchInfoBuilder.withEndTime(getTimestamp());

            if (listener != null) listener.onError(ex, searchInfoBuilder.build());
            cleanUp();
        }
    }

    private class PartialTranscriptReceivedEvent implements Runnable {
        private final TranscriptModel partialTranscription;

        private PartialTranscriptReceivedEvent(final TranscriptModel partialTranscription) {
            this.partialTranscription = partialTranscription;
        }

        @Override
        public void run() {
            removeSearchEvent(vadCheckRunnable);

            if (listener != null) listener.onTranscriptionUpdate(partialTranscription);

            if ( debug ) {
                Log.d( LOG_TAG, "Received partial translation - posting vadCheckRunnable");
            }
            postSearchEvent(vadCheckRunnable, serverVadWindow);
        }
    }

    private class AudioStopEvent implements Runnable {

        private AudioStopEvent() {}

        @Override
        public void run() {
            removeSearchEvent(vadCheckRunnable);
            markVadTimestamp(VoiceSearchInfo.VadSource.SERVER);

            // Only do the logic to stop once
            if (byteSplitter.isReading()) {
                if ( debug ) {
                    Log.d( LOG_TAG, "** Received safeToStopAudio");
                }

                postSearchEvent(new StopRecordingEvent());
            }
        }
    }

    private class SearchResponseEvent implements Runnable {
        private final ResponseModel responseReader;
        private final String rawResponse;

        private SearchResponseEvent(final ResponseModel responseReader, final String rawResponse) {
            this.responseReader = responseReader;
            this.rawResponse = rawResponse;
        }

        @Override
        public void run() {
            setState( VoiceSearchState.STATE_FINISHED );

            completeStop();

            searchInfoBuilder.setVadSource(vadSource);
            searchInfoBuilder.withContentBody(rawResponse);
            searchInfoBuilder.withEndTime(getTimestamp());

            listener.onResponse(responseReader, searchInfoBuilder.build());
            cleanUp();
        }
    }

    /**
     * Returns a string listing configuration settings
     *
     * @return
     */
    public String getConfigAsString() {
        StringBuilder str = new StringBuilder();
        str.append("endPoint = ");
        if ( connectionConfig.getEndpoint() != null ) {
            str.append( connectionConfig.getEndpoint().toString() );
        }
        else {
            str.append( "not set");
        }
        str.append("\n");

        str.append("searchingMaxDuration = ");
        str.append( connectionConfig.getReceivingTimeout() );
        str.append( " ms\n");

        str.append("audioInputStream = ");
        if ( audioInputStream != null ) {
            str.append( audioInputStream.getClass().getName() );
        }
        else {
            str.append("not set");
        }
        str.append("\n");

        str.append("serverVadWindow = ");
        str.append( serverVadWindow );
        str.append( " ms\n");

        str.append("\n");

        str.append("listener = ");
        if ( listener != null ) {
            str.append( listener.getClass().getName() );
        }
        else {
            str.append(" error - not set");
        }
        str.append("\n");

        str.append("compressAudio = ");
        str.append( compressAudio );
        str.append( " \n");

        str.append("inputLanguageEnglishName = ");
        str.append( inputLanguageEnglishName );
        str.append( " \n");

        if ( inputLanguageIetfTag != null ) {
            str.append("inputLanguageIetfTag = ");
            str.append(inputLanguageIetfTag);
            str.append(" \n");
        }

        str.append("sendRequestInfoInHttpHeader = ");
        str.append( sendRequestInfoInHttpHeader );
        str.append( " \n");

        str.append("debug = ");
        str.append( debug );
        str.append( " \n");

        return str.toString();
    }
}