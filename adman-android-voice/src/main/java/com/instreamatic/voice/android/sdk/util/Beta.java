package com.instreamatic.voice.android.sdk.util;

/**
 * Indicates that the class/field/method is a candidate for public consumption.
 */
public @interface Beta {
}
