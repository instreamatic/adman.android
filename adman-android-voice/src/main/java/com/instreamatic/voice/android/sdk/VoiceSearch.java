package com.instreamatic.voice.android.sdk;

import android.text.TextUtils;
import com.instreamatic.voice.android.sdk.audio.SimpleAudioByteStreamSource;
import com.instreamatic.voice.android.sdk.impl.VoiceSearchImpl;
import com.instreamatic.voice.android.sdk.util.Beta;
import com.instreamatic.voice.android.sdk.util.Exposed;
import com.instreamatic.voice.core.model.RequestModel;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

/**
 * <p>A one time use object that performs a voice search.  Do <b>not</b> reuse this object and instead build up a new voice search
 *  instance from the builder per search.</p>
 *
 * <p>All listeners on this class are fired on the UI thread.</p>
 *
 * <p>Use {@link Builder} to construct this object.  Once constructed the voice search can be started by calling
 * {@link #start()}</p>
 */
@Exposed
public abstract class VoiceSearch {

    /**
     * Used to turn on debug output to logcat
     */
    protected static boolean debug = false;

    /**
     * The sampling rate of the audio data being consumes from the stream set by
     * {@link VoiceSearch.Builder#setAudioSource(InputStream)}.
     */
    @Exposed
    public static final int SAMPLE_RATE = 16000;

    /**
     * Starts the voice search process.  Call this once you have build the voice search object.
     */
    @Exposed
    public abstract void start();

    /**
     * Closes the {@link InputStream} stream and calls through to {@link VoiceSearchListener#onRecordingStopped()}
     * <br/><br/>
     * It is safe to call this multiple times and will only call to the listener the first time it is called.
     */
    @Exposed
    public abstract void stopRecording();

    /**
     * Immediately kills the search and calls through to {@link VoiceSearchListener#onAbort(VoiceSearchInfo)}.
     */
    @Exposed
    public abstract void abort();

    /**
     * The current state of the search.
     * @return the state of the search
     */
    @Exposed
    public abstract VoiceSearchState getState();

    /**
     * Returns true if debug output to logcat has been turned on
     *
     * @return
     */
    @Exposed
    public static boolean isDebug() {
        return debug;
    }

    /**
     * Set true to enable debug output to logcat
     *
     * @return
     */
    @Exposed
    public static void setDebug(boolean debug) {
        VoiceSearch.debug = debug;
    }


    /**
     * The current volume at this point in the search.
     * @return a number between 0 and 100
     */
    @Exposed
    public abstract int getCurrentVolume();

    /**
     * <p>Builder class for creating a new {@link VoiceSearch}</p>
     *
     * <p>When building a voice search, you are required to set the following values:
     * <ul>
     *     <li>{@link #setAudioSource(InputStream)}</li>
     *     <li>{@link #setRequestInfo(RequestModel)}</li>
     *     <li>{@link #setListener(VoiceSearchListener)}</li>
     * </ul>
     * </p>
     */
    @Exposed
    public static class Builder {
        // Defaults are all set here but most likely should be changed by the implementer.

        private static final List<String> SUPPORTED_SCHEMES = Arrays.asList(
                "http", "https",
                "ws", "wss"
        );

        public URI endpoint;
        public InputStream audioInputStream;
        public RequestModel requestInfo;

        public long serverVadWindow = 1000;

        public int searchingMaxDuration = 10000;

        public String inputLanguageEnglishName;
        public String inputLanguageIetfTag;

        public VoiceSearchListener listener;

        public boolean compressAudio = true;
        public boolean debug = false;

        public boolean waitForExtraData = false;
        public String versionExtension = "";
        public boolean sendRequestInfoInHttpHeader = false;

        public Builder() {}

        /**
         * Set the endpoint for the voice search as a {@link URI}
         *
         * @param endpoint the endpoint for the assembled {@link VoiceSearch}
         * @return a reference to this {@link Builder}
         */
        @Beta
        public Builder setEndpoint(final URI endpoint) {
            this.endpoint = endpoint;

            if (!SUPPORTED_SCHEMES.contains(endpoint.getScheme())) {
                throw new IllegalArgumentException("Only supports the following schemes: " + TextUtils.join(",", SUPPORTED_SCHEMES));
            }

            return this;
        }

        /**
         * Returns the endpoint configured in the builder
         *
         * @return
         */
        @Beta
        public String getEndpoint() {
            if ( endpoint != null ) {
                return endpoint.toString();
            }
            return "";
        }

        /**
         * Set the endpoint for the voice search as a {@link String}
         *
         * @param endpoint the endpoint for the assembled {@link VoiceSearch}
         * @return a reference to this {@link Builder}
         * @throws IllegalArgumentException if the given endpoint is not a valid URI.
         */
        @Beta
        public Builder setEndpoint(final String endpoint) {
            try {
                return setEndpoint(new URI(endpoint));
            }
            catch (URISyntaxException ex) {
                throw new IllegalArgumentException(ex);
            }
        }

        /**
         * <p>Set the {@link InputStream} that the assembled {@link VoiceSearch}
         * will use to pull audio data from.</p>
         * <p>The audio data must be the following format.</p>
         * <ul>
         *     <li>16 bit PCM. See {@link android.media.AudioFormat#ENCODING_PCM_16BIT}</li>
         *     <li>Single channel i.e. Mono.  See {@link android.media.AudioFormat#CHANNEL_IN_MONO}</li>
         *     <li>16000 sampling rate</li>
         * </ul>
         *
         * @see SimpleAudioByteStreamSource
         *
         * @param audioRecordSource the {@link InputStream} that the
         *                          assembled {@link VoiceSearch} will use
         * @return a reference to this {@link Builder}
         */
        @Exposed
        public Builder setAudioSource(final InputStream audioRecordSource) {
            this.audioInputStream = audioRecordSource;
            return this;
        }

        /**
         * The central request info sent up to the server.  Contains various meta information from the client.
         * This is a required field.
         * @param requestInfo
         * @return a reference to this {@link Builder}
         */
        @Exposed
        public Builder setRequestInfo(final RequestModel requestInfo) {
            this.requestInfo = requestInfo;
            return this;
        }

        /**
         * This parameter should only be changed for special circumstances and should
         * not be adjust for normal SDK usage.
         *
         * The purpose of the serverVadWindow parameter is to control when the client's VAD
         * should be used to detect that user has stopped saying their query.  In most
         * cases the client SDK relies on the server VAD to determine when the user has stopped
         * speaking.  The server indicates a complete query has been received to the client by
         * setting the SafeToStopAudio flag to true in a partial transcript message.  At this point
         * the client stops sending audio to the server and waits for a response.
         *
         * If network conditions become slow then it is possible that the partial transcription messages
         * will get delayed coming from the server.  In this case, the client will fall back to
         * using its local VAD to determine when the user has stopped speaking.  It determines this
         * by comparing the duration of audio recorded locally with that processed by the server as
         * passed back in the last received partial transcription.  If the difference in local recorded
         * audio is greater then the server processed duration by "serverVadWindow" amount then the
         * local VAD is put into use.
         *
         * if ( localAudioRecordTime - serverAudioProcessTime > serverVadWindow ) {
         *     then start local VAD.
         * }
         *
         *
         * <p><b>Generally, should not be changed, the default value is 1000 milliseconds, setting
         *       this value to zero will cause local VAD to always be used</b></p>
         * @param serverVadWindow in milliseconds
         * @return
         */
        @Beta
        public Builder setServerVadWindow(long serverVadWindow) {
            this.serverVadWindow = serverVadWindow;
            return this;
        }

        /**
         * <p>Sets the maximum duration in milliseconds that the searching process is
         * allowed to take before the connection times out.  Searching time is defined
         * as the time from which audio recording has stopped, until the response is
         * received</p>
         *
         * <p>Default: <code>10000</code> milliseconds</p>
         *
         * @param searchingMaxDuration
         * @return a reference to this {@link Builder}
         */
        @Exposed
        public Builder setSearchMaxDuration(final int searchingMaxDuration) {
            this.searchingMaxDuration = searchingMaxDuration;
            return this;
        }

        /**
         * Adds a voice search listener.  Correct implementations must make this method thread safe.
         *
         * @param listener the {@link VoiceSearchListener} that will receive search event updates
         * @return a reference to this {@link Builder}
         */
        @Exposed
        public Builder setListener(final VoiceSearchListener listener) {
            this.listener = listener;
            return this;
        }

        /**
         * <p>Compresses the audio retrieved from the audioInputStream.  It is recommended to leave audio
         * compression on as uncompressed PCM data is <b>very very large</b> and is not mobile friendly.</p>
         * <p>By default this value is true.</p>
         * @param compressAudio
         */
        @Exposed
        public Builder setCompressAudio(boolean compressAudio) {
            this.compressAudio = compressAudio;
            return this;
        }


        /**
         * Set to true is we're using a proxy server that send an
         * extra data response following the standard JSON response.
         *
         * @param waitForExtraData
         * @return
         */
        @Exposed
        public Builder setWaitForExtraData(boolean waitForExtraData) {
            this.waitForExtraData = waitForExtraData;
            return this;
        }

        /**
         * Provides an extra string to be appended to the request's version string.
         * This is used by a proxy server to identify clients that receive an
         * Extra Data response.
         *
         * @param versionExtension
         * @return
         */
        @Exposed
        public Builder setVersionExtension(String versionExtension) {
            this.versionExtension = versionExtension;
            return this;
        }

        @Exposed
        public Builder setInputLanguageIetfTag(String inputLanguageIetfTag) {
            this.inputLanguageIetfTag = inputLanguageIetfTag;
            return this;
        }

        @Exposed
        public Builder setInputLanguageEnglishName(String inputLanguageEnglishName) {
            this.inputLanguageEnglishName = inputLanguageEnglishName;
            return this;
        }

        @Exposed
        public Builder setSendRequestInfoInHttpHeader(boolean requestInfoInHttpHeader) {
            this.sendRequestInfoInHttpHeader = requestInfoInHttpHeader;
            return this;
        }

        /**
         * Constructs the final voice search object.
         * @throws IllegalArgumentException if you build is misconfigured.
         */
        @Exposed
        public VoiceSearch build() {
            // Throw IllegalArgumentException if we don't get everything we want

            if (endpoint == null) {
                throw new IllegalArgumentException("Endpoint must not be null");
            }

            if (audioInputStream == null) {
                throw new IllegalArgumentException("You must set a audio source");
            }

            if (requestInfo == null) {
                throw new IllegalArgumentException("You must set a request info");
            }

            if (listener == null) {
                throw new IllegalArgumentException("You must set a listener");
            }

            return new VoiceSearchImpl(this);
        }

        /**
         * Returns a string listing configuration settings
         *
         * @return
         */
        @Exposed
        @Override
        public String toString() {
            StringBuilder str = new StringBuilder();
            str.append("endPoint = ");
            if ( endpoint != null ) {
                str.append( endpoint.toString() );
            }
            else {
                str.append( "not set");
            }
            str.append("\n");

            str.append("searchingMaxDuration = ");
            str.append( searchingMaxDuration );
            str.append( " ms\n");

            str.append("audioInputStream = ");
            if ( audioInputStream != null ) {
                str.append( audioInputStream.getClass().getName() );
            }
            else {
                str.append("not set");
            }
            str.append("\n");

            str.append("serverVadWindow = ");
            str.append( serverVadWindow );
            str.append( " ms\n");

            str.append("listener = ");
            if ( listener != null ) {
                str.append( listener.getClass().getName() );
            }
            else {
                str.append(" error - not set");
            }
            str.append("\n");

            str.append("compressAudio = ");
            str.append( compressAudio );
            str.append( " \n");

            str.append("inputLanguageEnglishName = ");
            str.append(inputLanguageEnglishName);
            str.append( " \n");

            str.append("inputLanguageIetfTag = ");
            str.append(inputLanguageIetfTag);
            str.append( " \n");

            str.append("debug = ");
            str.append( debug );
            str.append( " \n");

            return str.toString();
        }
    }
}
