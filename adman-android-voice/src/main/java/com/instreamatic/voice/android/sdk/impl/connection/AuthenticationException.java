package com.instreamatic.voice.android.sdk.impl.connection;

/**
 * Marker exception to be used by {@link VoiceConnection}s to indicate authentication (HTTP 403) errors
 */
public class AuthenticationException extends Exception {
}
