package com.instreamatic.voice.android.sdk;

import com.instreamatic.voice.android.sdk.util.Exposed;
import com.instreamatic.voice.core.model.ResponseModel;
import com.instreamatic.voice.core.model.TranscriptModel;

/**
* Listener for a {@link VoiceSearch}.
*/
@Exposed
public interface VoiceSearchListener {

    /**
     * If the partial transcript were enabled when constructing the voice search this method will be called throughout
     * the search.  You are always guaranteed to get at least one partial transcript before {#onResponse} is called.
     * @param transcript the partial transcript update
     */
    @Exposed
    void onTranscriptionUpdate(final TranscriptModel transcript);

    /**
     * Successful response back from the server that is properly formed.
     * @param response the main response model
     * @param info miscellaneous information about the search
     */
    @Exposed
    void onResponse(final ResponseModel response, final VoiceSearchInfo info);

    /**
     * An error occurred while sending or receiving the bytes.  Manually aborting a voice search will <b>not</b>
     * trigger this callback.
     * @param e
     * @param info
     */
    @Exposed
    void onError(final Throwable e, final VoiceSearchInfo info);

    /**
     * {@link VoiceSearch#abort()} was called.
     *
     * We are now in the {@link VoiceSearchState#STATE_ABORTED} state.
     * @param info
     */
    @Exposed
    void onAbort(final VoiceSearchInfo info);

    /**
     * Recording started.  We are now in the {@link VoiceSearchState#STATE_SEARCHING} state.
     */
    @Exposed
    void onRecordingStarted();

    /**
     * Recording stopped.  We are now in the {@link VoiceSearchState#STATE_SEARCHING} state.
     */
    @Exposed
    void onRecordingStopped();
}
