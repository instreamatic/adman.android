package com.instreamatic.voice.android.sdk.impl.connection;

import android.net.Uri;
import com.instreamatic.voice.android.sdk.VoiceSearch;
import com.instreamatic.voice.core.model.RequestModel;

public class VoiceConnectionConfig {

    private Uri endpoint;

    private RequestModel requestInfo;
    private int receivingTimeout;
    private boolean compressAudio;

    public Uri getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(Uri endpoint) {
        this.endpoint = endpoint;
    }

    public RequestModel getRequestInfo() {
        return requestInfo;
    }

    public void setRequestInfo(RequestModel requestInfo) {
        this.requestInfo = requestInfo;
    }

    public int getReceivingTimeout() {
        return receivingTimeout;
    }

    public void setReceivingTimeout(final int receivingTimeout) {
        this.receivingTimeout = receivingTimeout;
    }

    public boolean isCompressAudio() {
        return compressAudio;
    }

    public void setCompressAudio(boolean compressAudio) {
        this.compressAudio = compressAudio;
    }

    public static VoiceConnectionConfig fromVoiceSearchBuilder(final VoiceSearch.Builder builder) {
        final VoiceConnectionConfig config = new VoiceConnectionConfig();
        config.endpoint = Uri.parse(builder.endpoint.toString());
        config.requestInfo = builder.requestInfo;
        config.receivingTimeout = builder.searchingMaxDuration;
        config.compressAudio = builder.compressAudio;
        return config;
    }
}
