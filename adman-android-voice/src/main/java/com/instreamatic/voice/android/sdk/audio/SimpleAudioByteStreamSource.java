package com.instreamatic.voice.android.sdk.audio;

import android.media.AudioRecord;
import com.instreamatic.voice.android.sdk.util.Exposed;

import java.io.IOException;
import java.io.InputStream;

/**
 * A basic implementation of an {@link InputStream} that pulls data
 * from a preconfigured {@link AudioRecord} using the {@link AudioRecordFactory}.
 */
@Exposed
public class SimpleAudioByteStreamSource extends InputStream {

    private enum State {
        IDLE, STARTED, STOPPED;
    }

    private AudioRecord audioRecord;

    private State state = State.IDLE;

    private byte[] internalBuffer = new byte[1024];

    @Override
    public int read() throws IOException {
        throw new UnsupportedOperationException("This operation is not supported");
    }

    @Override
    public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
        if (state == State.IDLE) {
            try {
                audioRecord = AudioRecordFactory.getInstance();

                audioRecord.startRecording();
                state = State.STARTED;
            }
            catch (final AudioRecordFactory.AudioRecordException e) {
                return -1;
            }
        }

        final int bytesRead = audioRecord == null ? 0 : audioRecord.read(internalBuffer, 0, Math.min(byteCount, internalBuffer.length));
        if (bytesRead > 0) {
            System.arraycopy(internalBuffer, 0, buffer, byteOffset, bytesRead);
            return bytesRead;
        }
        else {
            close();
            throw new IOException("Error reading from audio record.  Status = " + bytesRead);
        }
    }

    @Override
    public void close() throws IOException {
        if (state == State.STARTED) {
            try {
                audioRecord.stop();
            }
            catch (IllegalStateException ex) {
                // Swallow it, main goal is to stop all recording.
            }
        }

        audioRecord = null;
        state = State.STOPPED;
    }
}
