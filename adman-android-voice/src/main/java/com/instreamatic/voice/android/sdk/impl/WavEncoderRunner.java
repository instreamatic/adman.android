package com.instreamatic.voice.android.sdk.impl;

import com.instreamatic.voice.android.sdk.bytesplitter.ByteOutput;
import com.instreamatic.voice.java.audio.WavHeader;
import com.instreamatic.voice.java.audio.WavHeaderFactory;
import com.instreamatic.voice.java.audio.WavHeaderUtil;

import java.nio.ByteBuffer;
import java.util.concurrent.BlockingQueue;

public class WavEncoderRunner implements ByteOutput {

    private final BlockingQueue<ByteBuffer> outputQueue;

    public WavEncoderRunner(final BlockingQueue<ByteBuffer> outputQueue) {
        this.outputQueue = outputQueue;
    }

    @Override
    public void onStart() {
        try {
            final WavHeader wavHeader = WavHeaderFactory.createPCM16MonoHeader(16000);
            outputQueue.offer(ByteBuffer.wrap(WavHeaderUtil.write(wavHeader)));
        }
        catch (WavHeaderUtil.WavHeaderFactoryException ex) {
            throw new RuntimeException("Error writing out wav header, this should never happen");
        }
    }

    @Override
    public void onBytes(final ByteBuffer buffer) {
        outputQueue.offer(buffer);
    }

    @Override
    public void onStop() {
        outputQueue.offer(ByteOutput.STOP);
    }
}