package com.instreamatic.voice.android.sdk.bytesplitter;

import java.nio.ByteBuffer;

public interface ByteOutput {

    ByteBuffer STOP = ByteBuffer.allocate(0);

    void onStart();

    void onBytes(final ByteBuffer byteBuffer);

    void onStop();
}
