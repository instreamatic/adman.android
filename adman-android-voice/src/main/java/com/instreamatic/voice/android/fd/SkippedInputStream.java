package com.instreamatic.voice.android.fd;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created on 10/13/15.
 */
public class SkippedInputStream extends InputStream {

    private final InputStream inputStream;
    private final int bytesToSkip;

    private boolean bytesSkipped;

    public SkippedInputStream(final InputStream inputStream, final int bytesToSkip) {
        this.inputStream = inputStream;
        this.bytesToSkip = bytesToSkip;
    }

    @Override
    public int read() throws IOException {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public int read(final byte[] buffer, final int byteOffset, final int byteCount) throws IOException {
        if (bytesToSkip > 0 && !bytesSkipped) {
            final byte[] skipBuffer = new byte[1024]; // This should only be allocated once so we are OK
            int totalRead = 0;
            bytesSkipped = true;

            while (totalRead < bytesToSkip) {
                final int read = inputStream.read(skipBuffer, 0, Math.min(skipBuffer.length, bytesToSkip - totalRead));

                if (read >= 0) {
                    totalRead += read;
                }
                else {
                    return -1;
                }
            }
        }

        return inputStream.read(buffer, byteOffset, byteCount);
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
