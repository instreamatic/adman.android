package com.instreamatic.voice.android.sdk.util;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Indicates that the class/method/field is for public consumption.
 */
@Documented
@Retention(RetentionPolicy.CLASS)
public @interface Exposed {
}
