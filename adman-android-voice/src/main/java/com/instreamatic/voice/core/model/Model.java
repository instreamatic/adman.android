package com.instreamatic.voice.core.model;

import org.json.JSONException;
import org.json.JSONObject;

abstract public class Model {
    abstract public void fromJSON(JSONObject json) throws JSONException;
    abstract public JSONObject toJSON() throws JSONException;
}
