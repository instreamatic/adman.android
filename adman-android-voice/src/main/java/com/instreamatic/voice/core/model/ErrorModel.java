package com.instreamatic.voice.core.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ErrorModel extends Model {

    String error;

    public ErrorModel() {
        // Empty for jackson
    }

    public String getError() {
        return error;
    }

    public void setError(final String error) {
        this.error = error;
    }

    @Override
    public void fromJSON(JSONObject json) throws JSONException {
        error = json.getString("error");
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("error", error);
        return result;
    }
}
