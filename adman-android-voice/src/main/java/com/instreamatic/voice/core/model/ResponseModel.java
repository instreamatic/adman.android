package com.instreamatic.voice.core.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ResponseModel extends Model {

    String transcript;
    String action;

    public ResponseModel() {
    }

    public String getTranscript() {
        return transcript;
    }

    public void setTranscript(String transcript) {
        this.transcript = transcript;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void fromJSON(JSONObject json) throws JSONException {
        transcript = json.getString("transcript");
        action = json.getString("action");
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("transcript", transcript);
        result.put("action", action);
        return result;
    }
}
