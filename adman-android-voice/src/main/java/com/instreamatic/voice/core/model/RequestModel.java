package com.instreamatic.voice.core.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The request info to be sent on a voice search.
 */
public class RequestModel extends Model {

    Integer partner_id;
    Integer site_id;
    String ad_id;
    Double response_delay;
    String device_info;
    String advertising_id;
    Boolean vad;

    public RequestModel(Integer partner_id, Integer site_id, String ad_id, Double response_delay, String device_info, String advertising_id, Boolean vad) {
        this.partner_id = partner_id;
        this.site_id = site_id;
        this.ad_id = ad_id;
        this.response_delay = response_delay;
        this.device_info = device_info;
        this.advertising_id = advertising_id;
        this.vad = vad;
    }

    @Override
    public void fromJSON(JSONObject json) throws JSONException {
        this.partner_id = json.getInt("partner_id");
        this.site_id = json.getInt("site_id");
        this.ad_id = json.getString("ad_id");
        this.response_delay = json.getDouble("response_delay");
        this.device_info = json.getString("device_info");
        this.advertising_id = json.getString("advertising_id");
        this.vad = json.getBoolean("vad");
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("partner_id", partner_id);
        result.put("site_id", site_id);
        result.put("ad_id", ad_id);
        result.put("response_delay", response_delay);
        result.put("device_info", device_info);
        result.put("advertising_id", advertising_id);
        result.put("vad", vad);
        return result;
    }
}
