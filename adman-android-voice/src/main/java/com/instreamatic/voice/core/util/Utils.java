package com.instreamatic.voice.core.util;

public class Utils {


    /**
     * Returns string containing stack trace of calling thread
     *
     * @return
     */
    static public String getStackTrace() {
        StringBuffer stackStr = new StringBuffer();
        stackStr.append("\n");

        StackTraceElement[] stack = Thread.currentThread().getStackTrace();
        if ( stack != null ) {
            for (int i = 0; i < stack.length; i++) {
                stackStr.append("    ");
                stackStr.append("\tat ");
                stackStr.append(stack[i].toString());
                stackStr.append("\n");
            }
        }
        return stackStr.toString();
    }

    public static byte[] concatAll(byte[]... rest) {
        int totalLength = 0;
        for (byte[] array : rest) {
            totalLength += array.length;
        }
        byte[] result = new byte[totalLength];
        int offset = 0;
        for (byte[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }

}
