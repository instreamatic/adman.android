package com.instreamatic.voice.core.model;

import org.json.JSONException;
import org.json.JSONObject;

public class TranscriptModel extends Model {

    String transcript;

    public TranscriptModel() {}

    public String getTranscript() {
        return transcript;
    }

    public void setTranscript(final String transcript) {
        this.transcript = transcript;
    }

    @Override
    public void fromJSON(JSONObject json) throws JSONException {
        transcript = json.getString("transcript");
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject result = new JSONObject();
        result.put("transcript", transcript);
        return result;
    }
}
