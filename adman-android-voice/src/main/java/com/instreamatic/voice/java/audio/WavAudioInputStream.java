package com.instreamatic.voice.java.audio;

import java.io.IOException;
import java.io.InputStream;
















public class WavAudioInputStream
  extends InputStream
{
  private static final boolean LOG_DEBUG = false;
  private static final int MAX_REALTIME_READ_SIZE = 512;
  private final WavHeader wavHeader;
  private final InputStream wavInputStream;
  private long startTime;
  private int totalBytesRead = 0;
  

  private boolean realTime;
  


  public WavAudioInputStream(InputStream is)
    throws WavHeaderUtil.WavHeaderFactoryException
  {
    this(is, true);
  }
  
  public WavAudioInputStream(InputStream is, boolean realTime) throws WavHeaderUtil.WavHeaderFactoryException
  {
    this.wavHeader = WavHeaderUtil.read(is);
    this.wavInputStream = is;
    setRealTimeFlag(realTime);
  }
  







  public WavHeader getWavHeader()
  {
    return this.wavHeader;
  }
  
  public void setRealTimeFlag(boolean flag) {
    if ((this.realTime) && (!this.realTime) && (flag)) {
      this.totalBytesRead = 0;
    }
    
    this.realTime = flag;
  }
  
  public boolean isRealTime() {
    return this.realTime;
  }
  


  public void resetRealTime()
  {
    this.totalBytesRead = 0;
  }
  
  public int read() throws IOException
  {
    int read = this.wavInputStream.read();
    
    if ((this.realTime) && (read >= 0)) {
      if (this.totalBytesRead == 0) {
        this.startTime = System.currentTimeMillis();
      }
      
      this.totalBytesRead += 1;
      waitForNext();
    }
    
    return read;
  }
  
  public int read(byte[] buffer) throws IOException
  {
    int bytesToRead = this.realTime ? 512 : buffer.length;
    int read = this.wavInputStream.read(buffer, 0, bytesToRead);
    
    if ((this.realTime) && (read >= 0)) {
      if (this.totalBytesRead == 0) {
        this.startTime = System.currentTimeMillis();
      }
      
      this.totalBytesRead += read;
      waitForNext();
    }
    
    return read;
  }
  
  private void waitForNext()
  {
    try {
      long diff = System.currentTimeMillis() - this.startTime;
      long waitTime = (long) (1000.0D * (this.totalBytesRead / this.wavHeader.getByteRate()));
      
      if (diff < waitTime) {
        Thread.sleep(waitTime - diff);
      }
    }
    catch (InterruptedException localInterruptedException) {}
  }
}
