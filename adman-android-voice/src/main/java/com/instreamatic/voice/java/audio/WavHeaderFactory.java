package com.instreamatic.voice.java.audio;




public class WavHeaderFactory
{
  public static WavHeader createPCM16MonoHeader(int sampleRate)
  {
    WavHeader wavHeader = new WavHeader();
    
    wavHeader.setChunkSize(0);
    wavHeader.setSubchunk1Size(16);
    wavHeader.setAudioFormat(1);
    wavHeader.setNumChannels(1);
    wavHeader.setSampleRate(sampleRate);
    wavHeader.setBitsPerSample(16);
    wavHeader.setByteRate(wavHeader.getSampleRate() * wavHeader.getNumChannels() * wavHeader.getBitsPerSample() / 8);
    wavHeader.setBlockAlign(wavHeader.getNumChannels() * wavHeader.getBitsPerSample() / 8);
    wavHeader.setSubchunk2Size(0);
    
    return wavHeader;
  }
}
