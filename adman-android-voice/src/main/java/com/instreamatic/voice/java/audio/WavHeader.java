package com.instreamatic.voice.java.audio;


public class WavHeader
{
  private int chunkSize;
  
  private int subchunk1Size;
  
  private int audioFormat;
  
  private int numChannels;
  private int sampleRate;
  private int byteRate;
  private int blockAlign;
  private int bitsPerSample;
  private int subchunk2Size;
  
  public int getChunkSize()
  {
    return this.chunkSize;
  }
  
  public void setChunkSize(int chunkSize) {
    this.chunkSize = chunkSize;
  }
  
  public int getSubchunk1Size() {
    return this.subchunk1Size;
  }
  
  public void setSubchunk1Size(int subchunk1Size) {
    this.subchunk1Size = subchunk1Size;
  }
  
  public int getAudioFormat() {
    return this.audioFormat;
  }
  
  public void setAudioFormat(int audioFormat) {
    this.audioFormat = audioFormat;
  }
  
  public int getNumChannels() {
    return this.numChannels;
  }
  
  public void setNumChannels(int numChannels) {
    this.numChannels = numChannels;
  }
  
  public int getSampleRate() {
    return this.sampleRate;
  }
  
  public void setSampleRate(int sampleRate) {
    this.sampleRate = sampleRate;
  }
  
  public int getByteRate() {
    return this.byteRate;
  }
  
  public void setByteRate(int byteRate) {
    this.byteRate = byteRate;
  }
  
  public int getBlockAlign() {
    return this.blockAlign;
  }
  
  public void setBlockAlign(int blockAlign) {
    this.blockAlign = blockAlign;
  }
  
  public int getBitsPerSample() {
    return this.bitsPerSample;
  }
  
  public void setBitsPerSample(int bitsPerSample) {
    this.bitsPerSample = bitsPerSample;
  }
  
  public int getSubchunk2Size() {
    return this.subchunk2Size;
  }
  
  public void setSubchunk2Size(int subchunk2Size) {
    this.subchunk2Size = subchunk2Size;
  }
  
  public long getDuration() {
    return (long) (1000.0D * getSubchunk2Size() / getByteRate());
  }
}
