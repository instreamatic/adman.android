package com.instreamatic.voice.java.audio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;












public class WavHeaderUtil
{
  public static WavHeader read(InputStream is)
    throws WavHeaderUtil.WavHeaderFactoryException
  {
    try
    {
      WavHeader wavHeader = new WavHeader();
      
      byte[] headerBlob = new byte[44];
      int read = is.read(headerBlob);
      if (read != 44) {
        throw new WavHeaderFactoryException("The InputStream does not have the necessary 44 bytes to be a valid header");
      }
      ByteBuffer buffer = ByteBuffer.wrap(headerBlob);
      buffer.order(ByteOrder.BIG_ENDIAN);
      
      byte[] fourBytes = new byte[4];
      

      buffer.get(fourBytes);
      String riffStr = new String(fourBytes, "US-ASCII");
      if (!"RIFF".equals(riffStr)) {
        throw new WavHeaderFactoryException("The InputStream does not contain the necessary 'RIFF' header");
      }
      
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.get(fourBytes);
      wavHeader.setChunkSize(convertToInt(fourBytes));
      
      buffer.order(ByteOrder.BIG_ENDIAN);
      buffer.get(fourBytes);
      String formatStr = new String(fourBytes, "US-ASCII");
      if (!"WAVE".equals(formatStr)) {
        throw new WavHeaderFactoryException("The InputStream does not contain the necessary 'WAVE' header");
      }
      

      buffer.get(fourBytes);
      String subChunk1IDstr = new String(fourBytes, "US-ASCII");
      if (!"fmt ".equals(subChunk1IDstr)) {
        throw new WavHeaderFactoryException("The InputStream does not contain the necessary 'fmt ' header");
      }
      
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.get(fourBytes);
      wavHeader.setSubchunk1Size(convertToInt(fourBytes));
      
      buffer.get(fourBytes, 0, 2);
      wavHeader.setAudioFormat(convertToInt(fourBytes, 2));
      
      buffer.get(fourBytes, 0, 2);
      wavHeader.setNumChannels(convertToInt(fourBytes, 2));
      
      buffer.get(fourBytes);
      wavHeader.setSampleRate(convertToInt(fourBytes));
      
      buffer.get(fourBytes);
      wavHeader.setByteRate(convertToInt(fourBytes));
      
      buffer.get(fourBytes, 0, 2);
      wavHeader.setBlockAlign(convertToInt(fourBytes, 2));
      
      buffer.get(fourBytes, 0, 2);
      wavHeader.setBitsPerSample(convertToInt(fourBytes, 2));
      

      buffer.order(ByteOrder.BIG_ENDIAN);
      buffer.get(fourBytes);
      String subChunk2IDstr = new String(fourBytes, "US-ASCII");
      if (!"data".equals(subChunk2IDstr)) {
        throw new WavHeaderFactoryException("The InputStream does not contain the necessary 'data ' header");
      }
      
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.get(fourBytes);
      wavHeader.setSubchunk2Size(convertToInt(fourBytes));
      
      WavHeader localWavHeader1 = wavHeader;return localWavHeader1;
    } catch (IOException ex) {
      ex = ex;
      throw new WavHeaderFactoryException(ex);
    }
    finally {}
  }
  
  public static byte[] write(WavHeader wavHeader) throws WavHeaderUtil.WavHeaderFactoryException
  {
    try
    {
      int BLOB_SIZE = 44;
      
      String RIFF = "RIFF";
      String WAVE = "WAVE";
      String FMT_SPACE = "fmt ";
      String DATA = "data";
      
      ByteBuffer buffer = ByteBuffer.wrap(new byte[44]);
      
      buffer.order(ByteOrder.BIG_ENDIAN);
      buffer.put("RIFF".getBytes("US-ASCII"));
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.put(convertToUnsignedInt(wavHeader.getChunkSize()));
      buffer.order(ByteOrder.BIG_ENDIAN);
      buffer.put("WAVE".getBytes("US-ASCII"));
      
      buffer.put("fmt ".getBytes("US-ASCII"));
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.put(convertToUnsignedInt(wavHeader.getSubchunk1Size()));
      buffer.put(convertToUnsignedShort(wavHeader.getAudioFormat()));
      buffer.put(convertToUnsignedShort(wavHeader.getNumChannels()));
      buffer.put(convertToUnsignedInt(wavHeader.getSampleRate()));
      buffer.put(convertToUnsignedInt(wavHeader.getByteRate()));
      buffer.put(convertToUnsignedShort(wavHeader.getBlockAlign()));
      buffer.put(convertToUnsignedShort(wavHeader.getBitsPerSample()));
      
      buffer.order(ByteOrder.BIG_ENDIAN);
      buffer.put("data".getBytes("US-ASCII"));
      buffer.order(ByteOrder.LITTLE_ENDIAN);
      buffer.put(convertToUnsignedInt(wavHeader.getSubchunk2Size()));
      
      return buffer.array();
    }
    catch (IOException ex) {
      throw new WavHeaderFactoryException(ex);
    }
  }
  


  private static byte[] convertToUnsignedShort(int num)
  {
    byte[] buf = new byte[2];
    buf[0] = ((byte)(num & 0xFF));
    buf[1] = ((byte)((num & 0xFF00) >> 8));
    return buf;
  }
  
  private static byte[] convertToUnsignedInt(int num)
  {
    byte[] buf = new byte[4];
    
    buf[0] = ((byte)(num & 0xFF));
    buf[1] = ((byte)((num & 0xFF00) >> 8));
    buf[2] = ((byte)((num & 0xFF0000) >> 16));
    buf[3] = ((byte)((num & 0xFF000000) >> 24));
    
    return buf;
  }
  
  private static int convertToInt(byte[] blob) {
    return convertToInt(blob, blob.length);
  }
  
  private static int convertToInt(byte[] blob, int len)
  {
    int value = 0;
    for (int i = 0; i < len; i++) {
      value += ((blob[i] & 0xFF) << 8 * i);
    }
    return value;
  }
  
  public static class WavHeaderFactoryException
    extends Exception
  {
    private static final long serialVersionUID = 1L;
    
    public WavHeaderFactoryException() {}
    
    public WavHeaderFactoryException(String detailMessage, Throwable throwable)
    {
      super(throwable);
    }
    
    public WavHeaderFactoryException(String detailMessage) {
      super();
    }
    
    public WavHeaderFactoryException(Throwable throwable) {
      super();
    }
  }
}
