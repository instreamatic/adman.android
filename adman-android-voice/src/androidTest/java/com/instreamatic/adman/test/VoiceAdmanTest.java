package com.instreamatic.adman.test;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.instreamatic.adman.*;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.adman.voice.VoiceEvent;
import com.instreamatic.vast.model.VASTInline;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@RunWith(AndroidJUnit4.class)
public class VoiceAdmanTest {

    public static AdmanRequest buildRequest() {
        return new AdmanRequest.Builder()
                .setRegion(Region.EUROPE)
                .setType(Type.VOICE)
                .setSiteId(777)
                .setPreview(90)
                .build();
    }

    public static Context getContext() {
        return InstrumentationRegistry.getInstrumentation().getContext();
    }

    public static void grantPermission(String name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().getUiAutomation().executeShellCommand(
                    "pm grant " + InstrumentationRegistry.getTargetContext().getPackageName() + " " + name
            );
        }
    }

    @Before
    public void grantPermissions() {
        grantPermission(Manifest.permission.RECORD_AUDIO);
    }

    @Test
    public void testVoice() throws Exception {
        final CountDownLatch lock = new CountDownLatch(3);
        final Map<VoiceEvent.Type, Boolean> state = new HashMap<>();
        IAdman adman = new Adman(getContext(), buildRequest());
        adman.bindModule(new AdmanVoice(getContext()) {
            @Override
            protected void onStartAd(VASTInline ad) {
                super.onStartAd(ad);
                Assert.assertEquals(6, getResponses().size());
                lock.countDown();
            }
        });
        adman.start();
        adman.getDispatcher().addListener(VoiceEvent.TYPE, new VoiceEvent.Listener() {
            @Override
            public void onVoiceEvent(VoiceEvent event) {
                state.put(event.getType(), true);
                switch (event.getType()) {
                    case START:
                    case STOP:
                        lock.countDown();
                        break;
                    case FAIL:
                        Assert.fail(event.getType().name());
                        break;
                }
            }
        });
        lock.await(45, TimeUnit.SECONDS);
        Assert.assertTrue("!START", state.containsKey(VoiceEvent.Type.START));
        Assert.assertTrue("!STOP", state.containsKey(VoiceEvent.Type.STOP));
    }

}
