package com.instreamatic.vast;

import android.content.Context;
import android.media.MediaPlayer;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.model.VASTEvent;
import com.instreamatic.vast.model.VASTMedia;

public class VASTPlayer extends AudioPlayer {

    final protected VASTMedia media;
    final protected VASTDispatcher dispatcher;

    private int positionValue;
    private int positionPValue;

    private boolean dispatchEnabled;

    public VASTPlayer(Context context, VASTMedia media, VASTDispatcher dispatcher, boolean autoplay) {
        super(context, media.url, autoplay);
        this.media = media;
        this.dispatcher = dispatcher;
        positionValue = 0;
        positionPValue = 0;
        dispatchEnabled = dispatcher != null;
    }

    public boolean isDispatchEnabled() {
        return dispatchEnabled;
    }

    public void setDispatchEnabled(boolean dispatchEnabled) {
        this.dispatchEnabled = dispatchEnabled;
    }

    @Override
    protected void onChangeState(State prevState, State nextState) {
        super.onChangeState(prevState, nextState);
        if (dispatchEnabled) {
            if (prevState == State.READY && nextState == State.PLAYING) {
                dispatcher.dispatch(VASTEvent.impression);
                dispatcher.dispatch(VASTEvent.start);
            }
            if (prevState == State.PLAYING && nextState == State.PAUSED) {
                dispatcher.dispatch(VASTEvent.pause);
            }
            if (prevState == State.PAUSED && nextState == State.PLAYING) {
                dispatcher.dispatch(VASTEvent.resume);
            }
            if (nextState == State.ERROR) {
                dispatcher.dispatch(VASTEvent.error);
            }
        }
    }

    @Override
    protected void onChangeProgress(int position, int duration) {
        super.onChangeProgress(position, duration);
        int v = Math.round(mediaPlayer.getCurrentPosition() / 1000);
        int p = Math.round(((float) mediaPlayer.getCurrentPosition() / mediaPlayer.getDuration()) * 100);
        if (dispatchEnabled) {
            for (int i = positionValue + 1; i <= v; i++) {
                dispatcher.onProgress(i);
            }
            for (int i = positionPValue + 1; i <= p; i++) {
                dispatcher.onProgressPercent(i);
            }
        }
        positionValue = v;
        positionPValue = p;
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        super.onCompletion(mediaPlayer);
        if (dispatchEnabled) {
            dispatcher.dispatch(VASTEvent.complete);
        }
    }
}
