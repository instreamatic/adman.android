package com.instreamatic.vast;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class VASTExtension {

    final public String type;
    final public String value;
    final public Node node;

    public VASTExtension(Node node) {
        NamedNodeMap attrs = node.getAttributes();
        this.type = attrs.getNamedItem("type").getNodeValue();
        this.value = node.getTextContent();
        this.node = node;
    }
}
