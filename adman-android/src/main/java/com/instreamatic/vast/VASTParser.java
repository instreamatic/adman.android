package com.instreamatic.vast;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import com.instreamatic.vast.model.*;
import com.instreamatic.vast.utils.NodeListWrapper;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.util.*;

@TargetApi(Build.VERSION_CODES.FROYO)
public class VASTParser {

    final private static String TAG = VASTParser.class.getSimpleName();

    public static List<String> extractNodesValues(XPath xpath, Node node, String name) throws XPathExpressionException {
        NodeList nodes = (NodeList) xpath.evaluate(name, node, XPathConstants.NODESET);
        List<String> result = new ArrayList<>();
        if (nodes != null) {
            for (Node n : new NodeListWrapper(nodes)) {
                result.add(n.getTextContent());
            }
        }
        return result;
    }

    public static List<VASTTrackingEvent> extractEvents(XPath xpath, Node node) throws XPathExpressionException {
        List<VASTTrackingEvent> events = new ArrayList<>();
        if (node == null) return events;

        NodeList eventsNodes = (NodeList) xpath.evaluate("TrackingEvents/Tracking", node, XPathConstants.NODESET);
        for (Node n : new NodeListWrapper(eventsNodes)) {
            NamedNodeMap attrs = n.getAttributes();
            Node event = attrs.getNamedItem("event");
            Node offset = attrs.getNamedItem("offset");
            if (event != null) {
                events.add(new VASTTrackingEvent(
                        attrs.getNamedItem("event").getNodeValue(),
                        n.getTextContent(),
                        offset != null ? offset.getNodeValue() : null
                ));
            }
        }
        return events;
    }

    public static VASTVideoClicks extractClicks(XPath xpath, Node node) throws XPathExpressionException {
        Node clickThroughNode = node == null ? null : (Node) xpath.evaluate("VideoClicks/ClickThrough", node, XPathConstants.NODE);
        NodeList clickTrackingNodes = node == null ? null : (NodeList) xpath.evaluate("VideoClicks/ClickTracking", node, XPathConstants.NODESET);
        String clickTrough = clickThroughNode != null ? clickThroughNode.getTextContent() : null;
        List<String> clickTracking = new ArrayList<>();
        if (clickTrackingNodes != null) {
            for (Node n : new NodeListWrapper(clickTrackingNodes)) {
                clickTracking.add(n.getTextContent());
            }
        }
        return new VASTVideoClicks(clickTracking, clickTrough);
    }

    public static List<VASTMedia> extractMedias(XPath xpath, Node node) throws XPathExpressionException {
        NodeList nodes = (NodeList) xpath.evaluate("MediaFiles/MediaFile", node, XPathConstants.NODESET);
        List<VASTMedia> result = new ArrayList<>();
        for (Node n : new NodeListWrapper(nodes)) {
            NamedNodeMap attrs = n.getAttributes();
            int bitrate = 0;
            try {
                Node attrNone = attrs.getNamedItem("bitrate");
                bitrate = attrNone == null ? 0 : Integer.parseInt(attrNone.getNodeValue());
            } catch (NumberFormatException ignored) {
            }
            result.add(new VASTMedia(
                    n.getTextContent(),
                    attrs.getNamedItem("type").getNodeValue(),
                    bitrate,
                    Integer.parseInt(attrs.getNamedItem("width").getNodeValue()),
                    Integer.parseInt(attrs.getNamedItem("height").getNodeValue())
            ));
        }
        return result;
    }

    public static VASTCalendar extractCalendar(XPath xpath, Node node) throws XPathExpressionException {
        Node calendarNone = (Node) xpath.evaluate("Calendar", node, XPathConstants.NODE);
        NamedNodeMap attrs = calendarNone.getAttributes();
        String[] attrName = new String[]{"name", "description", "start", "end", "timezone"};
        String[] values = new String[]{"", "", "", "", ""};
        for (int i = 0; i < attrName.length; i++) {
            Node attrNone = attrs.getNamedItem(attrName[i]);
            if (attrNone == null) {
                continue;
            }
            values[i] = attrNone.getNodeValue();
        }
        return new VASTCalendar(values[0], values[1], values[2], values[3], values[4]);
    }

    public static List<VASTCompanion> extractCompanions(XPath xpath, Node node) throws XPathExpressionException {
        NodeList nodes = (NodeList) xpath.evaluate("Companion", node, XPathConstants.NODESET);
        List<VASTCompanion> result = new ArrayList<>();
        for (Node n : new NodeListWrapper(nodes)) {
            Node resourceNode = (Node) xpath.evaluate("StaticResource", n, XPathConstants.NODE);
            if (resourceNode == null) continue;
            Node clickThroughNode = (Node) xpath.evaluate("CompanionClickThrough", n, XPathConstants.NODE);
            String clickThrough = clickThroughNode == null ? null : clickThroughNode.getTextContent();
            NamedNodeMap attrs = n.getAttributes();
            NamedNodeMap resourceAttrs = resourceNode.getAttributes();
            result.add(new VASTCompanion(
                    resourceNode.getTextContent(),
                    resourceAttrs.getNamedItem("creativeType").getNodeValue(),
                    Integer.parseInt(attrs.getNamedItem("width").getNodeValue()),
                    Integer.parseInt(attrs.getNamedItem("height").getNodeValue()),
                    clickThrough
            ));
        }
        return result;
    }

    public static Map<String, VASTExtension> extractExtensions(XPath xpath, Node node) throws XPathExpressionException {
        Map<String, VASTExtension> result = new HashMap<>();
        NodeList nodes = (NodeList) xpath.evaluate("Extensions/Extension", node, XPathConstants.NODESET);
        for (Node n : new NodeListWrapper(nodes)) {
            VASTExtension extension = new VASTExtension(n);
            result.put(extension.type, extension);
        }
        return result;
    }

    public static VASTSkipOffset extractSkipoffset(XPath xpath, Node node) throws XPathExpressionException {
        NamedNodeMap attrs = node.getAttributes();
        Node offset = attrs.getNamedItem("skipoffset");
        return (offset != null) ? VASTSkipOffset.parse(offset.getNodeValue()) : null;
    }

    public static int extractDuration(XPath xpath, Node node) throws XPathExpressionException {
        Node duration = (Node) xpath.evaluate("Duration", node, XPathConstants.NODE);
        return (duration != null) ? VASTAd.stringTime2Milliseconds(duration.getTextContent()) : -1;
    }

    public static String extractAdSystem(XPath xpath, Node node) {
        Node childNode = null;
        try {
            childNode = (Node) xpath.evaluate("AdSystem", node, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            Log.w(TAG, "Parse AdSystem failed", e);
        }
        String value = (childNode != null) ? childNode.getTextContent() : null;
        return (value != null) ? value.trim() : null;
    }

    public static VASTAd parseAd(XPath xpath, Node adNode) throws XPathExpressionException {
        String id = adNode.getAttributes().getNamedItem("id").getNodeValue();
        Node sequenceNode = adNode.getAttributes().getNamedItem("sequence");
        Integer sequence = sequenceNode != null ? Integer.parseInt(sequenceNode.getNodeValue()) : null;

        Node inLineNode = (Node) xpath.evaluate("InLine", adNode, XPathConstants.NODE);
        Node wrapperNode = (Node) xpath.evaluate("Wrapper", adNode, XPathConstants.NODE);
        if (inLineNode != null) {
            String adSystem = VASTParser.extractAdSystem(xpath, inLineNode);
            List<String> impressions = VASTParser.extractNodesValues(xpath, inLineNode, "Impression");
            List<String> errors = VASTParser.extractNodesValues(xpath, inLineNode, "Error");

            Node linearNode = (Node) xpath.evaluate("Creatives/Creative/Linear", inLineNode, XPathConstants.NODE);
            List<VASTTrackingEvent> events = VASTParser.extractEvents(xpath, linearNode);
            VASTVideoClicks clicks = VASTParser.extractClicks(xpath, linearNode);
            List<VASTMedia> medias = VASTParser.extractMedias(xpath, linearNode);
            VASTSkipOffset skipoffset = VASTParser.extractSkipoffset(xpath, linearNode);
            int duration = VASTParser.extractDuration(xpath, linearNode);

            Node companionNode = (Node) xpath.evaluate("Creatives/Creative/CompanionAds", inLineNode, XPathConstants.NODE);
            List<VASTCompanion> companions = companionNode != null ? VASTParser.extractCompanions(xpath, companionNode) : new ArrayList<VASTCompanion>();

            Map<String, VASTExtension> extensions = VASTParser.extractExtensions(xpath, inLineNode);

            return new VASTInline(
                    id,
                    sequence,
                    impressions,
                    errors,
                    events,
                    clicks,
                    medias,
                    companions,
                    extensions,
                    skipoffset,
                    adSystem,
                    duration
            );
        } else if (wrapperNode != null) {
            String adSystem = VASTParser.extractAdSystem(xpath, wrapperNode);
            List<String> impressions = VASTParser.extractNodesValues(xpath, wrapperNode, "Impression");
            List<String> errors = VASTParser.extractNodesValues(xpath, wrapperNode, "Error");

            Node linearNode = (Node) xpath.evaluate("Creatives/Creative/Linear", wrapperNode, XPathConstants.NODE);
            List<VASTTrackingEvent> events = VASTParser.extractEvents(xpath, linearNode);
            VASTVideoClicks clicks = VASTParser.extractClicks(xpath, linearNode);

            String adTagURI = ((Node) xpath.evaluate("VASTAdTagURI", wrapperNode, XPathConstants.NODE)).getTextContent();

            return new VASTWrapper(
                    id,
                    sequence,
                    impressions,
                    errors,
                    events,
                    clicks,
                    adTagURI,
                    adSystem
            );
        } else {
            throw new XPathExpressionException("No InLine or Wrapper TAG");
        }
    }

    public static VAST parseDocument(Document data) throws VASTException {
        XPath xpath = XPathFactory.newInstance().newXPath();
        List<VASTAd> ads = new ArrayList<>();

        NodeList adNodes;

        try {
            adNodes = (NodeList) xpath.evaluate("/VAST/Ad", data, XPathConstants.NODESET);
        } catch (XPathExpressionException e) {
            throw new VASTException("Invalid VAST", e);
        }

        for (Node adNode : new NodeListWrapper(adNodes)) {
            try {
                VASTAd ad = VASTParser.parseAd(xpath, adNode);
                ads.add(ad);
            } catch (XPathExpressionException e) {
                Log.w(TAG, "Parse Ad failed", e);
            }
        }

        return new VAST(ads);
    }

    public static void merge(VAST target, VAST source) {
        if (source.isWrapper() && target.isInline()) {
            VASTAd sourceAd = source.getWrapper();
            VASTAd targetAd = target.getInline();
            for (VASTTrackingEvent event : sourceAd.events) {
                targetAd.events.add(event);
            }
            for (String url : sourceAd.videoClicks.clickTracking) {
                targetAd.videoClicks.clickTracking.add(url);
            }
            for (String url : sourceAd.impressions) {
                targetAd.impressions.add(url);
            }
            for (String url : sourceAd.errors) {
                targetAd.errors.add(url);
            }
        }
    }
}
