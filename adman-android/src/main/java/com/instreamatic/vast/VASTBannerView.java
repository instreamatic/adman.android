package com.instreamatic.vast;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.instreamatic.adman.event.ControlEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.core.android.net.BitmapLoader;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.vast.model.VASTCompanion;

public class VASTBannerView extends ImageView implements View.OnClickListener {

    final private static String TAG = VASTBannerView.class.getSimpleName();

    final private VASTCompanion companion;
    final protected EventDispatcher dispatcher;

    private Bitmap image;

    public VASTBannerView(Context context, VASTCompanion companion, EventDispatcher dispatcher, boolean isClickable) {
        super(context);
        this.companion = companion;
        this.dispatcher = dispatcher;
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setScaleType(ImageView.ScaleType.FIT_CENTER);
        if (isClickable) {
            setOnClickListener(this);
        }

        /*if (companion != null) {
            prepare();
        }*/
    }

    public boolean isEmpty() {
        return companion == null;
    }

    final public Bitmap getBitmap() {
        return image;
    }

    public void prepare(final Runnable callback) {
        if (companion == null) {
            callback.run();
            return;
        }
        new BitmapLoader().GET(companion.url, new ICallback<Bitmap>() {
            @Override
            public void onSuccess(Bitmap data) {
                Log.d(TAG, "Load banner image success");
                image = data;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        setImageBitmap(image);
                    }
                });
                callback.run();
            }

            @Override
            public void onFail(Throwable t) {
                Log.e(TAG, "Load banner image failed", t);
                callback.run();
            }
        });
    }

    public void dispose() {
        post(new Runnable() {
            @Override
            public void run() {
                setImageBitmap(null);
            }
        });
        if (image != null) {
            image.recycle();
            image = null;
        }
    }

    @Override
    public void onClick(View view) {
        dispatcher.dispatch(new ControlEvent(ControlEvent.Type.CLICK));
    }
}
