package com.instreamatic.vast;

import android.util.Log;
import com.instreamatic.format.IFormat;
import com.instreamatic.format.ImageFormat;
import com.instreamatic.format.MediaFormat;
import com.instreamatic.vast.model.VASTCompanion;
import com.instreamatic.vast.model.VASTFile;
import com.instreamatic.vast.model.VASTMedia;

import java.util.List;

public class VASTSelector {

    final private static String TAG = VASTSelector.class.getSimpleName();

    final public static MediaFormat DEFAULT_MEDIA = new MediaFormat(new String[]{
            MediaFormat.MIMETYPE_AUDIO_MP3,
            MediaFormat.MIMETYPE_AUDIO_MPEG,
    });

    final public static ImageFormat DEFAULT_IMAGE = new ImageFormat(640, 640);

    private MediaFormat mediaFormat;
    private ImageFormat imageFormat;

    public VASTSelector(MediaFormat mediaFormat, ImageFormat imageFormat) {
        this.mediaFormat = mediaFormat == null ? DEFAULT_MEDIA : mediaFormat;
        this.imageFormat = imageFormat == null ? DEFAULT_IMAGE : imageFormat;
    }

    public VASTSelector() {
        this(null, null);
    }

    public MediaFormat getMediaFormat() {
        return mediaFormat;
    }

    public void setMediaFormat(MediaFormat mediaFormat) {
        this.mediaFormat = mediaFormat;
    }

    public ImageFormat getImageFormat() {
        return imageFormat;
    }

    public void setImageFormat(ImageFormat imageFormat) {
        this.imageFormat = imageFormat;
    }

    public VASTMedia selectMedia(List<VASTMedia> medias) {
        return new MediaSelector(mediaFormat).select(medias);
    }

    public VASTCompanion selectCompanion(List<VASTCompanion> companions) {
        return new ImageSelector(imageFormat).select(companions);
    }

    private interface ISelector<T extends VASTFile> {
        public T select(List<T> values);
    }

    abstract private class BaseSelector<T extends VASTFile, F extends IFormat> implements ISelector<T> {
        final protected F format;

        public BaseSelector(F format) {
            this.format = format;
        }

        private T methodDistance(List<T> values){
            T result = null;
            int distance = Integer.MAX_VALUE;
            for (T value : values) {
                if (format.contains(value.type)) {
                    int d = calcDistance(value);
                    if (d < distance) {
                        distance = d;
                        result = value;
                    }
                }
            }
            // Select value with any type
            if (result == null && !mediaFormat.strict) {
                distance = Integer.MAX_VALUE;
                for (T value : values) {
                    int d = calcDistance(value);
                    if (d < distance) {
                        distance = d;
                        result = value;
                    }
                }
            }
            return result;
        }
        // for method Distance
        abstract protected int calcDistance(T value);

        private T methodMatches(List<T> values){
            T result = null;
            for (T value : values) {
                if (format.contains(value.type)) {
                    if (checkMatches(value)) {
                        result = value;
                    }
                }
            }
            // Select value with any type
            if (result == null && !mediaFormat.strict) {
                for (T value : values) {
                    if (checkMatches(value)) {
                        result = value;
                    }
                }
            }
            return result;
        }
        // for method methodMatches
        protected boolean needMatches() {
            return false;
        }

        protected boolean checkMatches(T value) {
            return false;
        }

        @Override
        public T select(List<T> values) {
            T result = needMatches()
                    ? methodMatches(values)
                    : methodDistance(values);
            Log.d(TAG, "select: " + (result == null ? "null" : result));
            return result;
        }
    }

    private class MediaSelector extends BaseSelector<VASTMedia, MediaFormat> {
        private final MediaFormat media;

        public MediaSelector(MediaFormat format) {
            super(format);
            media = format;
        }

        @Override
        protected int calcDistance(VASTMedia value) {
            return Math.abs(format.getBitrate() - value.bitrate);
        }

        @Override
        protected boolean needMatches(){
            return !media.hasDefaultBitrate();
        }

        @Override
        protected boolean checkMatches(VASTMedia value){
            return format.getBitrate() == value.bitrate;
        }
    }

    private class ImageSelector extends BaseSelector<VASTCompanion, ImageFormat> {

        public ImageSelector(ImageFormat format) {
            super(format);
        }

        @Override
        protected int calcDistance(VASTCompanion value) {
            return Math.abs((format.width - value.width) + (format.height - value.height));
        }
    }
}
