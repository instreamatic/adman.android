package com.instreamatic.vast;

public class VASTException extends Exception {

    public VASTException(String message) {
        super(message);
    }

    public VASTException(String message, Throwable t) {
        super(message, t);
    }
}
