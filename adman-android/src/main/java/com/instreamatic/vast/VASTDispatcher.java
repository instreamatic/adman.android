package com.instreamatic.vast;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;
import com.instreamatic.adman.ActionUtil;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;
import com.instreamatic.vast.model.*;
import okhttp3.Response;

import java.util.List;

public class VASTDispatcher {

    final private static String TAG = VASTDispatcher.class.getSimpleName();

    final protected VASTAd ad;

    public VASTDispatcher(VASTAd ad) {
        this.ad = ad;
    }

    public void dispatch(VASTEvent event) {
        Log.d(TAG, "dispatch: " + event.name());
        switch (event) {
            case impression:
                sendImpression();
                break;
            case error:
                sendError();
                break;
            case click:
                sendClick();
                break;
            default:
                sendEvent(event);
                break;
        }
    }

    public void dispatch(String event) {
        try {
            VASTEvent vastEvent = VASTEvent.valueOf(event);
            dispatch(vastEvent);
        } catch (IllegalArgumentException e) {
            sendEvent(event);
        }
    }

    @SuppressLint("DefaultLocale")
    public void onProgress(int position) {
        String offset1 = String.format("%d:%02d:%02d", position / 3600, (position % 3600) / 60, (position % 60));
        String offset2 = offset1 + ".000";
        for (VASTTrackingEvent trackingEvent : ad.events) {
            if (trackingEvent.event.equals(VASTEvent.progress.name()) &&
                    (trackingEvent.offset.equals(offset1) || trackingEvent.offset.equals(offset2))) {
                send(trackingEvent.url);
            }
        }
    }

    public void onProgressPercent(int percent) {
        String offset = percent + "%";
        for (VASTTrackingEvent trackingEvent : ad.events) {
            if (trackingEvent.event.equals(VASTEvent.progress.name()) && trackingEvent.offset.equals(offset)) {
                send(trackingEvent.url);
            }
        }
        if (percent == 25) sendEvent(VASTEvent.firstQuartile);
        if (percent == 50) sendEvent(VASTEvent.midpoint);
        if (percent == 75) sendEvent(VASTEvent.thirdQuartile);
    }

    protected void sendImpression() {
        for (String url : ad.impressions) {
            send(url);
        }
    }

    protected void sendError() {
        for (String url : ad.errors) {
            send(url);
        }
    }

    protected void sendClick() {
        for (String url : ad.videoClicks.clickTracking) {
            send(url);
        }
    }

    protected void sendEvent(VASTEvent event) {
        sendEvent(event.name());
    }

    protected void sendEvent(String event) {
        for (VASTTrackingEvent trackingEvent : ad.events) {
            if (trackingEvent.event.equals(event)) {
                send(trackingEvent.url);
            }
        }
    }

    public static void send(String url) {
        Log.d(TAG, "GET: " + url);
        new PixelLoader().GET(url);
    }

    final static private class PixelLoader extends Loader<Void> {
        @Override
        protected void onResponse(Response response, ICallback<Void> callback) throws Exception {
            if (response != null){
                response.close();
            }
        }
    }
}
