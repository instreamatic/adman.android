package com.instreamatic.vast.model;

import java.util.List;

public class VAST {

    final public List<VASTAd> ads;

    public VAST(List<VASTAd> ads) {
        this.ads = ads;
    }

    public boolean isEmpty() {
        return ads.size() == 0;
    }

    public boolean isWrapper() {
        return !isEmpty() && ads.get(0).type.equals(VASTWrapper.TYPE);
    }

    public boolean isInline() {
        return !isEmpty() && ads.get(0).type.equals(VASTInline.TYPE);
    }

    public VASTWrapper getWrapper() {
        return isWrapper() ? (VASTWrapper) ads.get(0) : null;
    }

    public VASTInline getInline() {
        return isInline() ? (VASTInline) ads.get(0) : null;
    }

    public VASTAd getVAST() {
        return isEmpty() ? null : ads.get(0);
    }
}
