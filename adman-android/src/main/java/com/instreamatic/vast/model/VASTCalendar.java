package com.instreamatic.vast.model;

public class VASTCalendar {

    final public String name;
    final public String description;
    final public String begin;
    final public String end;
    final public String timezone;

    public VASTCalendar(String name, String description, String begin, String end, String timezone) {
        this.name = name;
        this.description = description;
        this.begin = begin;
        this.end = end;
        this.timezone = timezone;
    }
}