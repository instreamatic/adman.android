package com.instreamatic.vast.model;

public class VASTFile {
    final public String url;
    final public String type;

    public VASTFile(String url, String type) {
        this.url = url;
        this.type = type;
    }
}
