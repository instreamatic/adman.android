package com.instreamatic.vast.model;

public class VASTTrackingEvent {

    final public String event;
    final public String url;
    final public String offset;

    public VASTTrackingEvent(String event, String url, String offset) {
        this.event = event;
        this.url = url;
        this.offset = offset;
    }
}
