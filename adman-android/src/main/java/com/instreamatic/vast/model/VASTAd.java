package com.instreamatic.vast.model;

import android.util.Log;

import java.util.List;

public class VASTAd {

    final public String id;
    final public String type;
    final public Integer sequence;
    final public String adSystem;

    final public List<String> impressions;
    final public List<String> errors;

    final public List<VASTTrackingEvent> events;
    final public VASTVideoClicks videoClicks;

    public VASTAd(String id, String type, Integer sequence, List<String> impressions, List<String> errors, List<VASTTrackingEvent> events, VASTVideoClicks videoClicks, String adSystem) {
        this.id = id;
        this.type = type;
        this.sequence = sequence;
        this.impressions = impressions;
        this.errors = errors;
        this.events = events;
        this.videoClicks = videoClicks;
        this.adSystem = adSystem;
    }

    public static int intValueOfDef(String value, int def) {
        int res;
        try {
            res = Integer.valueOf(value);
        } catch (NumberFormatException e) {
            res = def;
        }
        return res;
    }

    public static int stringTime2Milliseconds(String value) {
        String[] timeParts = value.split("\\.");

        String[] timeItms = timeParts.length > 0 ? timeParts[0].split(":"): new String[0];
        int m_seconds = timeParts.length > 1 ? intValueOfDef(timeParts[1], 0): 0;

        int nItms = timeItms.length;
        int seconds = nItms > 0 ? intValueOfDef(timeItms[nItms-1], -1) : -1;
        int minutes = nItms > 1 ? intValueOfDef(timeItms[nItms-2], -1) : -1;;
        int hours = nItms > 2 ? intValueOfDef(timeItms[nItms-3], -1) : -1;;;
        if (hours < 0 && minutes < 0 && seconds < 0) {
            return -1;
        }
        int sec = (hours < 0 ? 0 : hours) * 60 * 60 + (minutes < 0 ? 0 : minutes) * 60 + (seconds < 0 ? 0 : seconds);
        return sec*1000 + m_seconds;
    }
}
