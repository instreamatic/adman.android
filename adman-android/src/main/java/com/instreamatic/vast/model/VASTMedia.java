package com.instreamatic.vast.model;


public class VASTMedia extends VASTFile {

    final public int bitrate;
    final public int width;
    final public int height;

    public VASTMedia(String url, String type, int bitrate, int width, int height) {
        super(url, type);
        this.bitrate = bitrate;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return "VASTMedia{" +
                "url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", bitrate=" + bitrate +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
