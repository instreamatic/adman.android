package com.instreamatic.vast.model;

public enum VASTEvent {
    impression,
    error,
    start,
    firstQuartile,
    midpoint,
    thirdQuartile,
    complete,
    pause,
    resume,
    click,
    progress,
    skip
}
