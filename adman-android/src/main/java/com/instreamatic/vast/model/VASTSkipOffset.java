package com.instreamatic.vast.model;

import android.util.Log;

public class VASTSkipOffset {

    final private static String TAG = VASTSkipOffset.class.getSimpleName();

    public int offset;
    public boolean isRate;

    /**
     * VASTSkipOffset constructor
     *
     * @param offset  0..100 | milliseconds
     * @param isRate rate: true(offset: 0..100).
     */
    public VASTSkipOffset(int offset, boolean isRate) {
        this.offset = offset;
        this.isRate = isRate;
    }

    public VASTSkipOffset(int offset) {
        this(offset, false);
    }

    final public static VASTSkipOffset parse(String value) {
        boolean isRate = false;
        int offset = 0;
        int n = value.length();
        if (value.endsWith("%")) {
            int val = VASTAd.intValueOfDef(value.substring(0, n-1), -1);
            if (val > -1 && val <= 100) {
                isRate = true;
                offset = val;
            } else {
                Log.w(TAG, "Parse SkipOffset failed: " + value);
                return null;
            }
        } else {
            offset = VASTAd.stringTime2Milliseconds(value);
            if (offset < 0) {
                Log.w(TAG, "Parse SkipOffset failed: " + value);
                return null;
            }

        }
        return new VASTSkipOffset(offset, isRate);
    }

    final public static boolean eventHappened(VASTSkipOffset offset, int position, int duration) {
        if (offset.isRate) {
            if (duration == 0) return false;
            int currRate = (position * 100) / duration;
            return currRate <= 100 && currRate >= offset.offset;
        } else {
            return position >= offset.offset;
        }
    }


}
