package com.instreamatic.vast.model;

import java.util.List;

public class VASTWrapper extends VASTAd {

    final public static String TYPE = "wrapper";

    final public String adTagURI;

    public VASTWrapper(String id, Integer sequence, List<String> impressions, List<String> errors,
                       List<VASTTrackingEvent> events, VASTVideoClicks videoClicks,
                       String adTagURI, String adSystem) {
        super(id, TYPE, sequence, impressions, errors, events, videoClicks, adSystem);
        this.adTagURI = adTagURI;
    }
}
