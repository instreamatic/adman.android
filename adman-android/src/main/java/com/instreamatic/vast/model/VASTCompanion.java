package com.instreamatic.vast.model;

public class VASTCompanion extends VASTFile {

    final public int width;
    final public int height;
    final public String clickThrough;

    public VASTCompanion(String url, String type, int width, int height, String clickThrough) {
        super(url, type);
        this.width = width;
        this.height = height;
        this.clickThrough = clickThrough;
    }

    public VASTCompanion(String url, String type, int width, int height) {
        this(url, type, width, height, null);
    }

    @Override
    public String toString() {
        return "VASTCompanion{" +
                "url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
