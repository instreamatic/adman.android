package com.instreamatic.vast.model;

import java.util.List;

public class VASTVideoClicks {

    final public List<String> clickTracking;
    final public String clickThrough;

    public VASTVideoClicks(List<String> clickTracking, String clickThrough) {
        this.clickTracking = clickTracking;
        this.clickThrough = clickThrough;
    }
}
