package com.instreamatic.vast.model;

import com.instreamatic.vast.VASTExtension;

import java.util.List;
import java.util.Map;

public class VASTInline extends VASTAd {

    final public static String TYPE = "inline";

    final public VASTSkipOffset skipoffset;
    final public List<VASTMedia> medias;
    final public List<VASTCompanion> companions;
    final public Map<String, VASTExtension> extensions;
    final public int duration; // milliseconds
    public VASTInline(String id, Integer sequence, List<String> impressions, List<String> errors,
                      List<VASTTrackingEvent> events, VASTVideoClicks videoClicks,
                      List<VASTMedia> medias, List<VASTCompanion> companions, Map<String, VASTExtension> extensions, VASTSkipOffset skipoffset,
                      String adSystem, int duration) {
        super(id, TYPE, sequence, impressions, errors, events, videoClicks, adSystem);
        this.medias = medias;
        this.companions = companions;
        this.extensions = extensions;
        this.skipoffset = skipoffset; // VASTSkipOffset.parse("50%");
        this.duration = duration;
    }

    public boolean isVoice() {
        return extensions.containsKey("response");
    }
}
