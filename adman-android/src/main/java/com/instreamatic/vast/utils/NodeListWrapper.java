package com.instreamatic.vast.utils;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Iterator;

public class NodeListWrapper implements Iterable<Node> {

    private final NodeList nodeList;

    public NodeListWrapper(NodeList nodeList) {
        this.nodeList = nodeList;
    }

    public Iterator<Node> iterator() {
        return new NodeListIterator(nodeList);
    }

    static class NodeListIterator implements Iterator<Node> {

        private final NodeList nodeList;
        private int index;
        private int length;

        private NodeListIterator(NodeList nodeList) {
            this.nodeList = nodeList;
            this.index = 0;
            this.length = nodeList.getLength();
        }

        public boolean hasNext() {
            return index < length;
        }

        public Node next() {
            return nodeList.item(index++);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("remove");
        }
    }
}
