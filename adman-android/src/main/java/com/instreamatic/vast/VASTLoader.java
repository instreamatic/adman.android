package com.instreamatic.vast;

import android.util.Log;
import com.instreamatic.core.android.net.XmlLoader;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;
import com.instreamatic.vast.model.VAST;
import okhttp3.Response;
import org.w3c.dom.Document;

public class VASTLoader extends Loader<VAST> {

    final private static int DEPTH_LIMIT = 10;

    private int depth = 0;
    private VAST parent;

    @Override
    protected void onResponse(Response response, ICallback<VAST> callback) throws Exception {
        Document document = XmlLoader.extractDocument(response);
        VAST result = VASTParser.parseDocument(document);

        if (parent != null) {
            VASTParser.merge(result, parent);
        }

        if (result.isWrapper()) {
            parent = result;
            if (++depth > DEPTH_LIMIT) {
                callback.onFail(new VASTException("Depth limit exceeded"));
            }
            String adTagURI = result.getWrapper().adTagURI;
            Log.d("VASTLoader", "adTagURI: " + adTagURI);
            this.runRequest(adTagURI);
        } else {
            callback.onSuccess(result);
        }
    }

    @Override
    public void doRelease() {
        super.doRelease();
        depth = 0;
    }
}
