package com.instreamatic.format;

public interface IFormat {
    public boolean contains(String mimeType);
}
