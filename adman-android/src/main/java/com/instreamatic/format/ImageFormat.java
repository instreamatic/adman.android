package com.instreamatic.format;

import java.util.Arrays;

public class ImageFormat implements IFormat {
    final public static String MIMETYPE_IMAGE_JPG = "image/jpg";
    final public static String MIMETYPE_IMAGE_PNG = "image/png";
    final public static String MIMETYPE_IMAGE_GIF = "image/gif";

    public final String[] mimeTypes;
    public final int width;
    public final int height;

    public ImageFormat(String[] mimeTypes, int width, int height) {
        this.mimeTypes = mimeTypes;
        this.width = width;
        this.height = height;
    }

    public ImageFormat(String mimeType, int width, int height) {
        this(new String[] {mimeType}, width, height);
    }

    public ImageFormat(int width, int height) {
        this(new String[] {
                MIMETYPE_IMAGE_JPG,
                MIMETYPE_IMAGE_PNG,
                MIMETYPE_IMAGE_GIF,
        }, width, height);
    }

    public boolean contains(String mimeType) {
        return Arrays.asList(mimeTypes).contains(mimeType);
    }
}