package com.instreamatic.format;

import java.util.Arrays;

public class MediaFormat implements IFormat  {

    final public static String MIMETYPE_AUDIO_MP3 = "audio/mp3";
    final public static String MIMETYPE_AUDIO_MPEG = "audio/mpeg";
    final public static String MIMETYPE_AUDIO_MP4 = "audio/mp4";
    final public static String MIMETYPE_AUDIO_AAC = "audio/aac";
    final public static String MIMETYPE_AUDIO_OGG = "audio/ogg";


    final public static int DEFAULT_BITRATE = 0;
    final private static int DEF_BITRATE = Bitrates.BITRATE_320.code;

    public final String[] mimeTypes;
    public final boolean strict;
    private final int bitrate; //0 - equal DEFAULT_BITRATE

    public MediaFormat(String[] mimeTypes, int bitrate, boolean strict) {
        this.mimeTypes = mimeTypes;
        this.bitrate = bitrate;
        this.strict = strict;
    }

    public MediaFormat(String mimeType, int bitrate, boolean strict) {
        this(new String[] {mimeType}, bitrate, strict);
    }

    public MediaFormat(int bitrate) {
        this(new String[] {
                MIMETYPE_AUDIO_MP3,
                MIMETYPE_AUDIO_MPEG,
                MIMETYPE_AUDIO_MP4,
                MIMETYPE_AUDIO_AAC,
                MIMETYPE_AUDIO_OGG,
        }, bitrate);
    }

    public int getBitrate(){
        return bitrate != 0  ? bitrate : DEF_BITRATE;
    }

    public MediaFormat(String[] mimeTypes, int bitrate) {
        this(mimeTypes, bitrate, false);
    }

    public MediaFormat(String[] mimeTypes) {
        this(mimeTypes, 0);
    }

    public MediaFormat(String mimeType, int bitrate) {
        this(mimeType, bitrate, false);
    }

    public MediaFormat(String mimeType) {
        this(mimeType, 0);
    }

    //IFormat
    @Override
    public boolean contains(String mimeType) {
        return Arrays.asList(mimeTypes).contains(mimeType);
    }

    public boolean hasDefaultBitrate(){
        return bitrate == 0;
    }


    public enum MIMETypes {
        AUDIO_MP3(MIMETYPE_AUDIO_MP3),
        AUDIO_MPEG(MIMETYPE_AUDIO_MPEG),
        AUDIO_MP4(MIMETYPE_AUDIO_MP4),
        AUDIO_AAC(MIMETYPE_AUDIO_AAC),
        AUDIO_OGG(MIMETYPE_AUDIO_OGG),
        ;

        final public String code;

        MIMETypes(String code) {
            this.code = code;
        }

        public static MIMETypes fromCode(String code) {
            for (MIMETypes item : values()) {
                if (item.code.equals(code)) {
                    return item;
                }
            }
            return null;
        }
    }


    public enum Bitrates {
        BITRATE_32(32000),
        BITRATE_96(96000),
        BITRATE_128(128000),
        BITRATE_160(160000),
        BITRATE_192(192000),
        BITRATE_256(256000),
        BITRATE_320(320000),
        ;

        final public int code;

        Bitrates(int code) {
            this.code = code;
        }

        public static Bitrates fromCode(int code) {
            for (Bitrates item : values()) {
                if (item.code == code) {
                    return item;
                }
            }
            return null;
        }
    }

}