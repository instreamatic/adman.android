package com.instreamatic.core.android.net;

import okhttp3.Response;
import org.json.JSONObject;

import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;

import java.nio.charset.Charset;

public class JsonLoader extends Loader<JSONObject> {

    @Override
    protected void onResponse(Response response, ICallback<JSONObject> callback) throws Exception {
        String string = response.body().source().readString(Charset.forName("UTF-8"));
        callback.onSuccess(new JSONObject(string));
    }
}
