package com.instreamatic.core.android;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.instreamatic.adman.ActionUtil;
import com.instreamatic.adman.event.ActivityEvent;
import com.instreamatic.adman.event.ReceiverEvent;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class ActionIntentStorage{
    final private static String TAG = "Adman."+ActionIntentStorage.class.getSimpleName();
    private List<Intent> listParameters = new ArrayList<>();
    private Object lock = new Object();

    private StorageInstance storage = null;

    public ActionIntentStorage(Context context){
        this.init(context);
    }

    private void init(Context context) {
        Log.d(TAG, "init storage");
        if (this.storage == null) {
            Log.d(TAG, "create storage");
            this.storage = new StorageInstance(context, this);
        }
    }

    public void done(Context context) {
        this.clear();
        if (this.storage != null) {
            this.storage.done(context);
            this.storage = null;
        }
    }

    public boolean add(final Intent value) {
        if (this.storage == null){
            Log.d(TAG, "add, storage == null");
            return false;
        }
        this.append(value);
        return true;
    }

    public void start(Context context) {
        if (this.storage == null){
            Log.d(TAG, "add, storage == null");
            return;
        }
        this.run(context);
    }

    public void setAutoStart(boolean value) {
        if (this.storage == null) return;
        this.storage.autoStart = value;
    }

    public boolean needStorage() {
        return this.storage != null && this.storage.needStorage();
    }

    public Activity getCurrentActivity() {
        return this.storage.getCurrentActivity();
    }

    //private methods
    private void append(final Intent value) {
        Log.d(TAG, "add intent, count: " + listParameters.size());
        listParameters.add(value);
    }

    private void clear() {
        listParameters.clear();
    }

    private void run(Context context) {
        synchronized (lock) {
            final WeakReference<Context> contextRef = new WeakReference<>(context);
            if ((context != null)) {
                Runnable task = new Runnable() {
                    @Override
                    public void run() {
                        int size = listParameters.size();
                        if (size > 0) {
                            Intent intent = listParameters.remove(0);
                            Log.d(TAG, String.format("run intent, count: %d", size));
                            if (size > 1) asyncRun(this);
                            Context context = contextRef.get();
                            if (context != null) {
                                ActionUtil.startActivityWithFirst(context, intent);
                            }
                        }
                    }
                };
                asyncRun(task);
            }
        }
    }

    private void asyncRun(Runnable callback) {
        Thread thread = new Thread(callback);
        thread.start();
    }

    /*
     *
     */
    private static class StorageInstance implements ReceiverEvent.Listener, ActivityEvent.Listener {
        private PhoneUnlockedReceiver receiver;
        private ActivityLifecycleEvent activityEvent;
        public boolean autoStart = true;
        final private WeakReference<ActionIntentStorage> storageRef;

        public StorageInstance(Context context, ActionIntentStorage storage) {
            Log.d(TAG, "version_sdk: " + android.os.Build.VERSION.SDK_INT);
            storageRef = new WeakReference<>(storage);
            activityEvent = ActivityLifecycleEvent.init(context);
            activityEvent.getDispatcher().addListener(ActivityEvent.TYPE, this, ActivityEvent.PRIORITY_DEFAULT);
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
//                activityEvent = ActivityLifecycleEvent.init(context);
//                activityEvent.getDispatcher().addListener(ActivityEvent.TYPE, this, ActivityEvent.PRIORITY_DEFAULT);
//            } else {
//                receiver = PhoneUnlockedReceiver.init(context);
//                receiver.getDispatcher().addListener(ReceiverEvent.TYPE, this, ReceiverEvent.PRIORITY_DEFAULT);
//            }
        }

        public void done(Context context) {
            if (activityEvent != null) {
                activityEvent.getDispatcher().removeListener(ActivityEvent.TYPE, this);
                ActivityLifecycleEvent.done(context, activityEvent);
                activityEvent = null;
            }
        }

        /*
         * ReceiverEvent.Listener
         */
        public void onReceiverEvent(ReceiverEvent event) {
            switch (event.getType()) {
                case PHONE_UNLOCKED:
                    ActionIntentStorage storage = storageRef.get();
                    if (storage != null) storage.run(event.getContext());
                    break;
                case PHONE_LOCKED:
                    break;
            }
        }

        /*
         * ActivityEvent.Listener
         */
        public void onActivityEvent(ActivityEvent event) {
            switch (event.getType()) {
                case ON_RESUMED:
                    Log.d(TAG, "event on_resumed, autoStart: " + autoStart);
                    ActionIntentStorage storage = storageRef.get();
                    if (storage != null && autoStart) storage.run(event.getActivity());
                    break;
                case ON_STOPPED:
                    break;
            }

        }

        public boolean needStorage() {
            if (receiver != null) {
                return !PhoneUnlockedReceiver.isScreenOn();
            }
            if (activityEvent != null) {
                return !activityEvent.isDisplay();
            }
            return false;
        }

        public Activity getCurrentActivity() {
            return activityEvent == null? null : activityEvent.getActivity();
        }

    }

}