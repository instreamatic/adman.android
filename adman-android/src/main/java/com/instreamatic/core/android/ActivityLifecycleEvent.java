package com.instreamatic.core.android;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.event.ActivityEvent;
import com.instreamatic.adman.event.EventDispatcher;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;


public class ActivityLifecycleEvent implements Application.ActivityLifecycleCallbacks {
    final private static String TAG = "Adman."+ActivityLifecycleEvent.class.getSimpleName();
    private Timer mActivityTimer;
    private TimerTask mActivityTimerTask;
    private long timer_interval = 0;
    private final long ACTIVITY_TIME_MS = 1000;

    private static final boolean LOG_DEBUG = true;

    public boolean isDisplay = true;
    public boolean wasStopped = true;

    //private Activity eventActivity;
    private WeakReference<Activity> eventActivity = null;

    final public EventDispatcher dispatcher;


    public ActivityLifecycleEvent() {
        this.dispatcher = new EventDispatcher();
    }


    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    public boolean isDisplay() {
        return isDisplay;
    }

    public Activity getActivity() {
        return this.eventActivity == null ? null : this.eventActivity.get();
    }

    /**
     *  Application.ActivityLifecycleCallbacks
     **/
    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        this.eventActivity = new WeakReference<>(activity);
        if (LOG_DEBUG) Log.d(TAG, "onCreated: " + activity);
    }

    @Override
    public void onActivityStarted(Activity activity) {
        this.eventActivity = new WeakReference<>(activity);;
        if (LOG_DEBUG) Log.d(TAG, "onStarted: " + activity);
    }

    @Override
    public void onActivityResumed(Activity activity) {
        if (LOG_DEBUG) Log.d(TAG, "onResume: " + activity);
        // eventActivity == activity - from background,  else - navigating between the activities
        Activity activity_ref = this.getActivity();
        timer_interval = activity_ref == activity ? 0 :ACTIVITY_TIME_MS;
        this.eventActivity = new WeakReference<>(activity);
        this.startActivityTimer(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if (LOG_DEBUG) Log.d(TAG, "onPaused: " + activity);
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityStopped(Activity activity) {
        if (LOG_DEBUG) Log.d(TAG, "onStopped: " + activity);
        this.eventActivity = null;
        stopActivityTimer(activity);
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (LOG_DEBUG) Log.d(TAG, "onDestroyed: " + activity);
        this.eventActivity = null;
    }



    static public ActivityLifecycleEvent init(Context context) {
        ActivityLifecycleEvent receiver;
        try {
            receiver = new ActivityLifecycleEvent();
            Application contextApp = (Application)context.getApplicationContext();
            contextApp.registerActivityLifecycleCallbacks(receiver);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.toString());
            receiver = null;
        }
        return receiver;
    }

    static public void done(Context context, ActivityLifecycleEvent receiver) {
        try {
            Application contextApp = (Application)context.getApplicationContext();
            contextApp.unregisterActivityLifecycleCallbacks(receiver);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.toString());
        }
    }

    public void startActivityTimer(final Activity activity) {
        this.clearTimer();
        this.mActivityTimer = new Timer();
        this.mActivityTimerTask = new TimerTask() {
            public void run() {
                ActivityLifecycleEvent.this.isDisplay = true;
                dispatcher.dispatch(new ActivityEvent(ActivityEvent.Type.ON_RESUMED, activity));
            }
        };
        this.mActivityTimer.schedule(mActivityTimerTask, this.timer_interval);
    }

    public void stopActivityTimer(Activity activity) {
        this.clearTimer();
        if (this.wasStopped) {
            this.isDisplay = false;
            dispatcher.dispatch(new ActivityEvent(ActivityEvent.Type.ON_STOPPED, activity));
        } else {
            this.wasStopped = true;
        }
    }

    private void clearTimer() {
        if (this.mActivityTimerTask != null) {
            this.mActivityTimerTask.cancel();
            this.mActivityTimerTask = null;
        }

        if (this.mActivityTimer != null) {
            this.mActivityTimer.cancel();
            this.mActivityTimer = null;
        }
    }
}