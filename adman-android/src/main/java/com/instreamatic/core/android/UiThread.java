package com.instreamatic.core.android;

import android.os.Handler;
import android.os.Looper;


final public class UiThread {

    private static Thread uiThread;
    private static Handler handler;

    public static void run(Runnable runnable) {
        if (Thread.currentThread() == uiThread) {
            runnable.run();
        } else {
            if (handler == null) handler = new Handler(Looper.getMainLooper());
            handler.post(runnable);
        }
    }

    public static void setThread(Thread uiThread) {
        UiThread.uiThread = uiThread;
    }

    public static Handler handler() {
        if (handler == null) handler = new Handler(Looper.getMainLooper());
        return handler;
    }
}

