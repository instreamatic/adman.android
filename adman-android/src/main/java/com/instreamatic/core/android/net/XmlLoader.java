package com.instreamatic.core.android.net;

import okhttp3.Response;
import org.w3c.dom.Document;

import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class XmlLoader extends Loader<Document> {

    public static Document extractDocument(Response response) throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        return db.parse(response.body().source().inputStream());
    }

    @Override
    protected void onResponse(Response response, ICallback<Document> callback) throws Exception {
        callback.onSuccess(XmlLoader.extractDocument(response));
    }
}
