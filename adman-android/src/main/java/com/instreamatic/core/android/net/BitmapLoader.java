package com.instreamatic.core.android.net;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;
import okhttp3.Response;


public class BitmapLoader extends Loader<Bitmap> {

    @Override
    protected void onResponse(Response response, ICallback<Bitmap> callback) throws Exception {
        callback.onSuccess(BitmapFactory.decodeStream(response.body().source().inputStream()));
    }
}
