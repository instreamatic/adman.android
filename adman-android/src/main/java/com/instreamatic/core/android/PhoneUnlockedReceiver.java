package com.instreamatic.core.android;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.event.ReceiverEvent;

public class PhoneUnlockedReceiver extends BroadcastReceiver {
    final private static String TAG = "PhoneUnlockedReceiver";

    final protected EventDispatcher dispatcher;
    static private boolean isScreenOn = true;

    public PhoneUnlockedReceiver() {
        this.dispatcher = new EventDispatcher();
    }


    public EventDispatcher getDispatcher() {
        return this.dispatcher;
    }

    /**
     *  BroadcastReceiver
     **/
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)) {
            isScreenOn = true;
            Log.d(TAG, "Phone unlocked");
            dispatcher.dispatch(new ReceiverEvent(ReceiverEvent.Type.PHONE_UNLOCKED, context));
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            isScreenOn = false;
            Log.d(TAG, "Phone locked");
            dispatcher.dispatch(new ReceiverEvent(ReceiverEvent.Type.PHONE_LOCKED, context));
        }
    }

    static public boolean isScreenOn() {
        return isScreenOn;
    }

    static public PhoneUnlockedReceiver init(Context context) {
        PhoneUnlockedReceiver receiver;
        try {
            receiver = new PhoneUnlockedReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_USER_PRESENT);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            context.registerReceiver(receiver, filter);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.toString());
            receiver = null;
        }
        return receiver;
    }

    /*
    static public void done(Context context, PhoneUnlockedReceiver receiver) {
        context.unregisterReceiver(receiver);
    }
    */
}