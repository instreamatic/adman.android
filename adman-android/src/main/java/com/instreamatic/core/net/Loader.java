package com.instreamatic.core.net;

import android.os.Build;

import okhttp3.*;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;


abstract public class Loader<D> implements ILoader<D> {

    private static String userAgent = "Android " + Build.VERSION.RELEASE;

    public static String getUserAgent() {
        return userAgent;
    }

    public static void setUserAgent(String userAgent) {
        Loader.userAgent = userAgent;
    }

    protected String url;
    protected Map<String, String> form;
    protected Map<String, String> headers;
    protected RequestMethod method;
    protected ICallback<D> callback;
    protected int timeout;
    protected Set<String> redirectSet;
    protected boolean isWaiting;

    protected boolean busy;

    public Loader() {
        busy = false;
        timeout = 10000;
    }

    abstract protected void onResponse(Response response, ICallback<D> callback) throws Exception;

    public void request(String url, RequestMethod method, Map<String, String> headers, ICallback<D> callback) {
        this.url = url;
        this.method = method;
        this.headers = headers;
        this.callback = callback;
        try {
            runRequest(this.url);
        } catch (Exception e) {
            doFail(e);
        }
    }

    public void GET(String url) {
        request(url, RequestMethod.GET, null, null);
    }

    public void GET(String url, ICallback<D> callback) {
        request(url, RequestMethod.GET, null, callback);
    }

    public void GET(String url, Map<String, String> headers, ICallback<D> callback) {
        request(url, RequestMethod.GET, headers, callback);
    }

    @Override
    public void POST(String url, Map<String, String> form, ICallback<D> callback) {
        this.form = form;
        request(url, RequestMethod.POST, null, callback);
    }

    protected void checkIsBusy() throws ConnectException {
        if (busy) throw new ConnectException("Connection is busy for " + url);
    }

    protected RequestBody buildBody() {
        FormBody.Builder builder = new FormBody.Builder();
        if (form != null) {
            for (Map.Entry<String, String> entry : form.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }

    protected void runRequest(String url) throws IOException {
        checkIsBusy();
        Request.Builder request = new Request.Builder()
                .url(url)
                .header("User-Agent", userAgent);
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request = request.header(entry.getKey(), entry.getValue());
            }
        }
        switch (method) {
            case GET:
                request = request.get();
                break;
            case POST:
                request = request.post(buildBody());
                break;
            default:
                throw new ConnectException("Unsupported request method: " + method + " for " + this.url);
        }
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(timeout, TimeUnit.SECONDS)
                .build();

        isWaiting = true;
        client.newCall(request.build()).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                doFail(e);
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    processResponse(response);
                } catch (Exception ex) {
                    doFail(ex);
                }
            }
        });
    }

    private void processResponse(Response response) throws Exception {
        if (response.isRedirect()) {
            String location = response.header("Location");
            if (location != null) {
                if (redirectSet == null) redirectSet = new HashSet<>();
                if (redirectSet.contains(location)) {
                    throw new ConnectException("Cyclic Redirect for " + url);
                }
                redirectSet.add(location);
                runRequest(location);
            } else {
                throw new ConnectException("Incorrect Redirect for " + url);
            }
        } else if (response.isSuccessful()) {
             onResponse(response, new ICallback<D>() {
                 @Override
                 public void onSuccess(D data) {
                     doSuccess(data);
                 }

                 @Override
                 public void onFail(Throwable t) {
                     doFail(t);
                 }
             });


        } else {
            throw new ConnectException("HTTP status code " + response.code() + " for " + url);
        }
    }

    public void doRelease() {
        form = null;
        redirectSet = null;
        busy = false;
    }

    public void doSuccess(D data) {
        doRelease();
        if (callback != null) callback.onSuccess(data);
    }

    public void doFail(Throwable t) {
        doRelease();
        if (callback != null) callback.onFail(t);
    }
}
