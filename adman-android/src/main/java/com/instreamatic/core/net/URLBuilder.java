package com.instreamatic.core.net;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class URLBuilder {

    private String base;
    private Map<String, String> query;

    public URLBuilder(String base) {
        this.base = base;
        this.query = new HashMap<>();
    }

    public void putQuery(String name, Object value) {
        if (value == null) {
            query.put(name, null);
        } else {
            query.put(name, String.valueOf(value));
        }
    }

    public String toString() {
        if (query.isEmpty()) {
            return base;
        } else {
            StringBuilder builder = new StringBuilder(base);
            String glue = "?";
            for (String key : query.keySet()) {
                String value = query.get(key);
                if (value != null) {
                    try {
                        String keyString = URLEncoder.encode(key, "UTF-8");
                        String valueString = URLEncoder.encode(query.get(key), "UTF-8");
                        builder.append(glue);
                        builder.append(keyString);
                        builder.append("=");
                        builder.append(valueString);
                        glue = "&";
                    } catch (UnsupportedEncodingException ignored) {
                    }
                }
            }
            return builder.toString();
        }
    }
}
