package com.instreamatic.core.net;

import okhttp3.Response;

import java.nio.charset.Charset;

public class TextLoader extends Loader<String> {

    @Override
    protected void onResponse(Response response, ICallback<String> callback) throws Exception {
        callback.onSuccess(response.body().source().readString(Charset.forName("UTF-8")));
    }
}
