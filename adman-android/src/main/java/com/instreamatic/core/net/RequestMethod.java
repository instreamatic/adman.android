package com.instreamatic.core.net;


public enum RequestMethod {
    GET("get"),
    POST("post");

    final public String id;

    RequestMethod(String id) {
        this.id = id;
    }
}
