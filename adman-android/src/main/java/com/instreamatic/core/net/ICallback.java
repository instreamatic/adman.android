package com.instreamatic.core.net;


public interface ICallback<D> {
    public void onSuccess(D data);

    public void onFail(Throwable t);
}
