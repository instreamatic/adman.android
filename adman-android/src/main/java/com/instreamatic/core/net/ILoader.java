package com.instreamatic.core.net;

import java.util.Map;

public interface ILoader<D> {
    public void request(String url, RequestMethod method, Map<String, String> headers, ICallback<D> callback);

    public void GET(String url, ICallback<D> callback);
    public void POST(String url, Map<String, String> form, ICallback<D> callback);
}
