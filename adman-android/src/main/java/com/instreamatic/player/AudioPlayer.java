package com.instreamatic.player;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;


public class AudioPlayer implements MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener{

    final private static String TAG = "Adman." + AudioPlayer.class.getSimpleName();
    final private static boolean customPrepare = true;

    public enum State {
        PREPARE,
        READY,
        STOPPED,
        PLAYING,
        PAUSED,
        ERROR
    }

    protected Context context;
    protected MediaPlayer mediaPlayer;
    private String url;

    private StateListener stateListener;
    private ProgressListener progressListener;
    private CompleteListener completeListener;

    private Handler handler;
    private State state;
    private float volume;
    private boolean autoplay;
    private int streamType;
    private int timeoutPrepare;
    private boolean needToStart = false;
    private String name;
    private String tag_name;


    public static AudioPlayer createNotification(Context context, String url) {
        return new AudioPlayer(context, url,false, AudioManager.STREAM_NOTIFICATION);
    }

//    public static AudioPlayer playNotification(Context context, String url) {
//        return new AudioPlayer(context, url,true, AudioManager.STREAM_NOTIFICATION);
//    }

    public AudioPlayer(Context context, String url, boolean autoplay) {
        this(context, url, autoplay, AudioManager.STREAM_MUSIC);
    }

    private AudioPlayer(Context context, String url, boolean autoplay, int streamType) {
        this.context = context;
        this.url = url;
        this.autoplay = autoplay;
        this.streamType = streamType;

        state = null;
        volume = 1.0f;
        timeoutPrepare = 5; //seconds

        if (customPrepare) {
            mediaPlayer = new MediaPlayer();
        } else {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(url));
        }
        if (mediaPlayer == null) {
            changeState(State.ERROR);
        } else {
            setAudioAttributes();
            mediaPlayer.setVolume(volume, volume);
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnCompletionListener(this);
            mediaPlayer.setOnErrorListener(this);
        }

        if (customPrepare) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    prepare();
                }
            }).start();
        }
    }

    private void setAudioAttributes() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            mediaPlayer.setAudioAttributes(
                    (streamType == AudioManager.STREAM_NOTIFICATION)
                        ? new AudioAttributes.Builder()
                                .setLegacyStreamType(AudioManager.STREAM_NOTIFICATION)
                                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                .build()
                        : new AudioAttributes.Builder()
                                .setLegacyStreamType(AudioManager.STREAM_MUSIC)
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build()
            );
        } else {
            mediaPlayer.setAudioStreamType(streamType);
        }
    }

    public void setName(String value) {
        this.name = value;
        this.tag_name = (value == null || value.isEmpty()) ? TAG : TAG + "." + value;
    }

    public void setNeedToStart(boolean value) {
        this.needToStart = value;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public State getState() {
        return state;
    }

    public int getPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    public void setStateListener(StateListener stateListener) {
        this.stateListener = stateListener;
    }

    public void setProgressListener(ProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public void setCompleteListener(CompleteListener completeListener) {
        this.completeListener = completeListener;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
        mediaPlayer.setVolume(volume, volume);
    }

    public void play() {
        Log.d(tag_name, "play, state: " + state);
        if (state == State.PAUSED || state == State.READY) {
            mediaPlayer.start();
            changeState(State.PLAYING);
        }
    }

    public void toStart(boolean needPlay) {
        if (state != State.ERROR || state != State.PREPARE) {
            changeState(State.READY);
            mediaPlayer.seekTo(0);
            if (needPlay) {
                play();
            }
        }
    }

    public void pause() {
        if (state == State.PLAYING) {
            mediaPlayer.pause();
            changeState(State.PAUSED);
        }
    }

    public void resume() {
        play();
    }

    public void rewind() {
        if (state == State.PLAYING || state == State.PAUSED) {
            mediaPlayer.seekTo(0);
        }
    }

    public void stop() {
        if (state == State.PLAYING || state == State.PAUSED) {
            mediaPlayer.stop();
            changeState(State.STOPPED);
        }
    }

    public void dispose() {
        stateListener = null;
        progressListener = null;
        completeListener = null;

        stop();
        if (mediaPlayer != null) {
            mediaPlayer.setOnPreparedListener(null);
            mediaPlayer.setOnCompletionListener(null);
            mediaPlayer.setOnErrorListener(null);
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    protected void changeState(State state) {
        Log.d(tag_name, "changeState: " + state);

        if (this.state != state) {
            onChangeState(this.state, state);
            this.state = state;
            if (stateListener != null) {
                stateListener.onStateChange(state);
            }
        }
    }

    private void prepare() {
        Log.d(tag_name,"prepare, url: " + url);
        changeState(State.PREPARE);
        try {
            //if will be error then call onError and dispose()
            mediaPlayer.setDataSource(url);
            if (mediaPlayer != null) {
                mediaPlayer.prepareAsync();
            }
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    if(state == State.PREPARE) {
                        if (mediaPlayer != null) mediaPlayer.reset();
                        Log.e(tag_name, "MediaPlayer timeout for prepare, url: " + url);
                        changeState(State.ERROR);
                    }

                }
            }, timeoutPrepare * 1000);
        } catch (IOException e) {
            Log.e(tag_name, "Fail to prepare mp3", e);
            changeState(State.ERROR);
        }
        catch (IllegalStateException e) {
            Log.e(tag_name, "Fail to prepare mp3", e);
            changeState(State.ERROR);
        }
    }

    private void startPlayProgressUpdater() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    startPlayProgressUpdater();
                }
            };
            if (handler == null) handler = new Handler();
            handler.postDelayed(notification, 1000);

            onChangeProgress(mediaPlayer.getCurrentPosition(), mediaPlayer.getDuration());
            if (progressListener != null) {
                progressListener.onProgressChange(mediaPlayer.getCurrentPosition(), mediaPlayer.getDuration());
            }
        }
    }

    protected void onChangeState(State prevState, State nextState) {
        switch (nextState) {
            case PLAYING:
                startPlayProgressUpdater();
                break;
        }
    }

    protected void onChangeProgress(int position, int duration) {}


    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        changeState(State.READY);
        if (autoplay) {
            play();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        changeState(State.STOPPED);
        if (completeListener != null) {
            completeListener.onComplete();
        }
        if (needToStart) {
            toStart(false);
        }
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        Log.e(tag_name, String.format("onError, url: %s", url));
        changeState(State.ERROR);
        return false;
    }


    public interface StateListener {
        void onStateChange(State state);
    }

    public interface ProgressListener {
        void onProgressChange(int position, int duration);
    }

    public interface CompleteListener {
        void onComplete();
    }
}
