package com.instreamatic.adman;

import android.os.Bundle;

import java.util.*;

final public class Region {
    final private static String EUROPE_VOICE_SERVER = "wss://v.instreamatic.com";
    final private static String GLOBAL_VOICE_SERVER = "wss://v3.instreamatic.com";
    final private static String DEFAULT_VOICE_SERVER = EUROPE_VOICE_SERVER;

    final private static Map<String, Region> valuesByName = new TreeMap<>();

    final public static Region EUROPE = new Region(
            "https://x.instreamatic.com",
            "https://xs.instreamatic.com",
            EUROPE_VOICE_SERVER,
            "EUROPE"
    );

    final public static Region GLOBAL = new Region(
            "https://x3.instreamatic.com",
            "https://xs3.instreamatic.com",
            GLOBAL_VOICE_SERVER,
            "GLOBAL"
    );

    final public static Region INDIA = new Region(
            "https://x-gaana.instreamatic.com",
            "https://xs-gaana.instreamatic.com",
            EUROPE_VOICE_SERVER,
            "INDIA"
    );

    final public static Region DEMO = new Region(
            "https://d3x.instreamatic.com",
            "https://d3xs.instreamatic.com",
            GLOBAL_VOICE_SERVER,
            "DEMO"
    );

    final public static Region TEST = new Region(
            "https://test.instreamatic.com/x",
            "https://test.instreamatic.com/xs",
            EUROPE_VOICE_SERVER,
            "TEST"
    );

    final public static Region DEVELOP = new Region(
            "http://x.un.local",
            "http://xs.un.local",
            "ws://192.168.0.196:8081",
            "DEVELOP"
    );

    final public static Region DEFAULT = EUROPE;

    final public String adServer;
    final public String statServer;
    final public String voiceServer;

    final private String name;

    public Region(String adServer, String statServer, String voiceServer, String name) {
        this.adServer = adServer;
        this.statServer = statServer;
        this.voiceServer = voiceServer;
        this.name = name;
        valuesByName.put(this.name, this);
    }

    public Region(String adServer, String statServer, String voiceServer) {
        this(adServer, statServer, voiceServer, adServer);
    }

    public Region(String adServer, String statServer) {
        this(adServer, statServer, DEFAULT_VOICE_SERVER);
    }

    final public String name() {
        return name;
    }

    final public int ordinal() {
        return new ArrayList<>(valuesByName.values()).indexOf(this);
    }

    final public String toString() {
        return name();
    }

    final public Bundle toBundle() {
        Bundle result = new Bundle();
        result.putString("adServer", adServer);
        result.putString("statServer", statServer);
        result.putString("voiceServer", voiceServer);
        result.putString("name", name());
        return result;
    }

    final public static Region[] values() {
        return valuesByName.values().toArray(new Region[0]);
    }

    final public static Region valueOf(String value) {
        return valuesByName.get(value);
    }

    final public static Region valueOf(Bundle value) {
        String name = value.getString("name");
        if (valuesByName.containsKey(name)) {
            return valuesByName.get(name);
        } else {
            return new Region(
                    value.getString("adServer"),
                    value.getString("statServer"),
                    value.getString("voiceServer"),
                    name
            );
        }
    }
}
