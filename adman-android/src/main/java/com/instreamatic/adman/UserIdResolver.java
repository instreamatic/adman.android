package com.instreamatic.adman;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class UserIdResolver {

    final private static String TAG = UserIdResolver.class.getSimpleName();

    private static UserId userId;

    private static List<Callback> callbacks = new ArrayList<>();

    public static void resolve(final Context context, final Callback callback) {
        if (userId == null) {
            callbacks.add(callback);
            userId = new UserId();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // userId.deviceId = resolveDeviceId(context);
                    userId.advertisingId = resolveAdvertisingId(context);
                    userId.resolved = true;
                    for (Callback c : callbacks) {
                        c.onResolved(userId);
                    }
                    callbacks.clear();
                }
            }).start();
        } else if (userId.resolved) {
            callback.onResolved(userId);
        } else {
            callbacks.add(callback);
        }
    }

    private static String resolveAdvertisingId(Context context) {
        try {
            AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(context);
            if (info.isLimitAdTrackingEnabled()) {
                throw new Exception("LimitAdTrackingEnabled");
            }
            Log.d(TAG, "advertisingId: " + info.getId());
            return info.getId();
        } catch (Exception e) {
            Log.e(TAG, "resolveAdvertisingId: " + e.getMessage());
        }
        return null;
    }

    private static String resolveDeviceId(Context context) {
        return Installation.id(context);
    }

    public static interface Callback {
        public void onResolved(UserId userId);
    }
}

