package com.instreamatic.adman;

public enum Type {
    ANY("any", true),
    AUDIO("audio"),
    AUDIO_ONLY("radio"),
    AUDIO_PLUS("digital"),
    VOICE("voice", true),
    ALEXA("alexa");

    final public String id;
    final public boolean voice;

    Type(String id, boolean voice) {
        this.id = id;
        this.voice = voice;
    }

    Type(String id) {
        this(id, false);
    }

    final public static Type DEFAULT = AUDIO;
}