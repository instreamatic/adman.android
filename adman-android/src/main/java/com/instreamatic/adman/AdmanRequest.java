package com.instreamatic.adman;

import android.os.Parcel;
import android.os.Parcelable;
import com.instreamatic.core.net.URLBuilder;

import java.util.Map;

public class AdmanRequest implements Parcelable {

    final private static String apiVersion = "v5";

    /**
     * Site id.
     */
    final public Integer siteId;

    /**
     * Player id.
     */
    final public Integer playerId;


    /**
     * Region
     */
    final public Region region;

    /**
     * Slot
     */
    final public Slot slot;

    /**
     * Format
     */
    final public Type type;

    /**
     * Ads count
     */
    final public Integer adsCount;

    /**
     * Max duration
     */
    final public Integer maxDuration;

    /**
     * Preview
     */
    final public Integer preview;

    /**
     * consent string
     */
    final public String consentString;

    /**
     * US Privacy
     */
    final public String usPrivacy;

    /**
     * Campaign id.
     */
    final public Integer campaignId;

    /**
     * gender identity
     */
    final public Gender gender;

    /**
     * age
     */
    final public Age age;

    /**
     * adUrlAPI
     */
    final public String adUrlAPI;

    /**
     * Constructor.
     *
     * @param siteId        site id.
     * @param playerId      player id.
     * @param region        region.
     * @param slot          slot.
     * @param type          type.
     * @param adsCount      adsCount.
     * @param maxDuration   maxDuration.
     * @param preview       preview.
     * @param consentString consent string.
     * @param campaignId    campaign id.
     * @param gender gender identity.
     * @param age age
     * @param adUrlAPI  url custom ad-server.
     * @param usPrivacy US Privacy.
     */
    public AdmanRequest(Integer siteId, Integer playerId, Region region, Slot slot, Type type, Integer adsCount, Integer maxDuration, Integer preview, String consentString, Integer campaignId, Gender gender, Age age, String adUrlAPI, String usPrivacy) {
        this.siteId = siteId;
        this.playerId = playerId;
        this.region = region == null ? Region.DEFAULT : region;
        this.slot = slot == null ? Slot.DEFAULT : slot;
        this.type = type == null ? Type.DEFAULT : type;
        this.adsCount = adsCount;
        this.maxDuration = maxDuration;
        this.preview = preview;
        this.consentString = consentString;
        this.campaignId = campaignId;
        this.gender = gender;
        this.age = age;
        this.adUrlAPI = adUrlAPI;
        this.usPrivacy = usPrivacy;
    }

    public AdmanRequest(String adUrlAPI){
        this(null, null, null, null, null, null, null, null, null, null, null, null, adUrlAPI, null);
    }


    public String buildUrl(UserId userId) {
        return buildUrl(userId, null);
    }

    public String buildUrl(UserId userId, Map<String, String> params) {
        URLBuilder url;
        if (adUrlAPI != null && !adUrlAPI.isEmpty()) {
            url = new URLBuilder(adUrlAPI);
        } else {
            String baseUrl = region.adServer + "/" + apiVersion + "/vast/" + siteId;
            if (playerId != null) baseUrl += "/" + playerId;
            url = new URLBuilder(baseUrl);
            url.putQuery("device_id", userId.deviceId);
            url.putQuery("android_id", userId.androidId);
            url.putQuery("advertising_id", userId.advertisingId);
            url.putQuery("preview", preview);
            url.putQuery("slot", slot.id);
            url.putQuery("type", type.id);
            url.putQuery("ads_count", adsCount);
            url.putQuery("max_duration", maxDuration);
            if (consentString != null) url.putQuery("consent_string", consentString);
            if (usPrivacy != null) url.putQuery("us_privacy", usPrivacy);
            if (campaignId != null && campaignId != 0) url.putQuery("campaign_id", campaignId);
            if (gender != null && gender != Gender.NONE) url.putQuery("gender", gender.id);
            if (age != null && !age.value().isEmpty()) url.putQuery("age", age.value());

            if (params != null) {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    url.putQuery(entry.getKey(), entry.getValue());
                }
            }
        }
        return url.toString();
    }

    public static AdmanRequest siteId(Integer siteId) {
        return new Builder().setSiteId(siteId).build();
    }

    public static void update(AdmanRequest[] requests, Builder params, boolean allField){
        int count = requests.length;
        for (int i = 0; i < count; i++) {
            //item - preview value
            AdmanRequest item = requests[i];
            Builder newItem = new Builder();
            if(item.siteId != null){
                newItem.siteId = item.siteId;
            }
            if(item.playerId != null){
                newItem.playerId = item.playerId;
            }
            if(item.region != null){
                newItem.region = item.region;
            }
            if(item.slot != null){
                newItem.slot = item.slot;
            }
            if(item.type != null){
                newItem.type = item.type;
            }
            if(item.adsCount != null){
                newItem.adsCount = item.adsCount;
            }
            if(item.maxDuration != null){
                newItem.maxDuration = item.maxDuration;
            }
            if(item.preview != null){
                newItem.preview = item.preview;
            }
            if(item.consentString != null){
                newItem.consentString = item.consentString;
            }
            if(item.campaignId != null){
                newItem.campaignId = item.campaignId;
            }
            if(item.gender != null){
                newItem.gender = item.gender;
            }
            if(item.age != null){
                newItem.age = item.age;
            }
            if(item.adUrlAPI != null){
                newItem.adUrlAPI = item.adUrlAPI;
            }
            if(item.usPrivacy != null){
                newItem.usPrivacy = item.usPrivacy;
            }
            newItem.assign(params, allField);
            requests[i] = newItem.build();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(siteId == null ? 0 : siteId);
        dest.writeInt(playerId == null ? 0 : playerId);
        dest.writeBundle(region.toBundle());
        dest.writeString(slot.name());
        dest.writeString(type.name());
        dest.writeInt(adsCount == null ? 0 : adsCount);
        dest.writeInt(maxDuration == null ? 0: maxDuration);
        dest.writeInt(preview == null ? 0 : preview);
        dest.writeString(consentString);
        dest.writeInt(campaignId == null ? 0 : campaignId);
        dest.writeString(gender.name());
        dest.writeString(age.value());
        dest.writeString(adUrlAPI);
        dest.writeString(usPrivacy);
    }

    public static final Creator<AdmanRequest> CREATOR = new Creator<AdmanRequest>() {
        private Integer intOrNull(int value) {
            return value == 0 ? null : value;
        }

        @Override
        public AdmanRequest createFromParcel(Parcel in) {
            Builder builder = new Builder();
            builder.setSiteId(intOrNull(in.readInt()));
            builder.setPlayerId(intOrNull(in.readInt()));
            builder.setRegion(Region.valueOf(in.readBundle()));
            builder.setSlot(Slot.valueOf(in.readString()));
            builder.setType(Type.valueOf(in.readString()));
            builder.setAdsCount(intOrNull(in.readInt()));
            builder.setMaxDuration(intOrNull(in.readInt()));
            builder.setPreview(intOrNull(in.readInt()));
            builder.setConsentString(in.readString());
            builder.setCampaignId(intOrNull(in.readInt()));
            builder.setGender(Gender.valueOf(in.readString()));
            builder.setAge(Age.valueOf(in.readString()));
            builder.setAdUrlAPI(in.readString());
            builder.setUSPrivacy(in.readString());

            return builder.build();
        }

        @Override
        public AdmanRequest[] newArray(int size) {
            return new AdmanRequest[size];
        }
    };

    /**
     *
     */
    public static class Builder {

        private Integer siteId;
        private Integer playerId;
        private Region region;
        private Slot slot;
        private Type type;
        private Integer adsCount;
        private Integer maxDuration;
        private Integer preview;
        private String consentString;
        private Integer campaignId;
        private Gender gender;
        private Age age;
        private String adUrlAPI;
        private String usPrivacy;

        public Builder() {
        }

        public Builder setSiteId(Integer siteId) {
            this.siteId = siteId;
            return this;
        }

        public Builder setPlayerId(Integer playerId) {
            this.playerId = playerId;
            return this;
        }

        public Builder setRegion(Region region) {
            this.region = region;
            return this;
        }

        public Builder setSlot(Slot slot) {
            this.slot = slot;
            return this;
        }

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setAdsCount(Integer adsCount) {
            this.adsCount = adsCount;
            return this;
        }

        public Builder setMaxDuration(Integer maxDuration) {
            this.maxDuration = maxDuration;
            return this;
        }

        public Builder setPreview(Integer preview) {
            this.preview = preview;
            return this;
        }

        public Builder setConsentString(String consentString) {
            final int CONSENT_STRING_MAX = 768;
            int length = (consentString == null) ? 0 : consentString.length();
            this.consentString = (length > CONSENT_STRING_MAX) ? consentString.substring(0, CONSENT_STRING_MAX): consentString;
            return this;
        }
        public Builder setUSPrivacy(String value) {
            final int US_PRIVACY_MAX = 768;
            int length = (value == null) ? 0 : value.length();
            this.usPrivacy = (length > US_PRIVACY_MAX) ? value.substring(0, US_PRIVACY_MAX): value;
            return this;
        }


        public Builder setCampaignId(Integer campaignId) {
            this.campaignId = campaignId;
            return this;
        }

        public Builder setGender(Gender gender) {
            this.gender = gender;
            return this;
        }

        public Builder setAge(Age age) {
            this.age = age;
            return this;
        }

        public Builder setAdUrlAPI(String value) {
            this.adUrlAPI = value;
            return this;
        }

        public Builder assign(Builder val, boolean allField) {
            if(allField || val.siteId != null){
                this.siteId = val.siteId;
            }
            if(allField || val.playerId != null){
                this.playerId = val.playerId;
            }
            if(allField || val.region != null){
                this.region = val.region;
            }
            if(allField || val.slot != null){
                this.slot = val.slot;
            }
            if(allField || val.type != null){
                this.type = val.type;
            }
            if(allField || val.adsCount != null){
                this.adsCount = val.adsCount;
            }
            if(allField || val.maxDuration != null){
                this.maxDuration = val.maxDuration;
            }
            if(allField || val.preview != null){
                this.preview = val.preview;
            }
            if(allField || val.consentString != null){
                this.consentString = val.consentString;
            }
            //adUrlAPI == null - is valid value
            this.adUrlAPI = val.adUrlAPI;

            if(allField || val.usPrivacy != null){
                this.usPrivacy = val.usPrivacy;
            }
            return this;
        }

        public AdmanRequest build() {
            return new AdmanRequest(siteId, playerId, region, slot, type, adsCount, maxDuration, preview, consentString, campaignId, gender, age, adUrlAPI, usPrivacy);
        }
    }
}
