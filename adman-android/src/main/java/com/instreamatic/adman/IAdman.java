package com.instreamatic.adman;

import android.content.Context;
import android.os.Bundle;
import android.view.ViewGroup;

import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.module.IAdmanModule;
import com.instreamatic.core.android.ActionIntentStorage;
import com.instreamatic.vast.VASTBannerView;
import com.instreamatic.vast.VASTDispatcher;
import com.instreamatic.vast.VASTPlayer;
import com.instreamatic.vast.VASTSelector;
import com.instreamatic.vast.model.VASTInline;

import java.util.List;

public interface IAdman {
    public String getVersion();

    public Context getContext();

    public VASTDispatcher getVASTDispatcher();

    public VASTSelector getSelector();

    public List<VASTInline> getAds();

    public VASTInline getCurrentAd();

    public VASTPlayer getPlayer();

    public VASTBannerView getBanner();

    public boolean hasValidVAST();

    public AdmanRequest getRequest();

    public AdmanRequest updateRequest(AdmanRequest.Builder params, boolean allField);

    public UserId getUser();

    public EventDispatcher getDispatcher();

    public void addListener(AdmanEvent.Listener listener);

    public void removeListener(AdmanEvent.Listener listener);

    public void preload();

    public void start();

    public void sendCanShow();

    public void play();

    public void pause();

    public void reset();

    public void skip();

    public boolean isPlaying();

    public boolean isVoiceRecording();

    public ActionIntentStorage getStorageAction();
    /**
     * open ad link
     */
    public void open();

    public boolean canShowBanner();

    public void showBanner(ViewGroup container);

    public void bindModule(IAdmanModule module);

    public void unbindModule(IAdmanModule module);

    public <T extends IAdmanModule> T getModule(String id);

    public <T extends IAdmanModule> T getModuleAs(String id, Class<T> moduleType);

    public Bundle getMetadata();

    public void setParameters(Bundle parameters);

    public void startPositiveIntent(Context context);
}
