package com.instreamatic.adman;

final public class Age {
    final public int a;
    final public int b;

    public Age(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public Age(Integer value) {
        this(value, 0);
    }

    public Age() {
        this(0, 0);
    }

    public String value() {
        String value = "";
        if (a > 0 && b > 0) {
            value = a < b
                    ? a + "-" + b
                    : String.valueOf(a);
        } else if (a > 0) {
            value = String.valueOf(a);
        } else if (b > 0) {
            value = String.valueOf(b);
        }
        return value;
    }

    public static Age valueOf(String value) {
        int a = 0;
        int b = 0;
        try {
            String[] parts = value.split("-");
            if (parts.length > 1) {
                a = Integer.parseInt(parts[0]);
                b = Integer.parseInt(parts[1]);
            } else {
                a = Integer.parseInt(value);
            }
        } catch (NumberFormatException ignored) {
        }
        return new Age(a, b);
    }

}

