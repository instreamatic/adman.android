package com.instreamatic.adman;

public class UserId {

    public String advertisingId;
    public String deviceId;
    public String androidId;
    public boolean resolved;

    public UserId(String advertisingId, String deviceId, String androidId) {
        this.advertisingId = advertisingId;
        this.deviceId = deviceId;
        this.androidId = androidId;
    }

    public UserId() {
        this(null, null, null);
    }

    public boolean isEmpty() {
        return advertisingId == null &&
                deviceId == null &&
                androidId == null;
    }

    public String toString() {
        return "UserId {\n" +
                "\tadvertisingId=" + advertisingId + ",\n" +
                "\tdeviceId=" + deviceId + ",\n" +
                "\tandroidId=" + androidId + ",\n" +
                "}";
    }
}
