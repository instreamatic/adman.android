package com.instreamatic.adman;

public enum Gender {
    NONE("_"),
    MALE("M"),
    FEMALE("F");

    final public String id;

    Gender(String id) {
        this.id = id;
    }
}
