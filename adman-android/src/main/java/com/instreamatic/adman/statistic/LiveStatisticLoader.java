package com.instreamatic.adman.statistic;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.UserId;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.core.net.Loader;
import com.instreamatic.vast.model.VASTInline;
import okhttp3.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LiveStatisticLoader extends Loader<Void> {

    final private static String TAG = "live";

    final protected static ICallback<Void> defaultCallback = new ICallback<Void>() {
        @Override
        public void onSuccess(Void data) {
            Log.d(TAG, "ok");
        }

        @Override
        public void onFail(Throwable t) {
            Log.e(TAG, "fail: " + t.getMessage());
        }
    };

    protected Map<String, String> buildData(IAdman adman, AdmanRequest request) {
        Map<String, String> form = new HashMap<>();
        if (request.siteId != null) {
            form.put("site_id", request.siteId.toString());
        }
        if (request.playerId != null) {
            form.put("player_id", request.playerId.toString());
        }
        form.put("slot", request.slot.id);
        form.put("type", request.type.id);
        VASTInline ad = adman.getCurrentAd();
        if (ad == null) {
            List<VASTInline> ads = adman.getAds();
            if (ads != null && ads.size() > 0) {
                ad = ads.get(0);
            }
        }
        if (ad != null) {
            form.put("ad_id", ad.id);
        }
        UserId user = adman.getUser();
        if (user != null) {
            if (user.advertisingId != null) {
                form.put("advertising_id", user.advertisingId);
            }
            if (user.androidId != null) {
                form.put("android_id", user.androidId);
            }
            if (user.deviceId != null) {
                form.put("device_id", user.deviceId);
            }
        }
        form.put("platform", "android");
        form.put("platform.version", Build.VERSION.RELEASE);
        form.put("sdk.version", adman.getVersion());
        // ToDo:
        if (adman.getContext().checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            form.put("microphone", "1");
        }
        if (adman.getContext().checkCallingOrSelfPermission(Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
            form.put("calendar", "1");
        }
        return form;
    }

    public void send(IAdman adman, String action) {
        AdmanRequest request = adman.getRequest();
        send(adman, request, action);
    }

    public void send(IAdman adman, AdmanRequest request, String action) {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : buildData(adman, request).entrySet()) {
            builder.append(entry.getKey());
            builder.append("=");
            builder.append(entry.getValue());
            builder.append("&");
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
        }
        String query = builder.toString();
        String value = Base64.encodeToString(query.getBytes(), 0);
        try {
            String encodedValue = URLEncoder.encode(value, "utf-8");
            Region region = request.region;
            Log.d(TAG, "SendAction: " + action + " : "+ region.name());
            GET(region.statServer + "/live/" + action + "?v=" + encodedValue, defaultCallback);
        } catch (UnsupportedEncodingException e) {
            defaultCallback.onFail(e);
        }
    }

    @Override
    protected void onResponse(Response response, ICallback<Void> callback) throws Exception {
        Log.d(TAG, "onResponse code: " + response.code());
        if (response != null){
            response.close();
        }
    }
}