package com.instreamatic.adman.statistic;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;


public class LiveStatistic extends BaseAdmanModule implements RequestEvent.Listener {

    final public static String ID = "statistic";

    private LiveStatisticLoader loader;

    public LiveStatistic() {
        loader = new LiveStatisticLoader();
    }

    @Override
    public String getId() {
        return ID;
    }

    public void onLoad() {
        new LiveStatisticLoader().send(getAdman(), "load");
    }

    public void sendAction(String action) {
        loader.send(getAdman(), action);
    }

    public void sendAction(AdmanRequest request, String action) {
        loader.send(getAdman(), request, action);
    }

    @Override
    public void onRequestEvent(RequestEvent event) {
        String action = null;
        switch (event.getType()) {
            case LOAD:
                action = "request";
                break;
            case FAILED:
                action = "request_error";
                break;
            case SUCCESS:
                action = "fetched";
                break;
        }
        if (action != null) {
            sendAction(action);
        }
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {RequestEvent.TYPE};
    }
}
