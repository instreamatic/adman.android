package com.instreamatic.adman;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.util.Log;

import java.util.List;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ActionUtil {

    final private static String TAG = "Adman."+ActionUtil.class.getSimpleName();

    public static Intent getIntentOpenUrl(String url) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(url));
        return intent;
    }

    public static void openUrl(Context context, String url) {
        startActivityWithFirst(context, getIntentOpenUrl(url));
    }

    public static Intent getIntentByPhone(String value) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + value));
        return intent;
    }

    public static void callByPhone(Context context, String value) {
        startActivityWithFirst(context, getIntentByPhone(value));
    }

    public static Intent getIntentBySMS(String[] data) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("sms:" + data[0]));
        if (data.length > 1) {
            intent.putExtra("sms_body", data[1]);
        }
        return intent;
    }

    public static void sendSMS(Context context, String[] data) {
        Intent intent = getIntentBySMS(data);
        if (!(context instanceof Activity)) {
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    /**
    * Selected first item from list of actual activities
    **/
    public static void startActivityWithFirst(Context context, Intent intent) {
        List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY | PackageManager.GET_RESOLVED_FILTER);
        //TODO: it is temporarily code
        Log.d(TAG, "resolveInfos: ");
        int i = 0;
        for (ResolveInfo item : resolveInfos) {
            boolean isDef = item.filter.hasCategory(Intent.CATEGORY_DEFAULT);
            Log.d(TAG, (i++) + ": isDef: " + isDef + " : " +item.priority + ": " + item.activityInfo);
        }
        //------------------
        if (resolveInfos.size() > 0) {
            ActivityInfo activityInfo = resolveInfos.get(0).activityInfo;
            ComponentName componentName = new ComponentName(
                    activityInfo.packageName,
                    activityInfo.name
            );
            Log.d(TAG, "componentName: " + componentName);
            intent.setComponent(componentName);
        }
        if (!(context instanceof Activity)) {
            intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
        }
        try {
            context.startActivity(intent);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "openAd", e);
        }
    }

}
