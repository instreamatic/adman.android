package com.instreamatic.adman.variable;

import android.content.Context;
import android.media.AudioDeviceInfo;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.List;

public class SystemServiceUtil {
    public enum AudioType {
        NONE(""),
        SPEAKER_PHONE("speakerphone"),
        BLUETOOTH_A2DP("bluetootha2dp"),
        BLUETOOTH_SCO("bluetoothsco"),
        WIRED_HEADSET("wiredheadset"),
        WIRED_HEADPHONES("wiredheadphones");

        final public String type;

        AudioType(String type) {
            this.type = type;
        }
    }

    public static AudioType getAudioType(Context context) {
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (am == null)
            return AudioType.NONE;
        AudioType audioType = AudioType.NONE;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1 && am.isWiredHeadsetOn()) {
            audioType = AudioType.WIRED_HEADSET;
        }
        if (audioType == AudioType.NONE && Build.VERSION.SDK_INT < Build.VERSION_CODES.M && am.isBluetoothA2dpOn()) {
            audioType = AudioType.BLUETOOTH_A2DP;
        }
        if (audioType == AudioType.NONE && am.isBluetoothScoOn()) {
            audioType = AudioType.BLUETOOTH_SCO;
        }
        if (audioType == AudioType.NONE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AudioDeviceInfo[] devices = am.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
            List<AudioType> types = new ArrayList<>();
            for (int i = 0; i < devices.length; i++) {
                AudioDeviceInfo device = devices[i];
                if (device.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET) {
                    types.add(AudioType.WIRED_HEADSET);
                    continue;
                }
                if (device.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES) {
                    types.add(AudioType.WIRED_HEADPHONES);
                    continue;
                }
                if (device.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_A2DP) {
                    types.add(0, AudioType.BLUETOOTH_A2DP);
                    continue;
                }
                if (device.getType() == AudioDeviceInfo.TYPE_BLUETOOTH_SCO) {
                    types.add(0, AudioType.BLUETOOTH_SCO);
                    continue;
                }
            }
            if (types.size() > 0) audioType = types.get(0);
        }
        if (audioType == AudioType.NONE && am.isSpeakerphoneOn()) {
            audioType = AudioType.SPEAKER_PHONE;
        }
        return audioType;
    }

    public enum NetworkType {
        NONE(""),
        WIFI("wifi"),
        II_G("2g"),
        III_G("3g"),
        IV_G("4g"),
        V_G("5g"),
        UNKNOWN("unk");

        final public String type;

        NetworkType(String type) {
            this.type = type;
        }
    }

    public static NetworkType getNetworkType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return NetworkType.NONE;

        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return NetworkType.NONE; // not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return NetworkType.WIFI;
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:     // api< 8: replace by 11
                case TelephonyManager.NETWORK_TYPE_GSM:      // api<25: replace by 16
                    return NetworkType.II_G;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:   // api< 9: replace by 12
                case TelephonyManager.NETWORK_TYPE_EHRPD:    // api<11: replace by 14
                case TelephonyManager.NETWORK_TYPE_HSPAP:    // api<13: replace by 15
                case TelephonyManager.NETWORK_TYPE_TD_SCDMA: // api<25: replace by 17
                    return NetworkType.III_G;
                case TelephonyManager.NETWORK_TYPE_LTE:      // api<11: replace by 13
                case TelephonyManager.NETWORK_TYPE_IWLAN:    // api<25: replace by 18
                case 19: // LTE_CA
                    return NetworkType.IV_G;
//                case TelephonyManager.NETWORK_TYPE_NR:       // api<29: replace by 20
//                    return NetworkType.V_G;
                default:
                    return NetworkType.UNKNOWN;
            }
        }
        return NetworkType.UNKNOWN;
    }
}