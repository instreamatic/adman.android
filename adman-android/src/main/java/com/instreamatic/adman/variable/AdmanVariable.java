package com.instreamatic.adman.variable;

import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;

import java.util.HashMap;
import java.util.Map;


public class AdmanVariable extends BaseAdmanModule implements RequestEvent.Listener {

    final public static String ID = "variable";

    final public Map<String, String> values;

    public AdmanVariable(Map<String, String> values) {
        super();
        this.values = values;
    }

    public AdmanVariable() {
        this(new HashMap<String, String>());
    }

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public void onRequestEvent(RequestEvent event) {
        switch (event.getType()) {
            case LOAD:
                event.params.putAll(values);
                break;
        }
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[] {RequestEvent.TYPE};
    }
}
