package com.instreamatic.adman.source;

import android.os.Bundle;
import android.util.Log;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.EventType;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.module.BaseAdmanModule;
import com.instreamatic.adman.variable.SystemServiceUtil;
import com.instreamatic.core.net.ICallback;
import com.instreamatic.vast.VASTLoader;
import com.instreamatic.vast.model.VAST;
import com.instreamatic.vast.model.VASTAd;
import com.instreamatic.vast.model.VASTInline;

import java.util.*;

public class AdmanSource extends BaseAdmanModule {
    final private static String TAG = "Adman."+AdmanSource.class.getSimpleName();

    final public static String ID = "source";

    private boolean loading;

    private RequestVerification requestVerification = new RequestVerification();

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public EventType[] eventTypes() {
        return new EventType[0];
    }

    public void load(final AdmanRequest[] requests) {
        // ToDo: exception?
        boolean isAllowed = requestVerification.isAllowed();
        if (loading || !isAllowed) {
            Log.d(TAG, String.format("loading: %b, isAllowed: %b, verification: %s", loading, isAllowed, requestVerification.toString()));
            if (!isAllowed) {
                getAdman().getDispatcher().dispatch(new RequestEvent((requests.length > 0 ? requests[0] : null), RequestEvent.Type.REQUEST_VERIFICATION_FAILED));
            }
            return;
        }
        loading = true;
        new AdmanLoader(getAdman()).GET(requests, new ICallback<AdmanLoaderResponse>() {
            @Override
            public void onSuccess(AdmanLoaderResponse data) {
                loading = false;
                RequestVerification.updateValues(requestVerification, false, data.ads);

                getAdman().getDispatcher().dispatch(new RequestEvent(data.request, RequestEvent.Type.SUCCESS, data.ads));
            }

            @Override
            public void onFail(Throwable t) {
                loading = false;
                RequestVerification.updateValues(requestVerification, true,null);

                if (t instanceof AdmanLoaderEmptyException) {
                    getAdman().getDispatcher().dispatch(new RequestEvent(((AdmanLoaderEmptyException) t).request, RequestEvent.Type.NONE));
                } else if (t instanceof AdmanLoaderException) {
                    getAdman().getDispatcher().dispatch(new RequestEvent(((AdmanLoaderException) t).request, RequestEvent.Type.FAILED));
                } else {
                    throw new RuntimeException("Unsupported exception", t);
                }
            }
        });
    }

    public void setParameters(Bundle parameters) {
        if (parameters == null) return;
        requestVerification.setTimeoutRequestFail(parameters.getLong("adman.timeout_fail_request_sec", RequestVerification.DEFAULT_TIMEOUT_REQUEST_FAIL_SEC));
        requestVerification.setCountMaxFail(parameters.getInt("adman.count_max_fail_request", RequestVerification.DEFAULT_MAX_FAIL));
        requestVerification.setTimeExpirationVAST(parameters.getLong("adman.time_expiration_vast_sec", RequestVerification.DEFAULT_TIME_EXPIRATION_VAST_SEC));
    }

    public boolean hasValidVAST() {
        boolean isValid = requestVerification.hasValidVAST();
        if (!isValid) {
            Log.d(TAG, requestVerification.toString());
        }
        return isValid;
    }

    private static class AdmanLoaderResponse {
        final public AdmanRequest request;
        final public List<VASTInline> ads;

        public AdmanLoaderResponse(AdmanRequest request, List<VASTInline> ads) {
            this.request = request;
            this.ads = ads;
        }
    }

    private static class AdmanLoaderException extends Exception {
        final public AdmanRequest request;
        final public Throwable original;

        public AdmanLoaderException(AdmanRequest request, Throwable original) {
            this.request = request;
            this.original = original;
        }

        public AdmanLoaderException(AdmanRequest request) {
            this(request, null);
        }
    }

    private static class AdmanLoaderEmptyException extends AdmanLoaderException {

        public AdmanLoaderEmptyException(AdmanRequest request) {
            super(request);
        }
    }

    private static class AdmanLoader extends VASTLoader {

        final private IAdman adman;

        public AdmanLoader(IAdman adman) {
            this.adman = adman;
        }

        private String buildUrl(AdmanRequest request) {
            Map<String, String> params = new HashMap<>();
            params.put("version", adman.getVersion());
            params.put("lang", Locale.getDefault().toString().replace("_", "-"));
            SystemServiceUtil.AudioType audioType = SystemServiceUtil.getAudioType(adman.getContext());
            if (audioType != SystemServiceUtil.AudioType.NONE) {
                params.put("audio_output", audioType.type);
            }
            SystemServiceUtil.NetworkType networkType = SystemServiceUtil.getNetworkType(adman.getContext());
            if (networkType != SystemServiceUtil.NetworkType.NONE) {
                params.put("network_type", networkType.type);
            }
            adman.getDispatcher().dispatch(new RequestEvent(request, RequestEvent.Type.LOAD, params));  //ToDo:
            return request.buildUrl(adman.getUser(), params);
        }

        private Map<String, String> buildHeaders() {
            Map<String, String> headers = new HashMap<>();
            headers.put("Referer", adman.getContext().getPackageName());
            return headers;
        }

        public void GET(AdmanRequest[] requests, ICallback<AdmanLoaderResponse> callback) {
            if (requests.length == 0) {
                callback.onFail(new ArrayIndexOutOfBoundsException());
            } else {
                next(0, requests, callback);
            }
        }

        private void next(final int index, final AdmanRequest[] requests, final ICallback<AdmanLoaderResponse> callback) {
            final AdmanRequest request = requests[index];
            String url = buildUrl(request);
            Log.d(TAG, "request: " + url);
            GET(url, buildHeaders(), new ICallback<VAST>() {
                @Override
                public void onSuccess(VAST data) {
                    Log.d(TAG, "response: " + data);
                    List<VASTInline> ads = new ArrayList<>();
                    for (VASTAd ad : data.ads) {
                        if (ad.type.equals(VASTInline.TYPE)) {
                            ads.add((VASTInline) ad);
                        }
                    }
                    if (ads.size() == 0) {
                        onFail(new AdmanLoaderEmptyException(request));
                    } else {
                        callback.onSuccess(new AdmanLoaderResponse(request, ads));
                    }
                }

                @Override
                public void onFail(Throwable t) {
                    if (index < requests.length - 1) {
                        next(index + 1, requests, callback);
                    } else {
                        callback.onFail(t instanceof AdmanLoaderException ? t : new AdmanLoaderException(request, t));
                    }
                 }
            });
        }
    }
}