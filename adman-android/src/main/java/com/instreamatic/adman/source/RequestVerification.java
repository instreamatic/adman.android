package com.instreamatic.adman.source;

import com.instreamatic.vast.model.VASTInline;

import java.util.Date;
import java.util.List;

public class RequestVerification {
    final public static int DEFAULT_MAX_FAIL = 2;
    final public static long DEFAULT_TIMEOUT_REQUEST_FAIL_SEC = 60;
    final public static long DEFAULT_TIME_EXPIRATION_VAST_SEC = 5*60;

    private int countMaxFail = DEFAULT_MAX_FAIL;
    private long timeoutRequestFailSec = DEFAULT_TIMEOUT_REQUEST_FAIL_SEC;
    //The number of seconds in which the ad is valid for execution
    private long timeExpirationVastSec = DEFAULT_TIME_EXPIRATION_VAST_SEC;

    private int countFail = 0;
    private Date timeRequestFail;
    private Date timeReceiveVAST;

    public boolean isAllowed() {
        if (countFail < countMaxFail || timeRequestFail == null) {
            return true;
        }

        long intervalFail = getIntervalRequestFail(new Date());
        return timeoutRequestFailSec <= 0 || intervalFail > (timeoutRequestFailSec*1000);
    }

    public boolean hasValidVAST() {
        long intervalReceiveVAST = getIntervalReceiveVAST(new Date());
        return timeReceiveVAST != null && intervalReceiveVAST < (timeExpirationVastSec*1000);
    }

    public void incFail(Date date) {
        countFail++;
        //reset countFail by interval
        long intervalFail = getIntervalRequestFail(date);
        if (intervalFail > (timeoutRequestFailSec*1000)) {
            countFail = 1;
        }
        timeRequestFail = date;
    }

    public void reset() {
        timeRequestFail = null;
        countFail = 0;
    }

    public void setTimeReceiveVAST(Date date) {
        timeReceiveVAST = date;
    }

    public void setTimeoutRequestFail(long value) {
        timeoutRequestFailSec = value;
    }

    public void setCountMaxFail(int value) {
        countMaxFail = value;
    }

    public void setTimeExpirationVAST(long value) {
        timeExpirationVastSec = value;
    }

    public void setTimeExpirationVAST(List<VASTInline> ads) {
        final long MAX_EXPIRES = 1000000;
        long value = MAX_EXPIRES;
        for (VASTInline ad : ads) {
            String expiresStr = ad.extensions != null && ad.extensions.containsKey("Expires") ? ad.extensions.get("Expires").value : "0";
            long expires = Long.parseLong(expiresStr.trim());
            if (expires > 0 && expires < value) {
                value = expires;
            }
        }
        setTimeExpirationVAST(value == MAX_EXPIRES ? DEFAULT_TIME_EXPIRATION_VAST_SEC : value);
    }

    public String toString() {
        String strCountFail = "fail: " + countFail + " - " + countMaxFail;
        String strIntervalFail = "intervalFail: " + getIntervalRequestFail(new Date()) + " - " + (timeoutRequestFailSec * 1000);
        String strReceiveVAST = "receiveVAST: " + getIntervalReceiveVAST(new Date()) + " - " + (timeExpirationVastSec * 1000);
        return String.format("%s; %s; %s", strCountFail, strIntervalFail, strReceiveVAST);
    }

    private long getIntervalRequestFail(Date value) {
        return timeRequestFail == null
                ? 0
                : value.getTime() - timeRequestFail.getTime();
    }


    private long getIntervalReceiveVAST(Date value) {
        return timeReceiveVAST == null
                ? 0
                : value.getTime() - timeReceiveVAST.getTime();
    }

    static void updateValues(RequestVerification verificator, boolean isFail, List<VASTInline> ads) {
        if (isFail) {
            verificator.incFail(new Date());
            verificator.setTimeReceiveVAST(null);
        } else {
            verificator.reset();
            verificator.setTimeReceiveVAST(new Date());
            verificator.setTimeExpirationVAST(ads);
        }
    }
}
