package com.instreamatic.adman;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import com.instreamatic.adman.event.*;
import com.instreamatic.adman.module.IAdmanModule;
import com.instreamatic.adman.source.AdmanSource;
import com.instreamatic.adman.statistic.LiveStatistic;
import com.instreamatic.core.android.ActionIntentStorage;
import com.instreamatic.core.net.Loader;
import com.instreamatic.player.AudioPlayer;
import com.instreamatic.vast.VASTBannerView;
import com.instreamatic.vast.VASTDispatcher;
import com.instreamatic.vast.VASTPlayer;
import com.instreamatic.vast.VASTSelector;
import com.instreamatic.vast.model.VASTCompanion;
import com.instreamatic.vast.model.VASTEvent;
import com.instreamatic.vast.model.VASTInline;
import com.instreamatic.vast.model.VASTMedia;
import com.instreamatic.vast.model.VASTSkipOffset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Adman implements IAdman,
        VASTPlayer.CompleteListener, VASTPlayer.StateListener, VASTPlayer.ProgressListener,
        RequestEvent.Listener, PlayerEvent.Listener, ControlEvent.Listener, ModuleEvent.Listener,
        AudioManager.OnAudioFocusChangeListener  {

    static {
        Loader.setUserAgent("Adman SDK " + BuildConfig.VERSION_NAME + "; " + Loader.getUserAgent());
    }

    final private static String TAG = Adman.class.getSimpleName();

    final private static int EVENT_PRIORITY = 10;

    protected List<VASTInline> ads;
    protected VASTInline ad;
    protected VASTPlayer player;
    protected VASTBannerView banner;
    protected VASTDispatcher vastDispatcher;
    protected VASTSelector selector;
    protected Context context;
    protected AdmanRequest[] requests;
    protected AdmanRequest request;
    protected UserId userId;

    protected EventDispatcher dispatcher;
    protected boolean playing;
    protected float volume;
    protected boolean startPlaying;
    protected Map<String, IAdmanModule> modules;
    protected boolean sentSkip;
    protected boolean skipped;
    protected boolean voiceRecording = false;

    private boolean needAudioFocus = true;
    private boolean needAlmostComplete = true;

    private ActionIntentStorage storageAction = null;
    /**
     * AdMan constructor
     *
     * @param context Android application context.
     * @param requests request.
     */
    public Adman(Context context, AdmanRequest[] requests) {
        Log.i(TAG, "version: " + getVersion());
        this.context = context;
        this.requests = requests;
        dispatcher = new EventDispatcher();
        dispatcher.addListener(ControlEvent.TYPE, this, EVENT_PRIORITY);
        dispatcher.addListener(RequestEvent.TYPE, this, EVENT_PRIORITY);
        dispatcher.addListener(PlayerEvent.TYPE, this, EVENT_PRIORITY);
        dispatcher.addListener(ModuleEvent.TYPE, this, EVENT_PRIORITY);
        selector = new VASTSelector();
        playing = false;
        startPlaying = false;
        volume = 1.0f;
        modules = new HashMap<>();
        sentSkip = false;
        skipped = false;
        bindModule(new LiveStatistic());
        bindModule(new AdmanSource());
        UserIdResolver.resolve(context, new UserIdResolver.Callback() {
            @Override
            public void onResolved(UserId data) {
                userId = data;
                Log.d(TAG, userId.toString());
                getModuleAs(LiveStatistic.ID, LiveStatistic.class).onLoad();
            }
        });

        this.storageAction = new ActionIntentStorage(context.getApplicationContext());
    }

    public Adman(Context context, AdmanRequest request) {
        this(context, new AdmanRequest[]{request});
    }

    @Override
    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }

    @Override
    public Context getContext() {
        return context;
    }

    @Override
    public VASTDispatcher getVASTDispatcher() {
        return vastDispatcher;
    }

    @Override
    public VASTSelector getSelector() {
        return selector;
    }

    @Override
    public List<VASTInline> getAds() {
        return ads;
    }

    @Override
    public VASTInline getCurrentAd() {
        return ad;
    }

    @Override
    public VASTPlayer getPlayer() {
        return player;
    }

    @Override
    public VASTBannerView getBanner() {
        return banner;
    }

    @Override
    public boolean hasValidVAST() {
        if (player == null) {
            Log.i(TAG, "VAST is absent");
            return false;
        }
        return getModuleAs(AdmanSource.ID, AdmanSource.class).hasValidVAST();
    }

    @Override
    public AdmanRequest getRequest() {
        // use first request as default
        return request == null
                ? (requests.length > 0) ? requests[0] : null
                : request;
    }

    @Override
    public AdmanRequest updateRequest(AdmanRequest.Builder params, boolean allField){
        if(request != null){
            AdmanRequest[] tmpRequest = {request};
            AdmanRequest.update( tmpRequest, params, allField);
        }
        AdmanRequest.update(requests, params, allField);
        return getRequest();
    }

    @Override
    public UserId getUser() {
        return userId;
    }

    @Override
    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    @Override
    public void addListener(AdmanEvent.Listener listener) {
        dispatcher.addListener(AdmanEvent.TYPE, listener);
    }

    @Override
    public void removeListener(AdmanEvent.Listener listener) {
        dispatcher.removeListener(AdmanEvent.TYPE, listener);
    }

    @Override
    public void preload() {
        load(false);
    }

    public void start() {
        load(true);
    }

    private void load(boolean startPlaying) {
        if (isPlaying()){
            Log.w(TAG, "Ad is already playing! Please complete the playback then request the new ad.");
            return;
        }
        this.startPlaying = startPlaying;
        UserIdResolver.resolve(context, new UserIdResolver.Callback() {
            @Override
            public void onResolved(UserId data) {
                getModuleAs(AdmanSource.ID, AdmanSource.class).load(requests);
            }
        });
    }

    private boolean isCompatibleAdAndMic(final VASTInline ad){
        return ad.isVoice()
                ? context.checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED
                : true;
    }

    protected void startAd(final VASTInline ad) {
        if (!isCompatibleAdAndMic(ad)){
            Log.w(TAG, "Current ad is voice. Mic not available");
            dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.FAILED));
            return;
        }
        this.ad = ad;
        vastDispatcher = new VASTDispatcher(ad);

        boolean isBannerClickable = (ad.extensions != null  && ad.extensions.containsKey("isClickable")) ? (1 == Integer.parseInt(ad.extensions.get("isClickable").value)) : true;

        VASTCompanion companion = selector.selectCompanion(ad.companions);
        banner = new VASTBannerView(context, companion, dispatcher, isBannerClickable);
        banner.prepare(new Runnable() {
            @Override
            public void run() {
                startPlayer(ad);
            }
        });
    }

    protected void startPlayer(VASTInline ad) {
        VASTMedia media = selector.selectMedia(ad.medias);
        if (media == null) {
            Log.e(TAG, "Unsupported ad medias: " + ad.medias);
            dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.FAILED));
        } else {
            player = new VASTPlayer(context, media, vastDispatcher, startPlaying);
            player.setName("VASTPlayer");
            player.setCompleteListener(this);
            player.setStateListener(this);
            player.setProgressListener(this);
        }
    }

    public void sendCanShow() {
        UserIdResolver.resolve(context, new UserIdResolver.Callback() {
            @Override
            public void onResolved(UserId data) {
                LiveStatistic statModule = getModuleAs(LiveStatistic.ID, LiveStatistic.class);
                for (AdmanRequest request : requests) {
                    statModule.sendAction(request, "can_show");
                }
            }
        });
    }

    public void play() {
        if (hasValidVAST()) {
            dispatcher.dispatch(new ControlEvent(ControlEvent.Type.RESUME));
        } else {
            Log.i(TAG, "VAST expiration");
            start();
        }
    }

    public void pause() {
        dispatcher.dispatch(new ControlEvent(ControlEvent.Type.PAUSE));
    }

    public void skip() {
        dispatcher.dispatch(new ControlEvent(ControlEvent.Type.SKIP));
    }

    public void open() {
        if (vastDispatcher != null) {
            vastDispatcher.dispatch(VASTEvent.click);
        }
        String link = getOpenLink();
        if (link != null) {
            ActionUtil.openUrl(context, link);
        }
    }

    private String getOpenLink() {
        if (ad == null || selector == null){
            return null;
        }
        String link = ad.videoClicks.clickThrough;
        if (link == null) {
            VASTCompanion companion = selector.selectCompanion(ad.companions);
            if (companion != null) link = companion.clickThrough;
        }
        return link;
    }

    public boolean isPlaying() {
        return this.playing;
    }

    public boolean isVoiceRecording() {
        return this.voiceRecording;
    }

    public ActionIntentStorage getStorageAction() {
        return this.storageAction;
    };

    public boolean canShowBanner() {
        return (banner != null && !banner.isEmpty());
    }

    public void showBanner(ViewGroup container) {
        if (banner != null) {
            ViewGroup parent = (ViewGroup) banner.getParent();
            if (parent != null) {
                parent.removeView(banner);
            }
            container.addView(banner);
        }
    }

    @Override
    public void bindModule(IAdmanModule module) {
        if (modules.containsKey(module.getId())) {
            modules.get(module.getId()).unbind();
        }
        modules.put(module.getId(), module);
        module.bind(this);
    }

    @Override
    public void unbindModule(IAdmanModule module) {
        if (modules.containsKey(module.getId())) {
            modules.get(module.getId()).unbind();
            modules.remove(module.getId());
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T extends IAdmanModule> T getModule(String id) {
        return (T) modules.get(id);
    }

    @Override
    public <T extends IAdmanModule> T getModuleAs(String id, Class<T> moduleType) {
        return getModule(id);
    }

    @Override
    public Bundle getMetadata() {
        Bundle extra = new Bundle();
        //METADATA_KEY_MEDIA_ID
        extra.putString("android.media.metadata.MEDIA_ID", "Instreamatic");
        //METADATA_KEY_TITLE
        if (ad != null && ad.extensions.containsKey("linkTxt")) {
            extra.putString("android.media.metadata.TITLE", ad.extensions.get("linkTxt").value);
        }
        //METADATA_KEY_ALBUM_ART_URI & METADATA_KEY_ALBUM_ART
        if ((ad != null) && canShowBanner()) {
            VASTCompanion companion = selector.selectCompanion(ad.companions);
            if (companion != null) extra.putString("android.media.metadata.ALBUM_ART_URI", companion.url);

            final Bitmap bitmap = banner.getBitmap();
            if(banner != null) extra.putParcelable("android.media.metadata.ALBUM_ART", bitmap);

            if(ad.id != null) extra.putString("adman.vast.ID", ad.id);
            if(ad.type != null) extra.putString("adman.vast.TYPE", ad.type);
            if(ad.adSystem != null) extra.putString("adman.vast.AD_SYSTEM", ad.adSystem);
        }
        String link = getOpenLink();
        if (link != null) {
            extra.putString("adman.vast.CLICK_THROUGH", link);
        }
        return extra;
    }

    @Override
    public void setParameters(Bundle parameters) {
        if (parameters == null) return;
        boolean autoStart = parameters.getBoolean("adman.auto_start_positive_intent", true);
        this.storageAction.setAutoStart(autoStart);
        getModuleAs(AdmanSource.ID, AdmanSource.class).setParameters(parameters);
        needAudioFocus = parameters.getBoolean("adman.need_audio_focus",  needAudioFocus);
    }

    @Override
    public void startPositiveIntent(Context context) {
        this.storageAction.start(context);
    }

    public void reset() {
        if (player != null) {
            player.dispose();
            player = null;
        }
        if (banner != null) {
            banner.dispose();
            banner = null;
        }
        ad = null;
        vastDispatcher = null;
        skipped = false;
        playing = false;
    }

    public float getVolume() {
        return volume;
    }

    public void setVolume(float volume) {
        this.volume = volume;
        if (player != null) {
            player.setVolume(volume);
        }
    }

    @Override
    public void onComplete() {
        //dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
    }

    @Override
    public void onStateChange(AudioPlayer.State state) {
        switch (state) {
            case PREPARE:
                dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.PREPARE));
                break;
            case READY:
                dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.READY));
                break;
            case PLAYING:
                this.needAlmostComplete = true;
                this.sentSkip = false;
                if (playing) {
                    dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.PLAY));
                } else {
                    playing = true;
                    dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_START, TAG));
                    dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.PLAYING));
                }
                break;
            case PAUSED:
                dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_PAUSE, TAG));
                dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.PAUSE));
                break;
            case ERROR:
                dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.FAILED));
                break;
            case STOPPED:
                //onComplete();
                dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.COMPLETE));
                break;
        }
    }

    @Override
    public void onProgressChange(int position, int duration) {
        dispatcher.dispatch(new PlayerEvent(PlayerEvent.Type.PROGRESS));
        if (ad == null) {
            Log.i(TAG, "Event onProgressChange. Ad is null!");
            return;
        }
        VASTSkipOffset offset = ad.skipoffset;
        PlayerEvent.Type type = PlayerEvent.Type.SKIPPABLE;
        if (offset == null) {
            offset = new VASTSkipOffset(5000);
            type = PlayerEvent.Type.CLOSEABLE;
        }
        if (!this.sentSkip && VASTSkipOffset.eventHappened(offset, position, duration)) {
            dispatcher.dispatch(new PlayerEvent(type));
            this.sentSkip = true;
        }

        int currRate = duration > 0 ? (position * 100) / duration : 0;
        if (currRate >= 75 && this.needAlmostComplete) {
            this.needAlmostComplete = false;
            dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.ALMOST_COMPLETE));
        }
    }

    @Override
    public void onPlayerEvent(PlayerEvent event) {
        switch (event.getType()) {
            case READY:
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.READY));
                break;
            case PLAYING:
                requestAudioFocus();
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.STARTED));
                break;
            case FAILED:
                reset();
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.FAILED));
                break;
            case COMPLETE:
                boolean skipped = this.skipped;
                reset();
                if (ads.size() > 0) {
                    startAd(ads.remove(0));
                } else {
                    abandonAudioFocus();
                    if (skipped) {
                        dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.SKIPPED));
                    } else {
                        dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.COMPLETED));
                    }
                }
                break;
        }
    }

    @Override
    public void onRequestEvent(RequestEvent event) {
        request = event.request;
        switch (event.getType()) {
            case LOAD:
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.PREPARE));
                break;
            case NONE:
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.NONE));
                break;
            case SUCCESS:
                ads = new ArrayList<>(event.ads);
                startAd(ads.remove(0));
                //dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.READY));
                break;
            case FAILED:
                dispatcher.dispatch(new AdmanEvent(AdmanEvent.Type.FAILED));
                break;
        }
    }

    @Override
    public void onControlEvent(ControlEvent event) {
        switch (event.getType()) {
            case PAUSE:
                dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_PAUSE, TAG));
                abandonAudioFocus();
                break;
            case RESUME:
                if (player != null) {
                    requestAudioFocus();
                }
                dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_RESUME, TAG));
                break;
            case SKIP:
                skipped = true;
                if (playing) {
                    if (vastDispatcher != null) {
                        vastDispatcher.dispatch(VASTEvent.skip);
                    }
                    player.stop();
                }
                break;
            case CLICK:
                this.open();
                break;
        }
    }

    @Override
    public void onModuleEvent(ModuleEvent event) {
        Log.d(TAG, "ModuleEvent: " +  event);
        switch (event.getType()) {
            case ADMAN_PAUSE:
                if (player != null) {
                    player.pause();
                }
                break;
            case ADMAN_RESUME:
                if (player != null) {
                    player.resume();
                }
                break;
            case RECORD_START:
                voiceRecording = true;
                break;
            case RECORD_STOP:
                voiceRecording = false;
                break;
        }
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        String event = "";
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                event = "AUDIOFOCUS_LOSS";
                skip();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                event = "AUDIOFOCUS_LOSS_TRANSIENT";
                dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_PAUSE, TAG));
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                event = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
                setVolume(0.5f);
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                event = "AUDIOFOCUS_GAIN";
                dispatcher.dispatch(new ModuleEvent(ModuleEvent.Type.ADMAN_RESUME, TAG));
                setVolume(1.0f);
                break;
        }
        Log.d(TAG, "onAudioFocusChange: " + event + " (" + focusChange + ")");
    }

    private void requestAudioFocus() {
        if (!needAudioFocus) return;
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "requestAudioFocus");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                AudioAttributes mAudioAttributes =
                        new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_MEDIA)
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build();
                AudioFocusRequest mAudioFocusRequest =
                        new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                                .setAudioAttributes(mAudioAttributes)
                                .setAcceptsDelayedFocusGain(true)
                                .setOnAudioFocusChangeListener(this)
                                .build();
                audioManager.requestAudioFocus(mAudioFocusRequest);
            } else {
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            }
        }
    }

    private void abandonAudioFocus() {
        if (!needAudioFocus) return;
        AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "abandonAudioFocus");
            audioManager.abandonAudioFocus(this);
        }
    }
}
