package com.instreamatic.adman;

public enum Slot {
    ANY("instreamatic"),
    PREROLL("preroll"),
    MIDROLL("midroll"),
    POSTROLL("postroll");

    final public String id;

    Slot(String id) {
        this.id = id;
    }

    final public static Slot DEFAULT = ANY;
}
