package com.instreamatic.adman.module;

import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.EventType;

abstract public class BaseAdmanModule implements IAdmanModule {

    private IAdman adman;

    public int eventPriority() {
        return 1;
    }

    @Override
    public void bind(IAdman adman) {
        this.adman = adman;
        for (EventType eventType : eventTypes()) {
            this.adman.getDispatcher().addListener(eventType, this, eventPriority());
        }
    }

    @Override
    public void unbind() {
        for (EventType eventType : eventTypes()) {
            this.adman.getDispatcher().removeListener(eventType, this);
        }
        this.adman = null;
    }

    protected IAdman getAdman() {
        return adman;
    }
}
