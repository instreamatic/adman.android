package com.instreamatic.adman.module;

import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public interface IAdmanModule extends EventListener {
    public String getId();

    public int eventPriority();

    public void bind(IAdman adman);

    public void unbind();

    public EventType[] eventTypes();
}
