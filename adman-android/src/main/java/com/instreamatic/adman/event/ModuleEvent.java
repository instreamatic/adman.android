package com.instreamatic.adman.event;

public class ModuleEvent extends BaseEvent<ModuleEvent.Type, ModuleEvent.Listener> {

    final public static EventType<Type, ModuleEvent, Listener> TYPE = new EventType<Type, ModuleEvent, Listener>("module_between") {
        @Override
        public void callListener(ModuleEvent event, Listener listener) {
            listener.onModuleEvent(event);
        }
    };

    public enum Type {
        ADMAN_START,
        ADMAN_PAUSE,
        ADMAN_RESUME,
        RECORD_START,
        RECORD_STOP,
    }

    final public String sender;

    public ModuleEvent(Type type, String sender) {
        super(type);
        this.sender = sender;
    }
    public ModuleEvent(Type type) {
        this(type, null);
    }

    @Override
    public EventType<Type, ModuleEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onModuleEvent(ModuleEvent event);
    }
}
