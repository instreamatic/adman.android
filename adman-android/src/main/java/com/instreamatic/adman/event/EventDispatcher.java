package com.instreamatic.adman.event;

import android.util.Log;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class EventDispatcher {

    private Map<EventType<?, ?, ?>, Map<EventListener, Integer>> listeners;

    public EventDispatcher() {
        this.listeners = new HashMap<>();
    }

    public <T extends Enum, E extends BaseEvent<T, L>, L extends EventListener> void addListener(EventType<T, E, L> eventType, L listener) {
        addListener(eventType, listener, 0);
    }

    public <T extends Enum, E extends BaseEvent<T, L>, L extends EventListener>  void addListener(EventType<T, E, L> eventType, L listener, int priority) {
        if (!listeners.containsKey(eventType)) {
            listeners.put(eventType, new HashMap<EventListener, Integer>());
        }
        listeners.get(eventType).put(listener, priority);
    }

    public <T extends Enum, E extends BaseEvent<T, L>, L extends EventListener> void removeListener(EventType<T, E, L> eventType, L listener) {
        if (listeners.containsKey(eventType)) {
            listeners.get(eventType).remove(listener);
        }
    }

    @SuppressWarnings("unchecked")
    public void dispatch(BaseEvent event) {
        Log.i("EventDispatcher", "dispatch " + event.getEventType().type + ":" + event.getType().name());
        EventType eventType = event.getEventType();
        if (listeners.containsKey(eventType)) {
            Map<EventListener, Integer> l = listeners.get(eventType);
            for (EventListener listener : sortByValues(l).keySet()) {
                Log.d("EventDispatcher", "    dispatch " + eventType.type + ":" + event.getType().name() + " in " + listener + " (" + l.get(listener) + ")");
                eventType.callListener(event, listener);
                if (event.isStopped()) {
                    Log.d("EventDispatcher", "    stopped " + event.getEventType().type + ":" + event.getType().name());
                    break;
                }
            }
        }
    }

    private static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                int compare = map.get(k2).compareTo(map.get(k1));
                if (compare == 0) return 1;
                else return compare;
            }
        };
        Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }
}
