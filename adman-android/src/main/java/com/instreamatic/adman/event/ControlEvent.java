package com.instreamatic.adman.event;

public class ControlEvent extends BaseEvent<ControlEvent.Type, ControlEvent.Listener> {

    final public static EventType<Type, ControlEvent, Listener> TYPE = new EventType<Type, ControlEvent, Listener>("control") {
        @Override
        public void callListener(ControlEvent event, Listener listener) {
            listener.onControlEvent(event);
        }
    };

    public enum Type {
        PREPARE,
        START,
        PAUSE,
        RESUME,
        SKIP,
        CLICK,
        CLICK_POSITIVE,
        CLICK_NEGATIVE,
    }

    public ControlEvent(Type type) {
        super(type);
    }

    @Override
    public EventType<Type, ControlEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onControlEvent(ControlEvent event);
    }
}
