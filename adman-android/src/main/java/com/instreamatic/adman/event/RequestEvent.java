package com.instreamatic.adman.event;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.vast.model.VASTInline;

import java.util.List;
import java.util.Map;

public class RequestEvent extends BaseEvent<RequestEvent.Type, RequestEvent.Listener> {

    final public static EventType<Type, RequestEvent, Listener> TYPE = new EventType<Type, RequestEvent, Listener>("request") {
        @Override
        public void callListener(RequestEvent event, Listener listener) {
            listener.onRequestEvent(event);
        }
    };

    public enum Type {
        REQUEST_VERIFICATION_FAILED,
        LOAD,
        FAILED,
        NONE,
        SUCCESS,
    }

    final public AdmanRequest request;
    final public Map<String, String> params;
    final public List<VASTInline> ads;

    public RequestEvent(AdmanRequest request, Type type) {
        this(request, type, null, null);
    }

    public RequestEvent(AdmanRequest request, Type type, Map<String, String> params, List<VASTInline> ads) {
        super(type);
        this.request = request;
        this.params = params;
        this.ads = ads;
    }

    public RequestEvent(AdmanRequest request, Type type, Map<String, String> params) {
        this(request, type, params, null);
    }

    public RequestEvent(AdmanRequest request, Type type, List<VASTInline> ads) {
        this(request, type, null, ads);
    }

    @Override
    public EventType<Type, RequestEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onRequestEvent(RequestEvent event);
    }
}