package com.instreamatic.adman.event;

abstract public class BaseEvent<T extends Enum, L extends EventListener> {

    final private T type;

    private boolean stopped;

    public BaseEvent(T type) {
        this.type = type;
        this.stopped = false;
    }

    abstract public EventType<T, ?, L> getEventType();

    public void stop() {
        stopped = true;
    }

    public boolean isStopped() {
        return stopped;
    }

    public T getType() {
        return type;
    }

}
