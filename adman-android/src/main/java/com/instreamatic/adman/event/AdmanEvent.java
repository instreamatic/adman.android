package com.instreamatic.adman.event;

public class AdmanEvent extends BaseEvent<AdmanEvent.Type, AdmanEvent.Listener> {

    final public static EventType<Type, AdmanEvent, Listener> TYPE = new EventType<Type, AdmanEvent, Listener>("adman") {
        @Override
        public void callListener(AdmanEvent event, Listener listener) {
            listener.onAdmanEvent(event);
        }
    };

    public enum Type {
        PREPARE,
        NONE,
        FAILED,
        READY,
        STARTED,
        ALMOST_COMPLETE,
        COMPLETED,
        SKIPPED
    }

    public AdmanEvent(Type type) {
        super(type);
    }

    @Override
    public EventType<Type, AdmanEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onAdmanEvent(AdmanEvent event);
    }
}
