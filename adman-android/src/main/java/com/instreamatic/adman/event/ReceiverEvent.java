package com.instreamatic.adman.event;

import android.content.Context;

public class ReceiverEvent extends BaseEvent<ReceiverEvent.Type, ReceiverEvent.Listener> {
    final public static int PRIORITY_DEFAULT = 10;

    final public static EventType<Type, ReceiverEvent, Listener> TYPE = new EventType<Type, ReceiverEvent, Listener>("inside") {
        @Override
        public void callListener(ReceiverEvent event, Listener listener) {
            listener.onReceiverEvent(event);
        }
    };

    public enum Type {
        PHONE_UNLOCKED,
        PHONE_LOCKED,
    }
    final private Context context;

    public ReceiverEvent(Type type, Context context) {
        super(type);
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public EventType<Type, ReceiverEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onReceiverEvent(ReceiverEvent event);
    }
}
