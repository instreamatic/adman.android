package com.instreamatic.adman.event;

abstract public class EventType<T extends Enum, E extends BaseEvent<T, L>, L extends EventListener> {
    final public String type;

    public EventType(String type) {
        this.type = type;
    }

    abstract public void callListener(E event, L listener);
}
