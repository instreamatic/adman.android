package com.instreamatic.adman.event;

import android.app.Activity;

public class ActivityEvent extends BaseEvent<ActivityEvent.Type, ActivityEvent.Listener> {
    final public static int PRIORITY_DEFAULT = 10;

    final public static EventType<Type, ActivityEvent, Listener> TYPE = new EventType<Type, ActivityEvent, Listener>("inside") {
        @Override
        public void callListener(ActivityEvent event, Listener listener) {
            listener.onActivityEvent(event);
        }
    };

    public enum Type {
        ON_RESUMED,
        ON_STOPPED,
    }
    final private Activity activity;

    public ActivityEvent(Type type, Activity activity) {
        super(type);
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    @Override
    public EventType<Type, ActivityEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onActivityEvent(ActivityEvent event);
    }
}
