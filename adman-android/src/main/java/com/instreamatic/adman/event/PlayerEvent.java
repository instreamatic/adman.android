package com.instreamatic.adman.event;

public class PlayerEvent extends BaseEvent<PlayerEvent.Type, PlayerEvent.Listener> {

    final public static EventType<Type, PlayerEvent, Listener> TYPE = new EventType<Type, PlayerEvent, Listener>("player") {
        @Override
        public void callListener(PlayerEvent event, Listener listener) {
            listener.onPlayerEvent(event);
        }
    };

    public enum Type {
        PREPARE,
        READY,
        FAILED,
        PLAYING,
        PLAY,
        PAUSE,
        PROGRESS,
        COMPLETE,
        CLOSEABLE,
        SKIPPABLE,
    }

    public PlayerEvent(Type type) {
        super(type);
    }

    @Override
    public EventType<Type, PlayerEvent, Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onPlayerEvent(PlayerEvent event);
    }
}
