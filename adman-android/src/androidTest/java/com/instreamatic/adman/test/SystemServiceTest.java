package com.instreamatic.adman.test;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;

import com.instreamatic.adman.test.utils.BaseTest;
import com.instreamatic.adman.variable.SystemServiceUtil;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class SystemServiceTest extends BaseTest {

    @Test
    public void testAudioType()
    {
        Context context = super.getContext();
        SystemServiceUtil.AudioType audioType = SystemServiceUtil.getAudioType(context);
        Assert.assertTrue(String.format("AudioType (%s) not Empty", audioType.type), audioType != SystemServiceUtil.AudioType.NONE);
    }

    @Test
    public void testNetworkType()
    {
        Context context = super.getContext();
        SystemServiceUtil.NetworkType networkType = SystemServiceUtil.getNetworkType(context);
        Assert.assertTrue(String.format("NetworkType (%s) not Empty", networkType.type), networkType != SystemServiceUtil.NetworkType.NONE);
    }

}
