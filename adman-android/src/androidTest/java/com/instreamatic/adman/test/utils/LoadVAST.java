package com.instreamatic.adman.test.utils;

import com.instreamatic.vast.VASTException;
import com.instreamatic.vast.VASTParser;
import com.instreamatic.vast.model.VAST;

import org.w3c.dom.Document;

import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class LoadVAST {

    public static Document loadXml(InputStream in)
    {
        Document doc = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            // f.setValidating(false); // не делать проверку валидации
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(in);
        }
        catch (Exception e){

        }
        return doc;
    }


    public static VAST LoadVAST(InputStream in)
    {
        Document document = loadXml(in);
        if (document == null) return null;

        VAST result;
        try {
            result = VASTParser.parseDocument(document);
        } catch (VASTException e) {
            result = null;
        }
        return result;
    }

}
