package com.instreamatic.adman.test;

import android.content.res.AssetManager;
import android.support.test.runner.AndroidJUnit4;

import com.instreamatic.adman.test.utils.BaseTest;
import com.instreamatic.adman.test.utils.LoadVAST;
import com.instreamatic.vast.model.VAST;
import com.instreamatic.vast.model.VASTAd;
import com.instreamatic.vast.model.VASTInline;

import java.io.InputStream;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(AndroidJUnit4.class)
public class LoadVASTTest extends BaseTest {
    static String fileVAST1 = "xml/vast_voice_us.xml";
    static String fileVAST2 = "xml/vast_voice_v5.xml";
    static String fileVAST3 = "xml/vast_wrapper.xml";
    static String fileVAST4 = "xml/vast_wrapper_streamtheworld.xml";

    private  VAST parametersVAST = null;
    private  String fileVAST = null;


    @Before
    public void InitVASTParameters()
    {
        if (fileVAST == null || fileVAST.isEmpty()) {
            return;
        }
        AssetManager assetManager = super.getAssetManager();
        try {
            InputStream in = assetManager.open(fileVAST);
            parametersVAST = LoadVAST.LoadVAST(in);
        }
        catch (Exception e){
            parametersVAST = null;

        }
        Assert.assertNotNull("VAST load", parametersVAST);
    }

    @Test
    public void CheckExpires()
    {
        fileVAST = fileVAST1;
        long value = 0;
        if (parametersVAST != null) {
            VASTInline inlineVAST = parametersVAST.getInline();
            String expires = inlineVAST.extensions.containsKey("Expires") ? inlineVAST.extensions.get("Expires").value : "0";
            value = Long.valueOf(expires.trim());
        }
        Assert.assertNotEquals("VAST Expires", value, 0);
    }

    @Test
    public void CheckExtension()
    {
        fileVAST = fileVAST2;
        String value = "";
        if (parametersVAST == null) InitVASTParameters();
        if(parametersVAST != null) {
            VASTInline inlineVAST = parametersVAST.getInline();
            value = inlineVAST.extensions.containsKey("ResponseLanguage") ? inlineVAST.extensions.get("ResponseLanguage").value : "";
        }
        Assert.assertTrue("VAST Language", value.isEmpty() == false);
    }

    @Test
    public void CheckParameters()
    {
        fileVAST = fileVAST4;
        String adSystem = null;
        if (parametersVAST == null) InitVASTParameters();
        if (parametersVAST != null) {
            VASTAd vast = parametersVAST.getVAST();
            if (vast != null) adSystem = vast.adSystem;
        }
        Assert.assertNotEquals("VAST AdSystem", adSystem, null);
    }

}
