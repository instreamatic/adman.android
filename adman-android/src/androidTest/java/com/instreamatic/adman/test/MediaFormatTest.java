package com.instreamatic.adman.test;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.instreamatic.adman.test.utils.BaseTest;
import com.instreamatic.adman.test.utils.LoadVAST;
import com.instreamatic.format.MediaFormat;
import com.instreamatic.vast.VASTSelector;
import com.instreamatic.vast.model.VAST;
import com.instreamatic.vast.model.VASTInline;
import com.instreamatic.vast.model.VASTMedia;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.InputStream;


@RunWith(AndroidJUnit4.class)
public class MediaFormatTest extends BaseTest {

    static public MediaFormat InitMediaFormat(String[] mimeTypes, int bitrate, boolean isStrict) {
        return new MediaFormat(mimeTypes, bitrate, isStrict);
    }

    static public MediaFormat InitMediaFormat(String mimeType, int bitrate, boolean isStrict) {
        return InitMediaFormat(new String[]{mimeType}, bitrate, isStrict);
    }

    static public MediaFormat DefaultMediaFormat()
    {
        String[] mimeTypes = VASTSelector.DEFAULT_MEDIA.mimeTypes;
//        new String[]{
//                MediaFormat.MIMETYPE_AUDIO_MP3,
//                MediaFormat.MIMETYPE_AUDIO_MPEG,
//        };
        int bitrate = MediaFormat.DEFAULT_BITRATE;
        boolean isStrict = false;
        return InitMediaFormat(mimeTypes, bitrate, isStrict);
    }

    static public MediaFormat DefaultMediaFormatWithBitrate(int bitrate, boolean strict)
    {
        String[] mimeTypes = VASTSelector.DEFAULT_MEDIA.mimeTypes;
        return InitMediaFormat(mimeTypes, bitrate, strict);
    }

    private  VAST parametersVAST = null;

    @Before
    public void InitVASTParameters()
    {
        //init VAST
        AssetManager assetManager = super.getAssetManager();
        try {
            InputStream in = assetManager.open("xml/vast_voice_us.xml");
            parametersVAST = LoadVAST.LoadVAST(in);
        }
        catch (Exception e){
            parametersVAST = null;

        }
        Assert.assertNotNull("VAST load is FAIL!!!", parametersVAST);
    }

    @Test
    public void testDefault()
    {
        //set MediaFormat
        VASTSelector selector = new VASTSelector();
        selector.setMediaFormat(DefaultMediaFormat());

        //extract media from VAST
        VASTMedia media = null;
        if(parametersVAST != null) {
            VASTInline inlineVAST = parametersVAST.getInline();
            media = selector.selectMedia(inlineVAST.medias);
        }

        Assert.assertNotNull("DEFAULT MediaFormat has been selected!", media);
    }

    @Test
    public void testBitrate()
    {
        //set MediaFormat
        VASTSelector selector = new VASTSelector();
        selector.setMediaFormat(DefaultMediaFormatWithBitrate(MediaFormat.Bitrates.BITRATE_128.code, false));

        //extract media from VAST
        VASTMedia media = null;
        if(parametersVAST != null) {
            VASTInline inlineVAST = parametersVAST.getInline();
            media = selector.selectMedia(inlineVAST.medias);
        }

        Assert.assertNotNull("Extract MediaFormat BITRATE_128 strong FALSE!", media);
    }

    @Test
    public void testMimeType()
    {
        //set MediaFormat
        VASTSelector selector = new VASTSelector();
        selector.setMediaFormat(InitMediaFormat(MediaFormat.MIMETYPE_AUDIO_AAC, MediaFormat.DEFAULT_BITRATE, true));

        //extract media from VAST
        VASTMedia media = null;
        if(parametersVAST != null) {
            VASTInline inlineVAST = parametersVAST.getInline();
            media = selector.selectMedia(inlineVAST.medias);
        }

        Assert.assertNull("Extract MediaFormat AUDIO_AAC strong TRUE!", media);
    }


}
