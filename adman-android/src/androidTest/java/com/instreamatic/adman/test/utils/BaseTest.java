package com.instreamatic.adman.test.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.test.InstrumentationRegistry;

public class BaseTest {

    public static Context getContext() {
        return InstrumentationRegistry.getInstrumentation().getContext();
    }

    public static AssetManager getAssetManager(){
        Context context = getContext();
        return context != null ? context.getAssets() : null;

    }
}
