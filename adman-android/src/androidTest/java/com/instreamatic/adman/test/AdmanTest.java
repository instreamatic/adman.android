package com.instreamatic.adman.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.event.RequestEvent;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@RunWith(AndroidJUnit4.class)
public class AdmanTest {

    public static AdmanRequest buildRequest() {
        return new AdmanRequest.Builder()
                .setRegion(Region.EUROPE)
                .setSiteId(777)
                .setPreview(11)
                .build();
    }

    public static Context getContext() {
        return InstrumentationRegistry.getInstrumentation().getContext();
    }

    @Test
    public void testRequest() throws Exception {
        final CountDownLatch lock = new CountDownLatch(2);
        final Map<RequestEvent.Type, Boolean> state = new HashMap<>();
        IAdman adman = new Adman(getContext(), buildRequest());
        adman.getDispatcher().addListener(RequestEvent.TYPE, new RequestEvent.Listener() {
            @Override
            public void onRequestEvent(RequestEvent event) {
                state.put(event.getType(), true);
                switch (event.getType()) {
                    case LOAD:
                    case SUCCESS:
                        lock.countDown();
                        break;
                    case FAILED:
                    case NONE:
                        Assert.fail(event.getType().name());
                        break;
                }
            }
        });
        adman.preload();
        lock.await(5, TimeUnit.SECONDS);
        Assert.assertTrue("!AD_PREPARE", state.containsKey(RequestEvent.Type.LOAD));
        Assert.assertTrue("!AD_SUCCESS", state.containsKey(RequestEvent.Type.SUCCESS));
    }

    @Test
    public void testPlaying() throws Exception {
        final CountDownLatch lock = new CountDownLatch(4);
        final Map<PlayerEvent.Type, Boolean> state = new HashMap<>();
        IAdman adman = new Adman(getContext(), buildRequest());
        adman.getDispatcher().addListener(PlayerEvent.TYPE, new PlayerEvent.Listener() {
            @Override
            public void onPlayerEvent(PlayerEvent event) {
                state.put(event.getType(), true);
                switch (event.getType()) {
                    case PREPARE:
                    case PLAYING:
                    case READY:
                    case COMPLETE:
                        lock.countDown();
                        break;
                    case FAILED:
                        Assert.fail(event.getType().name());
                        break;
                }
            }
        });
        adman.start();
        lock.await(30, TimeUnit.SECONDS);
        Assert.assertTrue("!PLAYER_PREPARE", state.containsKey(PlayerEvent.Type.PREPARE));
        Assert.assertTrue("!PLAYER_READY", state.containsKey(PlayerEvent.Type.READY));
        Assert.assertTrue("!PLAYER_PLAYING", state.containsKey(PlayerEvent.Type.PLAYING));
        Assert.assertTrue("!PLAYER_COMPLETE", state.containsKey(PlayerEvent.Type.COMPLETE));
    }
}
