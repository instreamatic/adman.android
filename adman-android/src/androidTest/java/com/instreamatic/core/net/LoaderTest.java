package com.instreamatic.core.net;

import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.UserId;
import com.instreamatic.adman.source.AdmanSource;
import com.instreamatic.core.android.net.XmlLoader;
import com.instreamatic.vast.VASTLoader;
import com.instreamatic.vast.model.VAST;
import com.instreamatic.vast.model.VASTAd;
import com.instreamatic.vast.model.VASTInline;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


@RunWith(AndroidJUnit4.class)
public class LoaderTest {

    public static AdmanRequest buildRequest() {
        return new AdmanRequest.Builder()
                .setRegion(Region.EUROPE)
                .setSiteId(777)
                .setPreview(11)
                .build();
    }

    @Test
    public void testXMLLoad() throws Exception {
        final CountDownLatch lock = new CountDownLatch(2);

        final Map<String, Boolean> state = new HashMap<>();

        //String url = buildRequest().buildUrl(new UserId());
        String url = "https://x.instreamatic.com/v4/vast/777?preview=90&advertising_id=c2fda714-c4f3-4b44-a543-694308002a0b&slot=instreamatic&type=any&lang=ru-RU&microphone=1";
        new XmlLoader().GET(url, new ICallback<Document>() {
            @Override
            public void onSuccess(Document data) {
                lock.countDown();
                state.put("onSuccess", true);
            }

            @Override
            public void onFail(Throwable t) {
                lock.countDown();
                state.put("onFail", true);
            }
        });
        lock.await(5, TimeUnit.SECONDS);
        Assert.assertTrue("!XML_LOAD_SUCCESS", state.containsKey("onSuccess"));
    }

    /*
    @Test
    public void testVASTLoad() throws Exception {
        final CountDownLatch lock = new CountDownLatch(2);

        final Map<String, Boolean> state = new HashMap<>();

        String urlWithRedirect = "http://test.instreamatic.com/x/v4/vast/777";
        new VASTLoader().GET(urlWithRedirect, new ICallback<VAST>() {
            @Override
            public void onSuccess(VAST data) {
                lock.countDown();
                state.put("onSuccess", true);
            }

            @Override
            public void onFail(Throwable t) {
                lock.countDown();
                state.put("onFail", true);
            }
        });
        lock.await(5, TimeUnit.SECONDS);
        Assert.assertTrue("!XML_VAST_SUCCESS", state.containsKey("onSuccess"));
    }
    /**/
}
