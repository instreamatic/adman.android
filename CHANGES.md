# Changes history #

## 7.19.2 ##
- Adman: Fixed eventActivity for ActivityLifecycleEvent

## 7.19.1 ##
- Adman: Added WeakReference for Activity in ActionIntentStorage

## 7.19.0 ##
- Adman: Remove static from ActionIntentStorage; Added getStorageAction for IAdman;

## 7.18.3 ##
- Adman: Added duration to VASTInline

## 7.18.2 ##
- Adman: Catch IllegalStateException in AudioPlayer
- Adman: adman.vast.CLICK_THROUGH to getMetaData

## 7.18.1 ##
View: Refactoring BaseAdmanView::show

## 7.18.0 ##
- Adman: Added isVoiceRecording for IAdman; modify(fix) rebuild() method from BaseAdmanView

## 7.17.39 ##
- Voice: Added statistic pixel "response_error"

## 7.17.38 ##
- Adman: okhtpp 4.9.0 (A connection to <address> was leaked. Did you forget to close a response body? #4399)

## 7.17.37 ##
- Adman: off/on audio-focus from external/internal commands pause/resume

## 7.17.36 ##
- Adman: Added RequestEvent.Type.REQUEST_VERIFICATION_FAILED

## 7.17.35 ##
- Voice: Fixed function sendLast for onAudioFocusChange(1) (event control:RESUME)

## 7.17.34 ##
- Adman: Added "audio_output" and "network_type" to ad-request(VAST)

## 7.17.33 ##
- Adman: Added "adSystem" to VASTAd

## 7.17.32 ##
- Adman: Added event ALMOST_COMPLETE (after playing 3/4 time)
- Adman: Added ModuleEvent
- Voice: Added pause-resume for record voice

## 7.17.31 ##
- Adman: Check valid VASTCompanion(from selectCompanion)

## 7.17.30 ##
- Adman: Fix VASTLoader for vast-wrapper
- Adman: Changed URL for DEMO

## 7.17.29 ##
- Adman: Check requestVerification.isAllowed() for exception-RequestEvent-FAILED

## 7.17.28 ##
- Adman: Check valid AudioPlayer after setDataSource

## 7.17.27 ##
- Adman: Checking compatible Ad and Mic
- Voice: Sounds of Mic(on\off) for repeat(unknown)

## 7.17.26 ##

- Adman: Banner click and processing a "CompanionClickThrough"

## 7.17.25 ##

- View: Processing "Extension"-params from VAST: "controls" - visiblity of buttons on view; "isClickable" - is clickable banner

## 7.17.24 ##

- Voice: Sending user action statistic for concrete actions (button, voice, click)

## 7.17.23 ##

- Adman: Added "us_privacy" to AdmanRequest

## 7.17.22 ##

- Adman: adUrlAPI moved from Region to AdmanRequest

## 7.17.21 ##

- Adman: Added parameters time expiration VAST to RequestVerification and checked valid VAST before playing
- Adman: Added method hasValidVAST to the IAdman
- Adman: Increased timeout for prepare-method of MediaPlayer
- Adman: Added parameter Expires to VAST-Extension

## 7.17.20 ##

- Adman: Start positive intent manually

## 7.17.19 ##

- Adman: Changed method set image for banner

## 7.17.18 ##

- Adman: Remove PhoneUnlocked and leave only analysis ActivityLifecycleEvent

## 7.17.17 ##

- Adman: VAST parser - processing a missing node

## 7.17.16 ##

- Adman: Changed filter in queryIntentActivities; checking lifecycle state (isDisplay)

## 7.17.15 ##

 - Adman: Added VAST-request for custom ad-server (Region.adUrlAPI)

## 7.17.14 ##

 - Adman: Add item target to vast-request: campaign_id/gender/age

## 7.17.13 ##

 - Adman: Call positive action (intent) with analyse activity lifecycle

## 7.17.12 ##

 - View:  Check valid activity and bundle in BaseAdmanView

## 7.17.10 ##

 - Adman:  Add consent string

## 7.17.9 ##

 - Adman:  Add delayed positive action (then display is lock)

## 7.17.7 ##

 - Adman:  Add positive action for ad response "calendar"

## 7.17.6 ##

 - Adman: Call "can_show" for all items of AdmanRequest(site_id/region)

## 7.17.5 ##

 - Adman: Add Region.INDIA

## 7.17.3 ##

 - Adman: Add METADATA_KEY_ALBUM_ART to getMetadata()

## 7.17.2 ##

 - Adman: Add method getMetadata() to IAdman. It return: METADATA_KEY_MEDIA_ID, METADATA_KEY_TITLE, METADATA_KEY_ALBUM_ART_URI

## 7.17.1 ##

 - View: Remove context from DefaultAdmanViewBindFactory

## 7.17.0 ##

 - View: New methods build custom view

## 7.16.16 ##

 - Adman: Add "SendAction" to log; Check build bundle in BaseAdmanView

## 7.16.15 ##

 - View: Move BaseAdmanView to the adman-android-view

## 7.16.14 ##

 - Adman: Add timeout for MediaPlayer prepare

## 7.16.13 ##

 - Adman: use async Okhttp

## 7.16.12 ##

 - Adman: check ad with null and reset listener AudioPlayer

## 7.16.11 ##

 - Adman: can use the strict MediaFormat from VAST

## 7.16.10 ##

 - Adman: voice layout form has been moved from DefaultView to BaseView

## 7.16.9 ##

 - Adman: checking active Ad before start new Ad

## 7.16.8 ##

- Voice: close view after recognize error

## 7.16.7 ##

 - Voice: process skip on AdmanVoice (audio focus)

## 7.16.6 ##

 - Adman: add respose pixel(button, banner)
 - Adman: use AdId from VAST

## 7.16.5 ##

 - Adman: process extension "ResponseUrl"

## 7.16.4 ##

 - Adman: method updateRequest to IAdman

## 7.16.3 ##

 - Adman: add Slot "postroll"
 - Adman: use audio was moved from "mediaplayer" to "adman"

## 7.16.2 ##

 - Adman: add event "skippable" for param "skipoffset" from VAST
 - Voice: display turn on microphone
 
## 7.16.1 ##

- Adman: add VastSelector
- Adman: support AdmanRequest array in Adman
- View: banner click as positive response for voice ad
- Test: add ACRA library
- Test: update

## 7.16.0 ##

- View: add voice layout in DefaultAdmanView
- Voice: microphone on/off sounds
- Adman: open ad url Intent with ComponentName
- Test: update

## 7.15.2 ##

- Adman: userId in sendCanShow

## 7.15.1 ##

- Adman: microphone param in LiveStatisticLoader

## 7.15.0 ##

- Adman: audio focus support

## 7.14.6 ##

- Adman: fix

## 7.14.5 ##

- Adman: add sendCanShow method

## 7.14.4 ##

- Adman: remove READ_PHONE_STATE and WAKE_LOCK permissions
- Voice: catch IllegalArgumentException on create AudioRecord

## 7.14.3 ##

- VoiceDemo: update

## 7.14.2 ##

- Adman: remove DEVICE_ID usage

## 7.14.1 ##

- Adman: add User-Agent header
- Voice: update RequestModel

## 7.14.0 ##

- DTMF: add AdmanDTMF module
- Adman: add AdmanVariable module

## 7.13.1 ##

- Adman: fix prepare

## 7.13.0 ##

- Adman: add lang param to request

## 7.12.0 ##

- Voice: disable audio compress
- Voice: remove Speex library
- Voice: regional voice servers support

## 7.11.0 ##

- Voice: remove VAD library

## 7.10.2 ##

- Voice: add Yandex source type

## 7.10.1 ##

- Voice: support VAST response_delay extension

## 7.10.0 ##

- Adman: use okhttp

## 7.9.1 ##

- Voice: up okhttp version
- Voice: update

## 7.9.0 ##

- Voice: refactoring and cleaning

## 7.8.12 ##

- VoiceDemo: fix

## 7.8.11 ##

- VoiceDemo: update

## 7.8.10 ##

- VoiceDemo: fix

## 7.8.9 ##

- Voice: fix

## 7.8.8 ##

- Adman: add ControlEvent.SKIP

## 7.8.7 ##

- Voice: update
- VoiceDemo: update notification

## 7.8.6 ##

- VoiceDemo: add DemoService

## 7.8.5 ##

- VoiceDemo: add Develop build

## 7.8.4 ##

- Adman: add ControlEvent

## 7.8.3 ##

- VoiceDemo: change Region

## 7.8.2 ##

- Voice: add unknown action support

## 7.8.1 ##

- VoiceDemo: add Updater

## 7.8 ##

- Voice: add skip action support
- VoiceDemo: update

## 7.7 ##

- VoiceDemo: add

## 7.6 ##

- Adman: add EventDispatcher
- Voice: VAST response extension support

## 7.5 ##

- Voice: update

## 7.4 ##

- Voice: update

## 7.3 ##

- Adman: add LiveStatistic module

## 7.2 ##

- Module system.

## 7.1 ##

- Voice response without activity.
- Voice response onTranscriptionUpdate process.

## 7.0 ##

- Voice response.

## 6.0 ##

- VAST support.

## 5.0 ##

- Переработан набор событий для соотвествия VAST.

## 4.3 ##

- Исправлена сборка AAR.

## 4.2 ##

- Исправлена отправка статистики при отсутствии банера.

## 4.1 ##

- Raw StatId.

## 4.0 ##

- AAR.

## 3.0 ##

- Удален slotId.

## 2.2 ##

- Фикс события READY_FOR_PLAYBACK.

## 2.1 ##

- Фикс NPE при отправке статистики.

## 2.0 ##

- Изменения в структуре.

## 1.8 ##

- Добавлен zoneId.

## 1.7 ##

- Исправления.

## 1.6 ##

- DefaultAdmanView.
- Добавлены события в статистике.

## 1.5 ##

- Фиксы повторного использования медиаплеера.
- Растягивание банера.

## 1.4 ##

- AdManView перенесён в модуль adman-android.

## 1.3 ## 

- Генерация ошибки при попытке воспроизвести рекламу с выключенным звуком.
- Добавлена генерация документации.

## 1.2 ##

- Предзагрузка ресурсов.

## 1.1 ##

- Добавлен метод closeBanner. 
- Добавлен флажок autoPlay.
